package com.coderview.common.constant;

import com.coderview.common.error.UnsupportedProgrammingLanguageException;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum ProgrammingLanguageEnum {

    JAVA("Java"),
    PYTHON("Python"),
    C_SHARP("C#");

    private final String value;

    ProgrammingLanguageEnum(final String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    public static ProgrammingLanguageEnum getByValue(final String value) {
        return Arrays.stream(values())
                .filter(entry -> entry.getValue().equalsIgnoreCase(value))
                .findFirst()
                .orElseThrow(() -> new UnsupportedProgrammingLanguageException(value));
    }
}
