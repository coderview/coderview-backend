package com.coderview.common.constant;

public enum ChallengeScope {
    GLOBAL,
    PRIVATE
}
