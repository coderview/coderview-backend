package com.coderview.common.constant;

public class SecurityConstants {

    public static final String AUTH_URL = "/authenticate";

    public static final String JWT_SECRET = "u7x!A%C*F-JaNdRgUkXp2s5v8y/B?E(G+KbPeShVmYq3t6w9z$C&F)J@McQfTjWn";

    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_TYPE_KEY = "typ";
    public static final String TOKEN_ISSUER = "coderview-auth-api";
    public static final String TOKEN_AUDIENCE = "coderview-webapp";
    public static final String ROLES_KEY = "roles";

    private SecurityConstants() {}
}
