package com.coderview.common.constant;

public class SolrConstants {

    public static final String INDEX_KEY = "{{index}}";

    public static final String INTERVIEW_INDEX = "interview";

    public static final String CHALLENGE_INDEX = "challenge";

    private SolrConstants() {
    }
}
