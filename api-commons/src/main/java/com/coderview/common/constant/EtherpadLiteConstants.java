package com.coderview.common.constant;

public class EtherpadLiteConstants {

    public static final String GROUP_ID = "groupID";
    public static final String GROUP_IDS = "groupIDs";
    public static final String PAD_IDS = "padIDs";
    public static final String PAD_ID = "padID";
    public static final String AUTHOR_ID = "authorID";
    public static final String AUTHOR_NAME = "authorName";
    public static final String SESSION_ID = "sessionID";
    public static final String VALID_UNTIL = "validUntil";
    public static final String TEXT = "text";
    public static final String HTML = "html";
    public static final String REVISIONS = "revisions";
    public static final String SAVED_REVISIONS = "savedRevisions";
    public static final String PAD_USERS_COUNT = "padUsersCount";
    public static final String PAD_USERS = "padUsers";
    public static final String COLOR_ID = "colorId";
    public static final String NAME = "name";
    public static final String TIMESTAMP = "timestamp";
    public static final String ID = "id";
    public static final String READ_ONLY_ID = "readOnlyID";
    public static final String PUBLIC_STATUS = "publicStatus";
    public static final String PASSWORD_PROTECTION = "passwordProtection";
    public static final String AUTHOR_IDS = "authorIDs";
    public static final String LAST_EDITED = "lastEdited";
    public static final String CURRENT_VERSION = "currentVersion";
    public static final String API_ROOT = "/api";
    public static final String STATS_URI = "/stats";

    private EtherpadLiteConstants() {}
}
