package com.coderview.common.constant;

public class EtherpadLiteUriConstants {

    public static final String KEY_API_VERSION = "{{api-version}}";
    public static final String KEY_METHOD = "{{method}}";
    public static final String KEY_API_KEY = "{{api-key}}";

    private EtherpadLiteUriConstants() {}
}
