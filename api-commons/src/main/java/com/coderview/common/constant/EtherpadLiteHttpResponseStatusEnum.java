package com.coderview.common.constant;

import com.coderview.common.error.CoreCoderviewException;

import java.util.Arrays;

public enum EtherpadLiteHttpResponseStatusEnum {

    EVERYTHING_OK(0, "everything ok"),
    WRONG_PARAMETERS(1, "wrong parameters"),
    INTERNAL_ERROR(2, "internal error"),
    NO_SUCH_FUNCTION(3, "no such function"),
    NO_OR_WRONG_API_KEY(4, "no or wrong API Key");

    private final Integer code;
    private final String codeMeaning;

    EtherpadLiteHttpResponseStatusEnum(final Integer code, final String codeMeaning) {
        this.code = code;
        this.codeMeaning = codeMeaning;
    }

    public Integer getCode() {
        return code;
    }

    public String getCodeMeaning() {
        return codeMeaning;
    }

    public static EtherpadLiteHttpResponseStatusEnum getByCode(final Integer code) {
        return Arrays.stream(values())
                .filter(value -> value.getCode().equals(code))
                .findFirst()
                .orElseThrow(() -> new CoreCoderviewException("Unknown response code."));
    }
}
