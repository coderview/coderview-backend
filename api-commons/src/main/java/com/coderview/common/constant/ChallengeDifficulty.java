package com.coderview.common.constant;

import com.coderview.common.error.CoreCoderviewException;

import java.util.Arrays;

public enum ChallengeDifficulty {

    BEGINNER("Beginner"),
    EASY("Easy"),
    NORMAL("Normal"),
    HARD("Hard"),
    EXPERT("Expert");

    private final String value;

    ChallengeDifficulty(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ChallengeDifficulty getByValue(final String value) {
        return Arrays.stream(values())
                .filter(difficulty -> difficulty.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new CoreCoderviewException("Challenge difficulty not found."));
    }
}
