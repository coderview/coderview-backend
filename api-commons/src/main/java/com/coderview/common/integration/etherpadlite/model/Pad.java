package com.coderview.common.integration.etherpadlite.model;

import java.util.List;

public class Pad extends BasePayload {

    private String text;
    private String html;
    private Integer revisions;
    private Integer savedRevisions;
    private Integer padUsersCount;
    private String readOnlyID;
    private String padID;
    private boolean publicStatus;
    private boolean passwordProtection;
    private List<PadUser> padUsers;
    private List<String> authorIDs;
    private Long lastEdited;

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(final String html) {
        this.html = html;
    }

    public Integer getRevisions() {
        return revisions;
    }

    public void setRevisions(final Integer revisions) {
        this.revisions = revisions;
    }

    public Integer getSavedRevisions() {
        return savedRevisions;
    }

    public void setSavedRevisions(final Integer savedRevisions) {
        this.savedRevisions = savedRevisions;
    }

    public Integer getPadUsersCount() {
        return padUsersCount;
    }

    public void setPadUsersCount(final Integer padUsersCount) {
        this.padUsersCount = padUsersCount;
    }

    public String getReadOnlyID() {
        return readOnlyID;
    }

    public void setReadOnlyID(final String readOnlyID) {
        this.readOnlyID = readOnlyID;
    }

    public String getPadID() {
        return padID;
    }

    public void setPadID(final String padID) {
        this.padID = padID;
    }

    public boolean isPublicStatus() {
        return publicStatus;
    }

    public void setPublicStatus(final boolean publicStatus) {
        this.publicStatus = publicStatus;
    }

    public boolean isPasswordProtection() {
        return passwordProtection;
    }

    public void setPasswordProtection(final boolean passwordProtection) {
        this.passwordProtection = passwordProtection;
    }

    public List<PadUser> getPadUsers() {
        return padUsers;
    }

    public void setPadUsers(final List<PadUser> padUsers) {
        this.padUsers = padUsers;
    }

    public List<String> getAuthorIDs() {
        return authorIDs;
    }

    public void setAuthorIDs(final List<String> authorIDs) {
        this.authorIDs = authorIDs;
    }

    public Long getLastEdited() {
        return lastEdited;
    }

    public void setLastEdited(final Long lastEdited) {
        this.lastEdited = lastEdited;
    }
}
