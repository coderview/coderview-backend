package com.coderview.common.integration.etherpadlite.model;

import java.util.List;

public class SessionsList extends BasePayload {

    private List<Session> sessions;

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(final List<Session> sessions) {
        this.sessions = sessions;
    }
}
