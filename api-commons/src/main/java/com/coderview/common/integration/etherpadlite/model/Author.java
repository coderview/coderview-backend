package com.coderview.common.integration.etherpadlite.model;

public class Author extends BasePayload {

    private String authorID;
    private String authorName;

    public String getAuthorID() {
        return authorID;
    }

    public void setAuthorID(final String authorID) {
        this.authorID = authorID;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(final String authorName) {
        this.authorName = authorName;
    }
}
