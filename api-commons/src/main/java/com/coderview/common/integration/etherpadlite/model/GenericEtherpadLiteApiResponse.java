package com.coderview.common.integration.etherpadlite.model;

import java.util.Map;

public class GenericEtherpadLiteApiResponse {

    private Integer code;
    private String message;
    private Map<String, Object> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(final Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(final Map<String, Object> data) {
        this.data = data;
    }
}
