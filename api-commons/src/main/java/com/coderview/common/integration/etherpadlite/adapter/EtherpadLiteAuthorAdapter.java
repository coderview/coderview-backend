package com.coderview.common.integration.etherpadlite.adapter;

import com.coderview.common.integration.etherpadlite.model.Author;
import com.coderview.common.integration.etherpadlite.model.Pads;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategyFactory;
import com.coderview.common.integration.etherpadlite.util.EtherpadLiteUriBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class EtherpadLiteAuthorAdapter extends EtherpadLiteBaseAdapter {

    @Value("${etherpadLite.server.http.apiVersion:}")
    private String apiVersion;

    @Value("${etherpadLite.server.http.apikey:}")
    private String apiKey;

    public EtherpadLiteAuthorAdapter(final RestTemplate etherpadLiteApiRestTemplate,
                                     final PayloadTransformStrategyFactory payloadTransformStrategyFactory) {
        super(etherpadLiteApiRestTemplate, payloadTransformStrategyFactory);
    }

    /**
     * Creates a new author without a name.
     *
     * @return author that was created.
     */
    public Author createAuthor() {
        return createAuthor(StringUtils.EMPTY);
    }

    /**
     * Creates a new author.
     *
     * @param name author name.
     * @return author that was created.
     */
    public Author createAuthor(final String name) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("createAuthor")
                .addParam("name", name);

        return execute(builder.getUri(), Author.class, builder.getUriVariables());
    }

    /**
     * This method allows to map application specific author id to Etherpad lite author id.
     * New author (without name) will be created if author with authorMapper id does not exist.
     *
     * @param authorMapper application specific author id.
     * @return author that was created or already existing author
     */
    public Author createAuthorIfNotExistsFor(final String authorMapper) {
        return createAuthorIfNotExistsFor(authorMapper, StringUtils.EMPTY);
    }

    /**
     * This method allows to map application specific author id to Etherpad lite author id.
     * New author (without name) will be created if author with authorMapper id does not exist.
     *
     * @param authorMapper application specific author id.
     * @param name author name.
     * @return author that was created or already existing author
     */
    public Author createAuthorIfNotExistsFor(final String authorMapper, final String name) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("createAuthorIfNotExistsFor")
                .addParam("authorMapper", authorMapper)
                .addParam("name", name);

        return execute(builder.getUri(), Author.class, builder.getUriVariables());
    }

    /**
     * Returns a list of all pads this author contributed to.
     *
     * @param authorID author id.
     * @return a list of pad ids
     */
    public List<String> listPadsOfAuthor(final String authorID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("listPadsOfAuthor")
                .addParam("authorID", authorID);

        return execute(builder.getUri(), Pads.class, builder.getUriVariables()).getPadIDs();
    }

    /**
     * Returns the name of the author.
     *
     * @param authorID author id.
     * @return author name
     */
    public String getAuthorName(final String authorID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getAuthorName")
                .addParam("authorID", authorID);

        return execute(builder.getUri(), Author.class, builder.getUriVariables()).getAuthorName();
    }
}
