package com.coderview.common.integration.etherpadlite.adapter;

import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategyFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static com.coderview.common.constant.EtherpadLiteConstants.API_ROOT;
import static com.coderview.common.constant.EtherpadLiteConstants.CURRENT_VERSION;

@Component
public class EtherpadLiteApiVersionAdapter extends EtherpadLiteBaseAdapter {

    public EtherpadLiteApiVersionAdapter(final RestTemplate etherpadLiteApiRestTemplate,
                                         final PayloadTransformStrategyFactory payloadTransformStrategyFactory) {
        super(etherpadLiteApiRestTemplate, payloadTransformStrategyFactory);
    }

    /**
     * Get Etherpad Lite HTTP API version.
     *
     * @return version string
     */
    public String apiVersion() {
        return execute(API_ROOT).getData().get(CURRENT_VERSION).toString();
    }
}
