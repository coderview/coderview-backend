package com.coderview.common.integration.etherpadlite.model;

import java.util.List;

public class Pads extends BasePayload {

    private List<String> padIDs;

    public List<String> getPadIDs() {
        return padIDs;
    }

    public void setPadIDs(final List<String> padIDs) {
        this.padIDs = padIDs;
    }
}
