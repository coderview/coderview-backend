package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import com.coderview.common.integration.etherpadlite.model.Groups;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategy;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.coderview.common.constant.EtherpadLiteConstants.GROUP_IDS;

@Component
public class GroupsTransformStrategy implements PayloadTransformStrategy<Groups> {

    @Override
    public Groups transform(final GenericEtherpadLiteApiResponse response) {
        return Optional.ofNullable(response.getData())
                .map(this::createGroups)
                .orElse(null);
    }

    @Override
    public Class<Groups> getPayloadType() {
        return Groups.class;
    }

    private Groups createGroups(final Map<String, Object> data) {
        Groups groups = new Groups();
        groups.setGroupIDs((List) data.get(GROUP_IDS));
        return groups;
    }
}
