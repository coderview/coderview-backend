package com.coderview.common.integration.etherpadlite.model;

public class PadUser extends BasePayload {

    private String colorId;
    private String name;
    private Long timestamp;
    private String id;

    public String getColorId() {
        return colorId;
    }

    public void setColorId(final String colorId) {
        this.colorId = colorId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }
}
