package com.coderview.common.integration.etherpadlite.adapter;

import com.coderview.common.constant.EtherpadLiteHttpResponseStatusEnum;
import com.coderview.common.error.InvalidEtherpadLiteResponseCodeException;
import com.coderview.common.integration.etherpadlite.model.BasePayload;
import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategyFactory;
import org.springframework.web.client.RestTemplate;

import static com.coderview.common.constant.EtherpadLiteHttpResponseStatusEnum.EVERYTHING_OK;

public abstract class EtherpadLiteBaseAdapter {

    private final RestTemplate etherpadLiteApiRestTemplate;

    private final PayloadTransformStrategyFactory payloadTransformStrategyFactory;

    public EtherpadLiteBaseAdapter(final RestTemplate etherpadLiteApiRestTemplate,
                                   final PayloadTransformStrategyFactory payloadTransformStrategyFactory) {
        this.etherpadLiteApiRestTemplate = etherpadLiteApiRestTemplate;
        this.payloadTransformStrategyFactory = payloadTransformStrategyFactory;
    }

    protected <T extends BasePayload> T execute(final String uri, final Class<T> payloadType, final Object... uriVariables) {
        return transformResponse(execute(uri, uriVariables), payloadType);
    }

    protected GenericEtherpadLiteApiResponse execute(final String uri, final Object... uriVariables) {
        final GenericEtherpadLiteApiResponse response = etherpadLiteApiRestTemplate.getForObject(uri, GenericEtherpadLiteApiResponse.class, uriVariables);
        checkResponse(response);
        return response;
    }

    protected <T extends BasePayload> T transformResponse(final GenericEtherpadLiteApiResponse response, final Class<T> payloadType) {
        return (T) payloadTransformStrategyFactory.getTransformStrategy(payloadType).transform(response);
    }

    private void checkResponse(final GenericEtherpadLiteApiResponse response) {
        final EtherpadLiteHttpResponseStatusEnum status = EtherpadLiteHttpResponseStatusEnum.getByCode(response.getCode());
        if (status != EVERYTHING_OK) {
            throw new InvalidEtherpadLiteResponseCodeException(response.getCode(), status.getCodeMeaning(), response.getMessage());
        }
    }
}
