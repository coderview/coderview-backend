package com.coderview.common.integration.etherpadlite.adapter;

import com.coderview.common.integration.etherpadlite.model.Pad;
import com.coderview.common.integration.etherpadlite.model.PadUser;
import com.coderview.common.integration.etherpadlite.model.Pads;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategyFactory;
import com.coderview.common.integration.etherpadlite.util.EtherpadLiteUriBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Component
public class EtherpadLitePadAdapter extends EtherpadLiteBaseAdapter {

    @Value("${etherpadLite.server.http.apiVersion:}")
    private String apiVersion;

    @Value("${etherpadLite.server.http.apikey:}")
    private String apiKey;

    public EtherpadLitePadAdapter(final RestTemplate etherpadLiteApiRestTemplate,
                                  final PayloadTransformStrategyFactory payloadTransformStrategyFactory) {
        super(etherpadLiteApiRestTemplate, payloadTransformStrategyFactory);
    }

    /**
     * Returns the most recent text of a pad.
     *
     * @param padID pad id.
     * @return pad text
     */
    public String getText(final String padID) {
        return getText(padID, null);
    }

    /**
     * Returns the text of a pad. If revision number is not specified then the most recent version will be returned.
     *
     * @param padID pad id.
     * @param rev revision number.
     * @return pad text
     */
    public String getText(final String padID, final Integer rev) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getText")
                .addParam("padID", padID);

        if (Objects.nonNull(rev)) {
            builder.addParam("rev", String.valueOf(rev));
        }

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getText();
    }

    /**
     * Sets the text of a pad.
     *
     * @param padID pad id.
     * @param text text for the pad.
     */
    public void setText(final String padID, final String text) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("setText")
                .addParam("padID", padID)
                .addParam("text", text);

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Appends text to a pad.
     *
     * @param padID pad id.
     * @param text text to append for the pad.
     */
    public void appendText(final String padID, final String text) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("appendText")
                .addParam("padID", padID)
                .addParam("text", text);

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Returns the most recent text of a pad formatted as HTML.
     *
     * @param padID pad id.
     * @return pad text formated as HTML
     */
    public String getHTML(final String padID) {
        return getHTML(padID, null);
    }

    /**
     * Returns the text of a pad formatted as HTML. If revision number is not specified then the most
     * recent version will be returned.
     *
     * @param padID pad id.
     * @param rev revision number.
     * @return pad text formated as HTML
     */
    public String getHTML(final String padID, final Integer rev) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getHTML")
                .addParam("padID", padID);

        if (Objects.nonNull(rev)) {
            builder.addParam("rev", String.valueOf(rev));
        }

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getHtml();
    }

    /**
     * Sets the text of a pad based on HTML, HTML must be well-formed. Malformed HTML will send a warning to the API log.
     *
     * @param padID pad id.
     * @param html pad text formatted as HTML.
     */
    public void setHTML(final String padID, final String html) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("setHTML")
                .addParam("padID", padID)
                .addParam("html", html);

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Creates a new (non-group) pad (without predefined text). Note that if you need to create a group Pad, you
     * should call createGroupPad. You get an error message if you use one of the following characters
     * in the padID: "/", "?", "&" or "#".
     *
     * @param padID pad id.
     */
    public void createPad(final String padID) {
        createPad(padID, StringUtils.EMPTY);
    }

    /**
     *  Creates a new (non-group) pad. Note that if you need to create a group Pad, you should call createGroupPad.
     *  You get an error message if you use one of the following characters in the padID: "/", "?", "&" or "#".
     *
     * @param padID pad id.
     * @param text pad text.
     */
    public void createPad(final String padID, final String text) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("createPad")
                .addParam("padID", padID)
                .addParam("text", text);

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Returns the number of revisions of this pad.
     *
     * @param padID pad id.
     * @return number of revisions
     */
    public Integer getRevisionsCount(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getRevisionsCount")
                .addParam("padID", padID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getRevisions();
    }

    /**
     * Returns the number of saved revisions of this pad.
     *
     * @param padID pad id.
     * @return number of saved revisions
     */
    public Integer getSavedRevisionsCount(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getSavedRevisionsCount")
                .addParam("padID", padID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getSavedRevisions();
    }

    /**
     * Returns the number of users that are currently editing this pad.
     *
     * @param padID pad id.
     * @return number of users
     */
    public Integer padUsersCount(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("padUsersCount")
                .addParam("padID", padID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getPadUsersCount();
    }

    /**
     * Returns the list of users that are currently editing this pad.
     *
     * @param padID pad id.
     * @return list of pad users
     */
    public List<PadUser> padUsers(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("padUsers")
                .addParam("padID", padID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getPadUsers();
    }

    /**
     * Deletes a pad.
     *
     * @param padID pad id to delete.
     */
    public void deletePad(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("deletePad")
                .addParam("padID", padID);

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Returns the read only link of a pad.
     *
     * @param padID pad id.
     * @return pad read only id
     */
    public String getReadOnlyID(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getReadOnlyID")
                .addParam("padID", padID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getReadOnlyID();
    }

    /**
     * Returns the id of a pad which is assigned to the readOnlyID.
     * TODO: fix this method, does not work correctly.
     *
     * @param readOnlyID pad read only id.
     * @return pad id
     */
    public String getPadID(final String readOnlyID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getPadID")
                .addParam("readOnlyID", readOnlyID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getPadID();
    }

    /**
     * Sets a boolean for the public status of a pad (true or false).
     *
     * @param padID pad id.
     * @param publicStatus public status.
     */
    public void setPublicStatus(final String padID, final boolean publicStatus) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("setPublicStatus")
                .addParam("padID", padID)
                .addParam("publicStatus", String.valueOf(publicStatus));

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Gets the public status of a pad (true or false).
     *
     * @param padID pad id.
     * @return public status (true or false)
     */
    public boolean getPublicStatus(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getPublicStatus")
                .addParam("padID", padID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).isPublicStatus();
    }

    /**
     * Sets password for the pad.
     *
     * @param padID pad id.
     * @param password password.
     */
    public void setPassword(final String padID, final String password) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("setPassword")
                .addParam("padID", padID)
                .addParam("password", password);

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Returns boolean indicating if a pad is password protected.
     *
     * @param padID pad id.
     * @return true if password is set, false otherwise
     */
    public boolean isPasswordProtected(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("isPasswordProtected")
                .addParam("padID", padID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).isPasswordProtection();
    }

    /**
     * Returns a list of authors who contributed to this pad.
     *
     * @param padID pad id.
     * @return a list of author ids
     */
    public List<String> listAuthorsOfPad(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("listAuthorsOfPad")
                .addParam("padID", padID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getAuthorIDs();
    }

    /**
     * Returns the timestamp of the last revision of the pad.
     *
     * @param padID pad id.
     * @return timestamp of the last edit
     */
    public Long getLastEdited(final String padID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getLastEdited")
                .addParam("padID", padID);

        return execute(builder.getUri(), Pad.class, builder.getUriVariables()).getLastEdited();
    }

    /**
     * Validates current api token.
     */
    public void checkToken() {
        final String uri = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("checkToken")
                .getUri();

        execute(uri);
    }

    /**
     * Lists all pads on this Etherpad lite instance.
     *
     * @return a list of pad ids
     */
    public List<String> listAllPads() {
        final String uri = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("listAllPads")
                .getUri();

        return execute(uri, Pads.class).getPadIDs();
    }
}
