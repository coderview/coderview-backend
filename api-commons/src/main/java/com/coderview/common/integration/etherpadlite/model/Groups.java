package com.coderview.common.integration.etherpadlite.model;

import java.util.List;

public class Groups extends BasePayload {

    private List<String> groupIDs;

    public List<String> getGroupIDs() {
        return groupIDs;
    }

    public void setGroupIDs(final List<String> groupIDs) {
        this.groupIDs = groupIDs;
    }
}
