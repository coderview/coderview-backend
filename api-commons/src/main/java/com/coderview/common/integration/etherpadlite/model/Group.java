package com.coderview.common.integration.etherpadlite.model;

public class Group extends BasePayload {

    private String groupID;

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(final String groupID) {
        this.groupID = groupID;
    }
}
