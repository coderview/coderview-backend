package com.coderview.common.integration.etherpadlite.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class EtherpadLiteIntegrationConfiguration {

    @Value("${etherpadLite.server.rootUri:}")
    private String rootUri;

    @Bean
    public RestTemplate etherpadLiteApiRestTemplate() {
        return new RestTemplateBuilder()
                .rootUri(rootUri)
                .build();
    }
}
