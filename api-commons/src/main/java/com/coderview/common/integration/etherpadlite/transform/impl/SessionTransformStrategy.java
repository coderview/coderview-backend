package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import com.coderview.common.integration.etherpadlite.model.Session;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategy;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

import static com.coderview.common.constant.EtherpadLiteConstants.AUTHOR_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.GROUP_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.SESSION_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.VALID_UNTIL;

@Component
public class SessionTransformStrategy implements PayloadTransformStrategy<Session> {

    @Override
    public Session transform(final GenericEtherpadLiteApiResponse response) {
        return Optional.ofNullable(response.getData())
                .map(this::createSession)
                .orElse(null);
    }

    @Override
    public Class<Session> getPayloadType() {
        return Session.class;
    }

    private Session createSession(final Map<String, Object> data) {
        final Session session = new Session();
        Optional.ofNullable(data.get(SESSION_ID)).ifPresent(sessionID -> session.setSessionID(sessionID.toString()));
        Optional.ofNullable(data.get(AUTHOR_ID)).ifPresent(authorID -> session.setAuthorID(authorID.toString()));
        Optional.ofNullable(data.get(GROUP_ID)).ifPresent(groupID -> session.setGroupID(groupID.toString()));
        Optional.ofNullable(data.get(VALID_UNTIL)).ifPresent(validUntil -> session.setValidUntil(Long.valueOf(validUntil.toString())));
        return session;
    }
}
