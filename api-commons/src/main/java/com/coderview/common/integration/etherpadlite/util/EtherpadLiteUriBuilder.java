package com.coderview.common.integration.etherpadlite.util;

import com.coderview.common.error.CoreCoderviewException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.coderview.common.constant.EtherpadLiteUriConstants.KEY_API_KEY;
import static com.coderview.common.constant.EtherpadLiteUriConstants.KEY_API_VERSION;
import static com.coderview.common.constant.EtherpadLiteUriConstants.KEY_METHOD;

public class EtherpadLiteUriBuilder {

    private static final String URI_TEMPLATE = "/api/{{api-version}}/{{method}}?apikey={{api-key}}";

    private String uri;
    private String method;
    private List<String> keys = new ArrayList<>();
    private List<String> values = new ArrayList<>();

    private EtherpadLiteUriBuilder(final String apiVersion, final String apiKey) {
        this.uri = URI_TEMPLATE
                .replace(KEY_API_VERSION, apiVersion)
                .replace(KEY_API_KEY, apiKey);
    }

    public static EtherpadLiteUriBuilder newBuilder(final String apiVersion, final String apiKey) {
        return new EtherpadLiteUriBuilder(apiVersion, apiKey);
    }

    public EtherpadLiteUriBuilder method(final String method) {
        this.method = method;
        return this;
    }

    public EtherpadLiteUriBuilder addParam(final String key, final String value) {
        if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value)) {
            keys.add(key);
            values.add(value);
        }
        return this;
    }

    public String getUri() {
        if (StringUtils.isBlank(method)) {
            throw new CoreCoderviewException("Api method must be provided.");
        }

        return uri.replace(KEY_METHOD, method) + getParamsString();
    }

    public Object[] getUriVariables() {
        return values.toArray();
    }

    private String getParamsString() {
        return keys.stream()
                .map(key -> "&" + key + "={" + key + "}")
                .collect(Collectors.joining());
    }
}
