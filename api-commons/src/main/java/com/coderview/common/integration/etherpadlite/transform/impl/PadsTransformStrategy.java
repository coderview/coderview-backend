package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import com.coderview.common.integration.etherpadlite.model.Pads;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategy;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.coderview.common.constant.EtherpadLiteConstants.PAD_IDS;

@Component
public class PadsTransformStrategy implements PayloadTransformStrategy<Pads> {

    @Override
    public Pads transform(final GenericEtherpadLiteApiResponse response) {
        return Optional.ofNullable(response.getData())
                .map(this::createPads)
                .orElse(null);
    }

    @Override
    public Class<Pads> getPayloadType() {
        return Pads.class;
    }

    private Pads createPads(final Map<String, Object> data) {
        final Pads pads = new Pads();
        pads.setPadIDs((List) data.get(PAD_IDS));
        return pads;
    }
}
