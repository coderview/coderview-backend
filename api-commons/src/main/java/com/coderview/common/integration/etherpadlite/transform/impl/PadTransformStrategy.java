package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import com.coderview.common.integration.etherpadlite.model.Pad;
import com.coderview.common.integration.etherpadlite.model.PadUser;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategy;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.coderview.common.constant.EtherpadLiteConstants.AUTHOR_IDS;
import static com.coderview.common.constant.EtherpadLiteConstants.COLOR_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.HTML;
import static com.coderview.common.constant.EtherpadLiteConstants.ID;
import static com.coderview.common.constant.EtherpadLiteConstants.LAST_EDITED;
import static com.coderview.common.constant.EtherpadLiteConstants.NAME;
import static com.coderview.common.constant.EtherpadLiteConstants.PAD_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.PAD_USERS;
import static com.coderview.common.constant.EtherpadLiteConstants.PAD_USERS_COUNT;
import static com.coderview.common.constant.EtherpadLiteConstants.PASSWORD_PROTECTION;
import static com.coderview.common.constant.EtherpadLiteConstants.PUBLIC_STATUS;
import static com.coderview.common.constant.EtherpadLiteConstants.READ_ONLY_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.REVISIONS;
import static com.coderview.common.constant.EtherpadLiteConstants.SAVED_REVISIONS;
import static com.coderview.common.constant.EtherpadLiteConstants.TEXT;
import static com.coderview.common.constant.EtherpadLiteConstants.TIMESTAMP;

@Component
public class PadTransformStrategy implements PayloadTransformStrategy<Pad> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Pad transform(final GenericEtherpadLiteApiResponse response) {
        return Optional.ofNullable(response.getData())
                .map(this::createPad)
                .orElse(null);
    }

    @Override
    public Class<Pad> getPayloadType() {
        return Pad.class;
    }

    private Pad createPad(final Map<String, Object> data) {
        final Pad pad = new Pad();
        Optional.ofNullable(data.get(TEXT)).ifPresent(text -> pad.setText(text.toString()));
        Optional.ofNullable(data.get(HTML)).ifPresent(html -> pad.setHtml(html.toString()));
        Optional.ofNullable(data.get(READ_ONLY_ID)).ifPresent(readOnlyID -> pad.setReadOnlyID(readOnlyID.toString()));
        Optional.ofNullable(data.get(PAD_ID)).ifPresent(padID -> pad.setPadID(padID.toString()));
        Optional.ofNullable(data.get(AUTHOR_IDS)).ifPresent(authorIDs -> pad.setAuthorIDs(transformAuthorIDs(authorIDs)));
        Optional.ofNullable(data.get(LAST_EDITED)).ifPresent(lastEdited -> pad.setLastEdited(Long.valueOf(lastEdited.toString())));
        Optional.ofNullable(data.get(REVISIONS))
                .ifPresent(revisions -> pad.setRevisions(Integer.valueOf(revisions.toString())));
        Optional.ofNullable(data.get(SAVED_REVISIONS))
                .ifPresent(savedRevisions -> pad.setSavedRevisions(Integer.valueOf(savedRevisions.toString())));
        Optional.ofNullable(data.get(PAD_USERS_COUNT))
                .ifPresent(padUsersCount -> pad.setPadUsersCount(Integer.valueOf(padUsersCount.toString())));
        Optional.ofNullable(data.get(PAD_USERS))
                .ifPresent(padUsers -> pad.setPadUsers(transformPadUsers(padUsers)));
        Optional.ofNullable(data.get(PUBLIC_STATUS))
                .ifPresent(publicStatus -> pad.setPublicStatus(Boolean.parseBoolean(publicStatus.toString())));
        Optional.ofNullable(data.get(PASSWORD_PROTECTION))
                .ifPresent(passwordProtection -> pad.setPasswordProtection(Boolean.parseBoolean(passwordProtection.toString())));
        return pad;
    }

    private List<PadUser> transformPadUsers(final Object padUsers) {
        return objectToListOfMaps(padUsers).stream()
                .map(this::transformPadUser)
                .collect(Collectors.toList());
    }

    private PadUser transformPadUser(final Map<String, String> values) {
        final PadUser padUser = new PadUser();
        Optional.ofNullable(values.get(COLOR_ID)).ifPresent(padUser::setColorId);
        Optional.ofNullable(values.get(NAME)).ifPresent(padUser::setName);
        Optional.ofNullable(values.get(TIMESTAMP)).ifPresent(timestamp -> padUser.setTimestamp(Long.valueOf(timestamp)));
        Optional.ofNullable(values.get(ID)).ifPresent(padUser::setId);
        return padUser;
    }

    private List<String> transformAuthorIDs(final Object authorIDs) {
        return objectToListOfStrings(authorIDs);
    }

    private List<Map<String, String>> objectToListOfMaps(final Object object) {
        return objectMapper.convertValue(object, new TypeReference<>() {});
    }

    private List<String> objectToListOfStrings(final Object object) {
        return objectMapper.convertValue(object, new TypeReference<>() {});
    }
}
