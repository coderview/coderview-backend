package com.coderview.common.integration.etherpadlite.transform;

import com.coderview.common.integration.etherpadlite.model.BasePayload;
import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;

public interface PayloadTransformStrategy<T extends BasePayload> {

    T transform(GenericEtherpadLiteApiResponse response);

    Class<T> getPayloadType();
}
