package com.coderview.common.integration.etherpadlite.adapter;

import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategyFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static com.coderview.common.constant.EtherpadLiteConstants.STATS_URI;

@Component
public class EtherpadLiteStatisticsAdapter extends EtherpadLiteBaseAdapter {

    public EtherpadLiteStatisticsAdapter(final RestTemplate etherpadLiteApiRestTemplate,
                                         final PayloadTransformStrategyFactory payloadTransformStrategyFactory) {
        super(etherpadLiteApiRestTemplate, payloadTransformStrategyFactory);
    }

    /**
     * Get various statistics of the running Etherpad Lite instance.
     *
     * @return statistics
     */
    public Map<String, Object> stats() {
        return execute(STATS_URI).getData();
    }
}
