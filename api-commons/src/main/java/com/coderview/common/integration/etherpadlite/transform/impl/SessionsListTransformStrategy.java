package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import com.coderview.common.integration.etherpadlite.model.Session;
import com.coderview.common.integration.etherpadlite.model.SessionsList;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategy;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.coderview.common.constant.EtherpadLiteConstants.AUTHOR_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.GROUP_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.VALID_UNTIL;

@Component
public class SessionsListTransformStrategy implements PayloadTransformStrategy<SessionsList> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public SessionsList transform(final GenericEtherpadLiteApiResponse response) {
        return Optional.ofNullable(response.getData())
                .map(this::createSessionsList)
                .orElse(null);
    }

    @Override
    public Class<SessionsList> getPayloadType() {
        return SessionsList.class;
    }

    private SessionsList createSessionsList(final Map<String, Object> data) {
        final SessionsList sessionsList = new SessionsList();

        final List<Session> sessions = data.entrySet().stream()
                .map(this::createSession)
                .collect(Collectors.toList());

        sessionsList.setSessions(sessions);

        return sessionsList;
    }

    private Session createSession(final Map.Entry<String, Object> entry) {
        final Map<String, String> values = objectToMap(entry.getValue());
        final Session session = new Session();
        session.setSessionID(entry.getKey());
        session.setGroupID(values.get(GROUP_ID));
        session.setAuthorID(values.get(AUTHOR_ID));
        session.setValidUntil(Long.valueOf(values.get(VALID_UNTIL)));
        return session;
    }

    private Map<String, String> objectToMap(final Object object) {
        return objectMapper.convertValue(object, new TypeReference<>() {});
    }
}
