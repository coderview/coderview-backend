package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.Author;
import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategy;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

import static com.coderview.common.constant.EtherpadLiteConstants.AUTHOR_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.AUTHOR_NAME;

@Component
public class AuthorTransformStrategy implements PayloadTransformStrategy<Author> {

    @Override
    public Author transform(final GenericEtherpadLiteApiResponse response) {
        return Optional.ofNullable(response.getData())
                .map(this::createAuthor)
                .orElse(null);
    }

    @Override
    public Class<Author> getPayloadType() {
        return Author.class;
    }

    private Author createAuthor(final Map<String, Object> data) {
        final Author author = new Author();
        Optional.ofNullable(data.get(AUTHOR_ID)).ifPresent(authorID -> author.setAuthorID(authorID.toString()));
        Optional.ofNullable(data.get(AUTHOR_NAME)).ifPresent(authorName -> author.setAuthorName(authorName.toString()));
        return author;
    }
}
