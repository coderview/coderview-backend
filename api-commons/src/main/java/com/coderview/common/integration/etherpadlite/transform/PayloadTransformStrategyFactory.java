package com.coderview.common.integration.etherpadlite.transform;

import com.coderview.common.error.CoreCoderviewException;
import com.coderview.common.integration.etherpadlite.model.BasePayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PayloadTransformStrategyFactory {

    private final List<? extends PayloadTransformStrategy<? extends BasePayload>> strategies;

    @Autowired
    public PayloadTransformStrategyFactory(final List<? extends PayloadTransformStrategy<? extends BasePayload>> strategies) {
        this.strategies = strategies;
    }

    public <T extends BasePayload> PayloadTransformStrategy<? extends BasePayload> getTransformStrategy(final Class<T> payloadType) {
        return strategies.stream()
                .filter(strategy -> strategy.getPayloadType().equals(payloadType))
                .findFirst()
                .orElseThrow(() -> new CoreCoderviewException("Payload transform strategy was not found."));
    }
}
