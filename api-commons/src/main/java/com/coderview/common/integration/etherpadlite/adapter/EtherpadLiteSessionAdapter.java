package com.coderview.common.integration.etherpadlite.adapter;

import com.coderview.common.integration.etherpadlite.model.Session;
import com.coderview.common.integration.etherpadlite.model.SessionsList;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategyFactory;
import com.coderview.common.integration.etherpadlite.util.EtherpadLiteUriBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class EtherpadLiteSessionAdapter extends EtherpadLiteBaseAdapter {

    @Value("${etherpadLite.server.http.apiVersion:}")
    private String apiVersion;

    @Value("${etherpadLite.server.http.apikey:}")
    private String apiKey;

    public EtherpadLiteSessionAdapter(final RestTemplate etherpadLiteApiRestTemplate,
                                      final PayloadTransformStrategyFactory payloadTransformStrategyFactory) {
        super(etherpadLiteApiRestTemplate, payloadTransformStrategyFactory);
    }

    /**
     * Creates a new session. validUntil is an unix timestamp in seconds.
     *
     * @param groupID group id.
     * @param authorID author id.
     * @param validUntil unix timestamp in seconds specifying when sessions is to be expired.
     * @return session that was created
     */
    public Session createSession(final String groupID, final String authorID, final long validUntil) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("createSession")
                .addParam("groupID", groupID)
                .addParam("authorID", authorID)
                .addParam("validUntil", String.valueOf(validUntil));

        return execute(builder.getUri(), Session.class, builder.getUriVariables());
    }

    /**
     * Deletes a session.
     *
     * @param sessionID session id to be deleted.
     */
    public void deleteSession(final String sessionID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("deleteSession")
                .addParam("sessionID", sessionID);

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Returns information about a session.
     *
     * @param sessionID session id.
     * @return session information
     */
    public Session getSessionInfo(final String sessionID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("getSessionInfo")
                .addParam("sessionID", sessionID);

        return execute(builder.getUri(), Session.class, builder.getUriVariables());
    }

    /**
     * Returns all sessions of a group.
     *
     * @param groupID group id.
     * @return a list of all sessions of this group
     */
    public List<Session> listSessionsOfGroup(final String groupID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("listSessionsOfGroup")
                .addParam("groupID", groupID);

        return execute(builder.getUri(), SessionsList.class, builder.getUriVariables()).getSessions();
    }

    /**
     * Returns all sessions of an author.
     *
     * @param authorID author id.
     * @return a list of all sessions of this author
     */
    public List<Session> listSessionsOfAuthor(final String authorID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("listSessionsOfAuthor")
                .addParam("authorID", authorID);

        return execute(builder.getUri(), SessionsList.class, builder.getUriVariables()).getSessions();
    }
}
