package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import com.coderview.common.integration.etherpadlite.model.Group;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategy;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

import static com.coderview.common.constant.EtherpadLiteConstants.GROUP_ID;

@Component
public class GroupTransformStrategy implements PayloadTransformStrategy<Group> {

    @Override
    public Group transform(final GenericEtherpadLiteApiResponse response) {
        return Optional.ofNullable(response.getData())
                .map(this::createGroup)
                .orElse(null);
    }

    @Override
    public Class<Group> getPayloadType() {
        return Group.class;
    }

    private Group createGroup(final Map<String, Object> data) {
        final Group group = new Group();
        group.setGroupID(data.get(GROUP_ID).toString());
        return group;
    }
}
