package com.coderview.common.integration.etherpadlite.adapter;

import com.coderview.common.integration.etherpadlite.model.Group;
import com.coderview.common.integration.etherpadlite.model.Groups;
import com.coderview.common.integration.etherpadlite.model.Pads;
import com.coderview.common.integration.etherpadlite.transform.PayloadTransformStrategyFactory;
import com.coderview.common.integration.etherpadlite.util.EtherpadLiteUriBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class EtherpadLiteGroupAdapter extends EtherpadLiteBaseAdapter {

    @Value("${etherpadLite.server.http.apiVersion:}")
    private String apiVersion;

    @Value("${etherpadLite.server.http.apikey:}")
    private String apiKey;

    public EtherpadLiteGroupAdapter(final RestTemplate etherpadLiteApiRestTemplate,
                                    final PayloadTransformStrategyFactory payloadTransformStrategyFactory) {
        super(etherpadLiteApiRestTemplate, payloadTransformStrategyFactory);
    }

    /**
     * Creates a new group.
     *
     * @return group that was created.
     */
    public Group createGroup() {
        final String uri = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("createGroup")
                .getUri();

        return execute(uri, Group.class);
    }

    /**
     * This method allows to map application specific group id to Etherpad lite group id.
     *
     * @param groupMapper application specific group id.
     * @return group that was created or already existing group.
     */
    public Group createGroupIfNotExistsFor(final String groupMapper) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("createGroupIfNotExistsFor")
                .addParam("groupMapper", groupMapper);

        return execute(builder.getUri(), Group.class, builder.getUriVariables());
    }

    /**
     * Deletes a group.
     *
     * @param groupID group id to delete.
     */
    public void deleteGroup(final String groupID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("deleteGroup")
                .addParam("groupID", groupID);

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Returns all pads of this group.
     *
     * @param groupID group id.
     * @return a list of pad ids
     */
    public List<String> listPads(final String groupID) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("listPads")
                .addParam("groupID", groupID);

        return execute(builder.getUri(), Pads.class, builder.getUriVariables()).getPadIDs();
    }

    /**
     * Creates a new pad (without predefined text) in this group.
     *
     * @param groupID group id.
     * @param padName pad name.
     */
    public void createGroupPad(final String groupID, final String padName) {
        createGroupPad(groupID, padName, StringUtils.EMPTY);
    }

    /**
     * Creates a new pad in this group.
     *
     * @param groupID group id.
     * @param padName pad name.
     * @param text text for the pad.
     */
    public void createGroupPad(final String groupID, final String padName, final String text) {
        final EtherpadLiteUriBuilder builder = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("createGroupPad")
                .addParam("groupID", groupID)
                .addParam("padName", padName)
                .addParam("text", text);

        execute(builder.getUri(), builder.getUriVariables());
    }

    /**
     * Lists all existing groups.
     *
     * @return a list of group ids
     */
    public List<String> listAllGroups() {
        final String uri = EtherpadLiteUriBuilder.newBuilder(apiVersion, apiKey)
                .method("listAllGroups")
                .getUri();

        return execute(uri, Groups.class).getGroupIDs();
    }
}
