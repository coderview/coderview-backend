package com.coderview.common.integration.etherpadlite.model;

public class Session extends BasePayload {

    private String sessionID;
    private String authorID;
    private String groupID;
    private Long validUntil;

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(final String sessionID) {
        this.sessionID = sessionID;
    }

    public String getAuthorID() {
        return authorID;
    }

    public void setAuthorID(final String authorID) {
        this.authorID = authorID;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(final String groupID) {
        this.groupID = groupID;
    }

    public Long getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(final Long validUntil) {
        this.validUntil = validUntil;
    }
}
