package com.coderview.common.error;

public class UnsupportedProgrammingLanguageException extends RuntimeException {

    private static final String KEY_LANGUAGE = "{{language}}";

    private static final String ERROR_MESSAGE_TEMPLATE = "Unsupported programming language {{language}}.";

    public UnsupportedProgrammingLanguageException(final String language) {
        super(ERROR_MESSAGE_TEMPLATE.replace(KEY_LANGUAGE, language));
    }
}
