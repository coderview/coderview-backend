package com.coderview.common.error;

public class CoreCoderviewException extends RuntimeException {

    public CoreCoderviewException(final String message) {
        super(message);
    }

    public CoreCoderviewException(final Throwable cause) {
        super(cause);
    }
}
