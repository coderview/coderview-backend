package com.coderview.common.error;

public class InvalidEtherpadLiteResponseCodeException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "code: %d, code meaning: %s, message: %s";

    public InvalidEtherpadLiteResponseCodeException(final Integer code, final String codeMeaning, final String message) {
        this(String.format(MESSAGE_TEMPLATE, code, codeMeaning, message));
    }

    public InvalidEtherpadLiteResponseCodeException(final String message) {
        super(message);
    }
}
