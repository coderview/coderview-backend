package com.coderview.common.solr.document;

import org.apache.solr.client.solrj.beans.Field;

import java.util.Date;

public class ChallengeDocument extends Document {

    private String id;

    private String title;

    private String description;

    private String scope;

    private String difficulty;

    private String type;

    private String owner;

    private String programmingLanguage;

    private Date lastModifiedDate;

    public ChallengeDocument() {
    }

    public String getId() {
        return id;
    }

    @Field("id")
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    @Field("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    @Field("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getScope() {
        return scope;
    }

    @Field("scope")
    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getDifficulty() {
        return difficulty;
    }

    @Field("difficulty")
    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getType() {
        return type;
    }

    @Field("type")
    public void setType(String type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    @Field("owner")
    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    @Field("programmingLanguage")
    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    @Field("lastModifiedDate")
    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
