package com.coderview.common.solr.document;

import org.apache.solr.client.solrj.beans.Field;

import java.util.Date;

public class InterviewDocument extends Document {

    private String id;

    private String title;

    private String status;

    private Date scheduledFor;

    private Date lastEditedAt;

    private String candidateFirstName;

    private String candidateLastName;

    private String candidateEmail;

    private String interviewer;

    private String link;

    public InterviewDocument() {
    }

    public String getId() {
        return id;
    }

    @Field("id")
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    @Field("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    @Field("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public Date getScheduledFor() {
        return scheduledFor;
    }

    @Field("scheduledFor")
    public void setScheduledFor(Date scheduledFor) {
        this.scheduledFor = scheduledFor;
    }

    public Date getLastEditedAt() {
        return lastEditedAt;
    }

    @Field("lastEditedAt")
    public void setLastEditedAt(Date lastEditedAt) {
        this.lastEditedAt = lastEditedAt;
    }

    public String getCandidateFirstName() {
        return candidateFirstName;
    }

    @Field("candidateFirstName")
    public void setCandidateFirstName(String candidateFirstName) {
        this.candidateFirstName = candidateFirstName;
    }

    public String getCandidateLastName() {
        return candidateLastName;
    }

    @Field("candidateLastName")
    public void setCandidateLastName(String candidateLastName) {
        this.candidateLastName = candidateLastName;
    }

    public String getCandidateEmail() {
        return candidateEmail;
    }

    @Field("candidateEmail")
    public void setCandidateEmail(String candidateEmail) {
        this.candidateEmail = candidateEmail;
    }

    public String getInterviewer() {
        return interviewer;
    }

    @Field("interviewer")
    public void setInterviewer(String interviewer) {
        this.interviewer = interviewer;
    }

    public String getLink() {
        return link;
    }

    @Field("link")
    public void setLink(final String link) {
        this.link = link;
    }
}
