package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.Author;
import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static com.coderview.common.constant.EtherpadLiteConstants.AUTHOR_ID;
import static com.coderview.common.constant.EtherpadLiteConstants.AUTHOR_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(JUnit4.class)
public class AuthorTransformStrategyTest {

    private static final String AUTHOR_ID_VALUE = "id123";
    private static final String AUTHOR_NAME_VALUE = "name123";

    private GenericEtherpadLiteApiResponse response;

    private Map<String, Object> data;

    private AuthorTransformStrategy authorTransformStrategy = new AuthorTransformStrategy();

    @Before
    public void setUp() {
        data = new HashMap<>();
        response = new GenericEtherpadLiteApiResponse();
        response.setData(data);
    }

    @Test
    public void shouldTransformAuthorWithAllProperties() {
        data.put(AUTHOR_ID, AUTHOR_ID_VALUE);
        data.put(AUTHOR_NAME, AUTHOR_NAME_VALUE);

        Author author = authorTransformStrategy.transform(response);

        assertEquals(AUTHOR_ID_VALUE, author.getAuthorID());
        assertEquals(AUTHOR_NAME_VALUE, author.getAuthorName());
    }

    @Test
    public void shouldReturnNullGivenNullData() {
        response.setData(null);

        Author author = authorTransformStrategy.transform(response);

        assertNull(author);
    }

    @Test
    public void shouldNotFailGivenEmptyData() {
        Author author = authorTransformStrategy.transform(response);

        assertNotNull(author);
        assertNull(author.getAuthorID());
        assertNull(author.getAuthorName());
    }

    @Test
    public void shouldSetAuthorIdOnly() {
        data.put(AUTHOR_ID, AUTHOR_ID_VALUE);

        Author author = authorTransformStrategy.transform(response);

        assertEquals(AUTHOR_ID_VALUE, author.getAuthorID());
        assertNull(author.getAuthorName());
    }

    @Test
    public void shouldSetAuthorNameOnly() {
        data.put(AUTHOR_NAME, AUTHOR_NAME_VALUE);

        Author author = authorTransformStrategy.transform(response);

        assertEquals(AUTHOR_NAME_VALUE, author.getAuthorName());
        assertNull(author.getAuthorID());
    }
}