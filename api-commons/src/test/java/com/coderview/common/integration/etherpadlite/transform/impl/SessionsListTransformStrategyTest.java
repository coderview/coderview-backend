package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class SessionsListTransformStrategyTest {

    private SessionsListTransformStrategy sessionsListTransformStrategy = new SessionsListTransformStrategy();

    private GenericEtherpadLiteApiResponse response;

    private Map<String, Object> data;

    @Before
    public void setUp() throws Exception {
        data = new HashMap<>();
        response = new GenericEtherpadLiteApiResponse();
        response.setData(data);
    }

    @Test
    public void transform() {
        sessionsListTransformStrategy.transform(response);
    }

    @Test
    public void transformEmpty() {
        sessionsListTransformStrategy.transform(new GenericEtherpadLiteApiResponse());
    }

    @Test(expected = NullPointerException.class)
    public void transformNull() {
        sessionsListTransformStrategy.transform(null);
    }

    @Test
    public void getPayloadType() {
        sessionsListTransformStrategy.getPayloadType();
    }
}