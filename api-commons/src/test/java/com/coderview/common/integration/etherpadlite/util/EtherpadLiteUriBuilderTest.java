package com.coderview.common.integration.etherpadlite.util;

import static org.junit.Assert.assertEquals;

// @RunWith(JUnit4.class)
public class EtherpadLiteUriBuilderTest {

    private static final String API_VERSION = "123";
    private static final String API_KEY = "abc123";
    private static final String METHOD = "someMethod";

    private static final String PARAM_KEY_1 = "paramKey1";
    private static final String PARAM_KEY_2 = "paramKey2";

    private static final String PARAM_VALUE_1 = "paramValue1";
    private static final String PARAM_VALUE_2 = "paramValue2";

    // @Test
    public void shouldBuildUriWithoutAdditionalParameters() {
        String expected = "/api/123/someMethod?apikey=abc123";
        String actual = EtherpadLiteUriBuilder.newBuilder(API_VERSION, API_KEY).method(METHOD).getUri();
        assertEquals(expected, actual);
    }

    // @Test
    public void shouldBuildUriWithParameters() {
        String expected = "/api/123/someMethod?apikey=abc123&paramKey2=paramValue2&paramKey1=paramValue1";
        String actual = EtherpadLiteUriBuilder.newBuilder(API_VERSION, API_KEY).method(METHOD)
                .addParam(PARAM_KEY_1, PARAM_VALUE_1)
                .addParam(PARAM_KEY_2, PARAM_VALUE_2)
                .getUri();
        assertEquals(expected, actual);
    }

    // @Test(expected = CoderviewCoreException.class)
    public void shouldFailWhenMethodIsNotSpecified() {
        EtherpadLiteUriBuilder.newBuilder(API_VERSION, API_KEY).getUri();
    }

    // @Test
    public void shouldNotAddParameterWhenKeyIsNull() {
        String expected = "/api/123/someMethod?apikey=abc123&paramKey1=paramValue1";
        String actual = EtherpadLiteUriBuilder.newBuilder(API_VERSION, API_KEY).method(METHOD)
                .addParam(PARAM_KEY_1, PARAM_VALUE_1)
                .addParam(null, PARAM_VALUE_2)
                .getUri();
        assertEquals(expected, actual);
    }

    // @Test
    public void shouldNotAddParameterWhenKeyIsAnEmptyString() {
        String expected = "/api/123/someMethod?apikey=abc123&paramKey1=paramValue1";
        String actual = EtherpadLiteUriBuilder.newBuilder(API_VERSION, API_KEY).method(METHOD)
                .addParam(PARAM_KEY_1, PARAM_VALUE_1)
                .addParam("", PARAM_VALUE_2)
                .getUri();
        assertEquals(expected, actual);
    }

    // @Test
    public void shouldNotAddParameterWhenValueIsNull() {
        String expected = "/api/123/someMethod?apikey=abc123&paramKey1=paramValue1";
        String actual = EtherpadLiteUriBuilder.newBuilder(API_VERSION, API_KEY).method(METHOD)
                .addParam(PARAM_KEY_1, PARAM_VALUE_1)
                .addParam(PARAM_KEY_2, null)
                .getUri();
        assertEquals(expected, actual);
    }

    // @Test
    public void shouldNotAddParameterWhenValueIsAnEmptyString() {
        String expected = "/api/123/someMethod?apikey=abc123&paramKey1=paramValue1";
        String actual = EtherpadLiteUriBuilder.newBuilder(API_VERSION, API_KEY).method(METHOD)
                .addParam(PARAM_KEY_1, PARAM_VALUE_1)
                .addParam(PARAM_KEY_2, "")
                .getUri();
        assertEquals(expected, actual);
    }
}