package com.coderview.common.integration.etherpadlite.transform;

import com.coderview.common.error.CoreCoderviewException;
import com.coderview.common.integration.etherpadlite.model.Author;
import com.coderview.common.integration.etherpadlite.model.Group;
import com.coderview.common.integration.etherpadlite.model.Pad;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PayloadTransformStrategyFactoryTest {

    @Mock
    private PayloadTransformStrategy<Pad> padTransformStrategy;

    @Mock
    private PayloadTransformStrategy<Group> groupTransformStrategy;

    private PayloadTransformStrategyFactory payloadTransformStrategyFactory;

    @Before
    public void setUp() {
        payloadTransformStrategyFactory = new PayloadTransformStrategyFactory(Arrays.asList(
                padTransformStrategy, groupTransformStrategy
        ));

        when(padTransformStrategy.getPayloadType()).thenReturn(Pad.class);
        when(groupTransformStrategy.getPayloadType()).thenReturn(Group.class);
    }

    @Test
    public void shouldResolveTransformStrategiesGivenValidPayloadTypes() {
        assertEquals(padTransformStrategy, payloadTransformStrategyFactory.getTransformStrategy(Pad.class));
        assertEquals(groupTransformStrategy, payloadTransformStrategyFactory.getTransformStrategy(Group.class));
    }

    @Test(expected = CoreCoderviewException.class)
    public void shouldThrowExceptionWhenStrategyCannotBeResolved() {
        payloadTransformStrategyFactory.getTransformStrategy(Author.class);
    }
}