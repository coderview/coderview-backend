package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static com.coderview.common.constant.EtherpadLiteConstants.GROUP_ID;

@RunWith(JUnit4.class)
public class GroupTransformStrategyTest {

    private GroupTransformStrategy groupTransformStrategy = new GroupTransformStrategy();

    private GenericEtherpadLiteApiResponse response;

    private Map<String, Object> data;

    @Before
    public void setUp() throws Exception {
        data = new HashMap<>();
        response = new GenericEtherpadLiteApiResponse();
        response.setData(data);
        data.put(GROUP_ID, "group1");
    }

    @Test
    public void transform() {
        groupTransformStrategy.transform(response);
    }

    @Test
    public void transformEmpty() {
        groupTransformStrategy.transform(new GenericEtherpadLiteApiResponse());
    }

    @Test(expected = NullPointerException.class)
    public void transformNull() {
        groupTransformStrategy.transform(null);
    }

    @Test
    public void getPayloadType() {
        groupTransformStrategy.getPayloadType();
    }
}