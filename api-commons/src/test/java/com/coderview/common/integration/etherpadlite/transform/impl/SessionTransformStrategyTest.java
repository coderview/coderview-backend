package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class SessionTransformStrategyTest {

    private SessionTransformStrategy sessionTransformStrategy = new SessionTransformStrategy();

    private GenericEtherpadLiteApiResponse response;

    private Map<String, Object> data;

    @Before
    public void setUp() throws Exception {
        data = new HashMap<>();
        response = new GenericEtherpadLiteApiResponse();
        response.setData(data);
    }

    @Test
    public void transform() {
        sessionTransformStrategy.transform(response);
    }

    @Test
    public void transformEmpty() {
        sessionTransformStrategy.transform(new GenericEtherpadLiteApiResponse());
    }

    @Test(expected = NullPointerException.class)
    public void transformNull() {
        sessionTransformStrategy.transform(null);
    }

    @Test
    public void getPayloadType() {
        sessionTransformStrategy.getPayloadType();
    }
}