package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class PadsTransformStrategyTest {

    private PadsTransformStrategy padsTransformStrategy = new PadsTransformStrategy();

    private GenericEtherpadLiteApiResponse response;

    private Map<String, Object> data;

    @Before
    public void setUp() throws Exception {
        data = new HashMap<>();
        response = new GenericEtherpadLiteApiResponse();
        response.setData(data);
    }

    @Test
    public void transform() {
        padsTransformStrategy.transform(response);
    }

    @Test
    public void transformEmpty() {
        padsTransformStrategy.transform(new GenericEtherpadLiteApiResponse());
    }

    @Test(expected = NullPointerException.class)
    public void transformNull() {
        padsTransformStrategy.transform(null);
    }

    @Test
    public void getPayloadType() {
        padsTransformStrategy.getPayloadType();
    }
}