package com.coderview.common.integration.etherpadlite.transform.impl;

import com.coderview.common.integration.etherpadlite.model.GenericEtherpadLiteApiResponse;
import com.coderview.common.integration.etherpadlite.model.Groups;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.coderview.common.constant.EtherpadLiteConstants.GROUP_IDS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(JUnit4.class)
public class GroupsTransformStrategyTest {

    private List<String> groupIds = Arrays.asList("1", "2", "3");

    private GenericEtherpadLiteApiResponse response;

    private Map<String, Object> data;

    private GroupsTransformStrategy groupsTransformStrategy = new GroupsTransformStrategy();

    @Before
    public void setUp() {
        data = new HashMap<>();
        response = new GenericEtherpadLiteApiResponse();
        response.setData(data);
    }

    @Test
    public void shouldTransformGroups() {
        data.put(GROUP_IDS, groupIds);

        Groups groups = groupsTransformStrategy.transform(response);

        assertEquals(groupIds, groups.getGroupIDs());
    }

    @Test
    public void shouldReturnNullGivenNullData() {
        response.setData(null);

        Groups groups = groupsTransformStrategy.transform(response);

        assertNull(groups);
    }

    @Test
    public void shouldNotFailGivenEmptyData() {
        Groups groups = groupsTransformStrategy.transform(response);

        assertNotNull(groups);
        assertNull(groups.getGroupIDs());
    }
}