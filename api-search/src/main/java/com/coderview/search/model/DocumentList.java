package com.coderview.search.model;

import com.coderview.common.solr.document.Document;

import java.util.List;

public class DocumentList<T extends Document> {

    private long numFound;

    private List<T> entries;

    public DocumentList() {
    }

    public long getNumFound() {
        return numFound;
    }

    public DocumentList<T> setNumFound(final long numFound) {
        this.numFound = numFound;
        return this;
    }

    public List<T> getEntries() {
        return entries;
    }

    public DocumentList<T> setEntries(final List<T> documents) {
        this.entries = documents;
        return this;
    }
}
