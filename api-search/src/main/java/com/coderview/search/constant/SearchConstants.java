package com.coderview.search.constant;

public class SearchConstants {

    public static final String SEARCH_PARAM_KEY = "_search";
    public static final String PAGE_PARAM_KEY = "_page";
    public static final String ROWS_PARAM_KEY = "_rows";
    public static final String FILTER_PARAM_KEY = "_filter";
    public static final String SORT_PARAM_KEY = "_sort";

    public static final String TEXT_SEARCH_FIELD = "content";

    public static final String STATUS = "status";
    public static final String TITLE = "title";
    public static final String CANDIDATE = "candidate";
    public static final String SCHEDULED_FOR = "scheduledFor";

    public static final String TYPE = "type";
    public static final String DIFFICULTY = "difficulty";
    public static final String SCOPE = "scope";
    public static final String PROGRAMMING_LANGUAGE = "programmingLanguage";
    public static final String LAST_MODIFIED_DATE = "lastModifiedDate";

    public static final String SOLR_FILTER_STATUS = "status";
    public static final String SOLR_FILTER_TYPE = "type";
    public static final String SOLR_FILTER_DIFFICULTY = "difficulty";
    public static final String SOLR_FILTER_PROGRAMMING_LANGUAGE = "programmingLanguage";
    public static final String SOLR_SORT_TITLE = "title";
    public static final String SOLR_SORT_CANDIDATE = "candidateFirstName";
    public static final String SOLR_SORT_STATUS = "status";
    public static final String SOLR_SORT_SCHEDULED_FOR = "scheduledFor";
    public static final String SOLR_SORT_LAST_MODIFIED_DATE = "lastModifiedDate";

    public static final String INTERVIEWER = "interviewer";
    public static final String OWNER = "owner";

    private SearchConstants() {
    }
}
