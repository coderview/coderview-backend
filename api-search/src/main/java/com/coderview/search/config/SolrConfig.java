package com.coderview.search.config;

import com.coderview.common.constant.SolrConstants;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SolrConfig {

    @Value("${solr.baseUrl}")
    private String solrBaseUrl;

    @Bean
    public HttpSolrClient interviewsSolrClient() {
        return new HttpSolrClient
                .Builder(solrBaseUrl.replace(SolrConstants.INDEX_KEY, SolrConstants.INTERVIEW_INDEX))
                .build();
    }

    @Bean
    public HttpSolrClient challengesSolrClient() {
        return new HttpSolrClient
                .Builder(solrBaseUrl.replace(SolrConstants.INDEX_KEY, SolrConstants.CHALLENGE_INDEX))
                .build();
    }
}
