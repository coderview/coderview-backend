package com.coderview.search.util;

import com.coderview.search.criteria.SearchCriteria;
import com.coderview.search.criteria.filter.Filter;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;

import java.util.ArrayList;
import java.util.List;

import static com.coderview.search.constant.SearchConstants.TEXT_SEARCH_FIELD;

public class SearchQueryBuilder {

    private static final String EVERYTHING = "*:*";

    private final SearchCriteria criteria;

    private final List<String> additionalFilterQueries = new ArrayList<>();

    private SearchQueryBuilder(final SearchCriteria criteria) {
        this.criteria = criteria;
    }

    public static SearchQueryBuilder from(final SearchCriteria criteria) {
        return new SearchQueryBuilder(criteria);
    }

    public SearchQueryBuilder withAdditionalFilterQuery(final String field, final String value) {
        additionalFilterQueries.add(field + ":" + value);
        return this;
    }

    public SearchQueryBuilder withAdditionalFilterQueryIfTrue(final boolean condition, final String field, final String value) {
        if (condition) {
            additionalFilterQueries.add(field + ":" + value);
        }
        return this;
    }

    public SolrQuery build() {
        final SolrQuery solrQuery = new SolrQuery();

        solrQuery.setQuery(getMainQueryExpression(criteria.getSearch()));

        criteria.getFilterCriteria().stream()
                .map(Filter::getExpression)
                .forEach(solrQuery::addFilterQuery);

        additionalFilterQueries.forEach(solrQuery::addFilterQuery);

        criteria.getSortCriteria().stream()
                .forEach(sort -> solrQuery.addSort(sort.getField(), sort.getOrder()));

        solrQuery.setStart((criteria.getPage() - 1) * criteria.getRows());
        solrQuery.setRows(criteria.getRows());

        return solrQuery;
    }

    private String getMainQueryExpression(final String search) {
        return StringUtils.isBlank(search) ? EVERYTHING : TEXT_SEARCH_FIELD + ":" + search;
    }
}
