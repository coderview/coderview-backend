package com.coderview.search.controller;

import com.coderview.common.constant.ChallengeScope;
import com.coderview.common.solr.document.ChallengeDocument;
import com.coderview.common.solr.document.InterviewDocument;
import com.coderview.search.criteria.SearchCriteria;
import com.coderview.search.criteria.filter.ChallengesFilter;
import com.coderview.search.criteria.filter.InterviewsFilter;
import com.coderview.search.criteria.sort.ChallengesSort;
import com.coderview.search.criteria.sort.InterviewsSort;
import com.coderview.search.model.DocumentList;
import com.coderview.search.service.SearchService;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static com.coderview.search.constant.SearchConstants.FILTER_PARAM_KEY;
import static com.coderview.search.constant.SearchConstants.PAGE_PARAM_KEY;
import static com.coderview.search.constant.SearchConstants.ROWS_PARAM_KEY;
import static com.coderview.search.constant.SearchConstants.SEARCH_PARAM_KEY;
import static com.coderview.search.constant.SearchConstants.SORT_PARAM_KEY;

@RestController
public class SearchController {

    private final SearchService searchService;

    @Autowired
    public SearchController(final SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping(path = "/search/interviews", produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentList<InterviewDocument> getInterviews(final @RequestParam(value = PAGE_PARAM_KEY, defaultValue = "1") int page,
                                                         final @RequestParam(value = ROWS_PARAM_KEY, defaultValue = "10") int rows,
                                                         final @RequestParam(value = SEARCH_PARAM_KEY, required = false) String search,
                                                         final @RequestParam(value = FILTER_PARAM_KEY, required = false) List<InterviewsFilter> filterCriteria,
                                                         final @RequestParam(value = SORT_PARAM_KEY, required = false) List<InterviewsSort> sortCriteria) throws IOException, SolrServerException {
        final SearchCriteria criteria = new SearchCriteria(page, rows, search, filterCriteria, sortCriteria);
        return searchService.getInterviews(criteria);
    }

    @GetMapping(path = "/search/challenges/{scope}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DocumentList<ChallengeDocument> getChallenges(final @PathVariable ChallengeScope scope,
                                                         final @RequestParam(value = PAGE_PARAM_KEY, defaultValue = "1") int page,
                                                         final @RequestParam(value = ROWS_PARAM_KEY, defaultValue = "10") int rows,
                                                         final @RequestParam(value = SEARCH_PARAM_KEY, required = false) String search,
                                                         final @RequestParam(value = FILTER_PARAM_KEY, required = false) List<ChallengesFilter> filterCriteria,
                                                         final @RequestParam(value = SORT_PARAM_KEY, required = false) List<ChallengesSort> sortCriteria) throws IOException, SolrServerException {
        final SearchCriteria criteria = new SearchCriteria(page, rows, search, filterCriteria, sortCriteria);
        return searchService.getChallenges(criteria, scope);
    }
}
