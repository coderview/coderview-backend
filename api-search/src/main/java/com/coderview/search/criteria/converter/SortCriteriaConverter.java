package com.coderview.search.criteria.converter;

import com.coderview.search.criteria.sort.Sort;
import org.apache.solr.client.solrj.SolrQuery;
import org.springframework.core.convert.converter.Converter;

public abstract class SortCriteriaConverter<T extends Sort> implements Converter<String, T> {

    @Override
    public T convert(final String input) {
        final String[] parts = input.split(":");

        final String field = parts[0];
        final SolrQuery.ORDER order = parts[1].equals("asc") ? SolrQuery.ORDER.asc : SolrQuery.ORDER.desc;

        final T sort = createSort();

        sort.setField(convertField(field));
        sort.setOrder(order);

        return sort;
    }

    protected abstract T createSort();

    protected abstract String convertField(String field);
}
