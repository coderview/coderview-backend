package com.coderview.search.criteria.converter;

import com.coderview.search.criteria.sort.ChallengesSort;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.coderview.search.constant.SearchConstants.LAST_MODIFIED_DATE;
import static com.coderview.search.constant.SearchConstants.SOLR_SORT_LAST_MODIFIED_DATE;

@Component
public class ChallengesSortCriteriaConverter extends SortCriteriaConverter<ChallengesSort> {

    private static final Map<String, String> MAP = new HashMap<>() {{
        put(LAST_MODIFIED_DATE, SOLR_SORT_LAST_MODIFIED_DATE);
    }};

    @Override
    protected ChallengesSort createSort() {
        return new ChallengesSort();
    }

    @Override
    protected String convertField(final String field) {
        return MAP.getOrDefault(field, field);
    }
}
