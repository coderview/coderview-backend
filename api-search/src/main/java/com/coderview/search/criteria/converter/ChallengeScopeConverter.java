package com.coderview.search.criteria.converter;

import com.coderview.common.constant.ChallengeScope;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ChallengeScopeConverter implements Converter<String, ChallengeScope> {

    @Override
    public ChallengeScope convert(final String scope) {
        return ChallengeScope.valueOf(scope.toUpperCase());
    }
}
