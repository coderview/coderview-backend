package com.coderview.search.criteria.sort;

import org.apache.solr.client.solrj.SolrQuery;

public class Sort {

    private String field;

    private SolrQuery.ORDER order;

    public Sort() {}

    public String getField() {
        return field;
    }

    public void setField(final String field) {
        this.field = field;
    }

    public SolrQuery.ORDER getOrder() {
        return order;
    }

    public void setOrder(final SolrQuery.ORDER order) {
        this.order = order;
    }
}
