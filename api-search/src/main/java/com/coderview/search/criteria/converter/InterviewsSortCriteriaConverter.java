package com.coderview.search.criteria.converter;

import com.coderview.search.criteria.sort.InterviewsSort;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.coderview.search.constant.SearchConstants.CANDIDATE;
import static com.coderview.search.constant.SearchConstants.SCHEDULED_FOR;
import static com.coderview.search.constant.SearchConstants.SOLR_SORT_CANDIDATE;
import static com.coderview.search.constant.SearchConstants.SOLR_SORT_SCHEDULED_FOR;
import static com.coderview.search.constant.SearchConstants.SOLR_SORT_STATUS;
import static com.coderview.search.constant.SearchConstants.SOLR_SORT_TITLE;
import static com.coderview.search.constant.SearchConstants.STATUS;
import static com.coderview.search.constant.SearchConstants.TITLE;

@Component
public class InterviewsSortCriteriaConverter extends SortCriteriaConverter<InterviewsSort> {

    private static final Map<String, String> MAP = new HashMap<>() {{
        put(TITLE, SOLR_SORT_TITLE);
        put(CANDIDATE, SOLR_SORT_CANDIDATE);
        put(STATUS, SOLR_SORT_STATUS);
        put(SCHEDULED_FOR, SOLR_SORT_SCHEDULED_FOR);
    }};

    @Override
    protected InterviewsSort createSort() {
        return new InterviewsSort();
    }

    @Override
    protected String convertField(final String field) {
        return MAP.getOrDefault(field, field);
    }
}
