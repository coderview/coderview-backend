package com.coderview.search.criteria.converter;

import com.coderview.search.criteria.filter.InterviewsFilter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.coderview.search.constant.SearchConstants.SOLR_FILTER_STATUS;
import static com.coderview.search.constant.SearchConstants.STATUS;

@Component
public class InterviewsFilterCriteriaConverter extends FilterCriteriaConverter<InterviewsFilter> {

    private static final Map<String, String> MAP = new HashMap<>() {{
        put(STATUS, SOLR_FILTER_STATUS);
    }};

    @Override
    protected InterviewsFilter createFilter() {
        return new InterviewsFilter();
    }

    @Override
    protected String convertField(final String field) {
        return MAP.getOrDefault(field, field);
    }
}
