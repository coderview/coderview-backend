package com.coderview.search.criteria;

import com.coderview.search.criteria.filter.Filter;
import com.coderview.search.criteria.sort.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SearchCriteria {

    private String search;

    private int page;

    private int rows;

    private List<? extends Filter> filterCriteria;

    private List<? extends Sort> sortCriteria;

    public SearchCriteria(final int page, final int rows, final String search, final List<? extends Filter> filterCriteria, final List<? extends Sort> sortCriteria) {
        this.page = page;
        this.rows = rows;
        this.search = search;
        this.filterCriteria = Objects.isNull(filterCriteria) ? new ArrayList<>() : filterCriteria;
        this.sortCriteria = Objects.isNull(sortCriteria) ? new ArrayList<>() : sortCriteria;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(final String search) {
        this.search = search;
    }

    public int getPage() {
        return page;
    }

    public void setPage(final int page) {
        this.page = page;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public List<? extends Filter> getFilterCriteria() {
        return filterCriteria;
    }

    public void setFilterCriteria(final List<? extends Filter> filterCriteria) {
        this.filterCriteria = filterCriteria;
    }

    public List<? extends Sort> getSortCriteria() {
        return sortCriteria;
    }

    public void setSortCriteria(final List<Sort> sortCriteria) {
        this.sortCriteria = sortCriteria;
    }
}
