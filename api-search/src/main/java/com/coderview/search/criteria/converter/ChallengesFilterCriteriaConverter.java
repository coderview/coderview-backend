package com.coderview.search.criteria.converter;

import com.coderview.search.criteria.filter.ChallengesFilter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.coderview.search.constant.SearchConstants.DIFFICULTY;
import static com.coderview.search.constant.SearchConstants.PROGRAMMING_LANGUAGE;
import static com.coderview.search.constant.SearchConstants.SOLR_FILTER_DIFFICULTY;
import static com.coderview.search.constant.SearchConstants.SOLR_FILTER_PROGRAMMING_LANGUAGE;
import static com.coderview.search.constant.SearchConstants.SOLR_FILTER_TYPE;
import static com.coderview.search.constant.SearchConstants.TYPE;

@Component
public class ChallengesFilterCriteriaConverter extends FilterCriteriaConverter<ChallengesFilter> {

    private static final Map<String, String> MAP = new HashMap<>() {{
        put(TYPE, SOLR_FILTER_TYPE);
        put(DIFFICULTY, SOLR_FILTER_DIFFICULTY);
        put(PROGRAMMING_LANGUAGE, SOLR_FILTER_PROGRAMMING_LANGUAGE);
    }};

    @Override
    protected ChallengesFilter createFilter() {
        return new ChallengesFilter();
    }

    @Override
    protected String convertField(final String field) {
        return MAP.getOrDefault(field, field);
    }
}
