package com.coderview.search.criteria.converter;

import com.coderview.search.criteria.filter.Filter;
import org.springframework.core.convert.converter.Converter;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class FilterCriteriaConverter<T extends Filter> implements Converter<String, T> {

    private final Function<String, String> addQuotes = s -> "\"" + s + "\"";

    @Override
    public T convert(final String input) {
        final String[] parts = input.split(":");

        final String field = parts[0];
        final List<String> values = Arrays.stream(parts[1].split("\\|"))
                .map(addQuotes)
                .collect(Collectors.toList());;

        final T filter = createFilter();

        filter.setField(convertField(field));
        filter.setValues(values);

        return filter;
    }

    protected abstract T createFilter();

    protected abstract String convertField(String field);
}
