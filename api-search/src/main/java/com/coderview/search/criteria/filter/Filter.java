package com.coderview.search.criteria.filter;

import java.util.List;

public class Filter {

    private String field;

    private List<String> values;

    public Filter() {}

    public String getField() {
        return field;
    }

    public void setField(final String field) {
        this.field = field;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(final List<String> values) {
        this.values = values;
    }

    public String getExpression() {
        final String v = values.size() > 1
                ? "(" + String.join(" OR ", values) + ")"
                : values.get(0);

        return field + ":" + v;
    }
}
