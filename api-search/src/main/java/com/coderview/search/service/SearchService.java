package com.coderview.search.service;

import com.coderview.common.constant.ChallengeScope;
import com.coderview.common.solr.document.ChallengeDocument;
import com.coderview.common.solr.document.InterviewDocument;
import com.coderview.search.criteria.SearchCriteria;
import com.coderview.search.model.DocumentList;
import com.coderview.search.util.SearchQueryBuilder;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static com.coderview.search.constant.SearchConstants.INTERVIEWER;
import static com.coderview.search.constant.SearchConstants.OWNER;
import static com.coderview.search.constant.SearchConstants.SCOPE;

@Service
public class SearchService {

    private final HttpSolrClient interviewsSolrClient;

    private final HttpSolrClient challengesSolrClient;

    private final AuthService authService;

    public SearchService(final AuthService authService,
                         final HttpSolrClient interviewsSolrClient,
                         final HttpSolrClient challengesSolrClient) {
        this.authService = authService;
        this.interviewsSolrClient = interviewsSolrClient;
        this.challengesSolrClient = challengesSolrClient;
    }

    public DocumentList<InterviewDocument> getInterviews(final SearchCriteria criteria) throws IOException, SolrServerException {
        final SolrQuery query = SearchQueryBuilder.from(criteria)
                .withAdditionalFilterQuery(INTERVIEWER, authService.getAuthentication().getName())
                .build();

        final QueryResponse response = interviewsSolrClient.query(query);

        return new DocumentList<InterviewDocument>()
                .setNumFound(response.getResults().getNumFound())
                .setEntries(response.getBeans(InterviewDocument.class));
    }

    public DocumentList<ChallengeDocument> getChallenges(final SearchCriteria criteria, final ChallengeScope scope) throws IOException, SolrServerException {
        final SolrQuery query = SearchQueryBuilder.from(criteria)
                .withAdditionalFilterQueryIfTrue(isScopePrivate(scope), OWNER, authService.getAuthentication().getName())
                .withAdditionalFilterQuery(SCOPE, scope.name())
                .build();

        final QueryResponse response = challengesSolrClient.query(query);

        return new DocumentList<ChallengeDocument>()
                .setNumFound(response.getResults().getNumFound())
                .setEntries(response.getBeans(ChallengeDocument.class));
    }

    private boolean isScopePrivate(final ChallengeScope scope) {
        return ChallengeScope.PRIVATE == scope;
    }
}
