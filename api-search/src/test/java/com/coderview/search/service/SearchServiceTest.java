package com.coderview.search.service;

import com.coderview.common.constant.ChallengeScope;
import com.coderview.search.criteria.SearchCriteria;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SearchServiceTest {

    @Mock
    private HttpSolrClient interviewsSolrClient;

    @Mock
    private HttpSolrClient challengesSolrClient;

    @Mock
    private AuthService authService;

    @Mock
    private Authentication authentication;

    @Mock
    public QueryResponse queryResponse;

    @Mock
    private SolrDocumentList solrDocuments;

    @InjectMocks
    private SearchService searchService;

    @Test
    public void getInterviews() throws IOException, SolrServerException {
        when(authService.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("test@test.com");
        when(interviewsSolrClient.query(any())).thenReturn(queryResponse);
        when(challengesSolrClient.query(any())).thenReturn(queryResponse);
        when(queryResponse.getResults()).thenReturn(solrDocuments);
        when(queryResponse.getBeans(any())).thenReturn(Collections.emptyList());
        when(solrDocuments.getNumFound()).thenReturn(2L);

        searchService.getInterviews(new SearchCriteria(1, 10, "", new ArrayList<>(), new ArrayList<>()));
    }

    @Test
    public void getChallenges() throws IOException, SolrServerException {
        when(authService.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("test@test.com");
        when(interviewsSolrClient.query(any())).thenReturn(queryResponse);
        when(challengesSolrClient.query(any())).thenReturn(queryResponse);
        when(queryResponse.getResults()).thenReturn(solrDocuments);
        when(queryResponse.getBeans(any())).thenReturn(Collections.emptyList());
        when(solrDocuments.getNumFound()).thenReturn(2L);

        searchService.getChallenges(new SearchCriteria(1, 10, "", new ArrayList<>(), new ArrayList<>()), ChallengeScope.GLOBAL);
    }
}