#!/bin/bash

# This script is used to compile and run C# source code.
# Source code must be placed in a /coderview/execution/input.txt file and
# should have a Program class with static void Main method.
# In the end /coderview/execution/output/output.txt file gets created with execution results.

standard_output="standard-output.txt"
error_output="error-output.txt"
log_file="log-file.txt"
output="output/output.txt"

cd /coderview/execution
cp input.txt Program.cs

# Compile and execute C# code
START=`date +%s.%3N`
csc -target:exe Program.cs 2> $error_output
if [ $? -eq 0 ]
  then
    Program 1> $standard_output 2> $error_output
fi
END=`date +%s.%3N`
runtime=`echo $END - $START | bc`

# Compose output
echo "Runtime:" $runtime > $log_file
if [ -s $error_output ]
then
  cat $error_output >> $log_file
else
  cat $standard_output >> $log_file
fi
cp $log_file $output

echo Done!