package com.coderview.service.execution;

import com.coderview.model.execution.ExecutionContext;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.StartContainerCmd;
import com.github.dockerjava.api.model.Bind;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DockerServiceTest {

    @Mock
    private DockerClient dockerClient;

    @Mock
    private CreateContainerCmd createContainerCmd;

    @Mock
    private CreateContainerResponse createContainerResponse;

    @Mock
    private StartContainerCmd startContainerCmd;

    @InjectMocks
    private DockerService dockerService;

    @Test
    public void runTest() {
        final ExecutionContext context = new ExecutionContext();

        context.setDockerImage("test-image");
        context.setDockerBind("test-bind");
        context.setDockerVolume("test-volume");
        context.setExecutionId("456");

        when(dockerClient.createContainerCmd("test-image")).thenReturn(createContainerCmd);
        when(createContainerCmd.exec()).thenReturn(createContainerResponse);
        when(createContainerCmd.withHostConfig(any())).thenReturn(createContainerCmd);
        when(createContainerCmd.withBinds(any(Bind.class))).thenReturn(createContainerCmd);
        when(createContainerCmd.exec()).thenReturn(createContainerResponse);
        when(createContainerResponse.getId()).thenReturn("123");
        when(dockerClient.startContainerCmd("123")).thenReturn(startContainerCmd);

        dockerService.run(context);
    }
}