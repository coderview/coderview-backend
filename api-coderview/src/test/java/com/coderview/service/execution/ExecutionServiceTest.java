package com.coderview.service.execution;

import com.coderview.common.constant.ProgrammingLanguageEnum;
import com.coderview.common.integration.etherpadlite.adapter.EtherpadLitePadAdapter;
import com.coderview.constant.InterviewStatus;
import com.coderview.domain.interview.Interview;
import com.coderview.domain.interview.InterviewEnvironment;
import com.coderview.domain.interview.ProgrammingLanguage;
import com.coderview.domain.interview.Tab;
import com.coderview.model.execution.ExecutionRequest;
import com.coderview.model.execution.ExecutionResults;
import com.coderview.persistence.repository.TabRepository;
import com.coderview.processor.ExecutionProcessor;
import com.coderview.service.websocket.TopicBroadcastService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExecutionServiceTest {

    @Mock
    private ExecutionProcessor executionProcessor;

    @Mock
    private TabRepository tabRepository;

    @Mock
    private EtherpadLitePadAdapter etherpadLitePadAdapter;

    @Mock
    private TopicBroadcastService topicBroadcastService;

    @InjectMocks
    private ExecutionService executionService;

    @Test
    public void execute() {
        executionService.execute(new ExecutionRequest());
    }

    @Test
    public void executeTab() {
        final ProgrammingLanguage programmingLanguage = new ProgrammingLanguage();
        final InterviewEnvironment interviewEnvironment = new InterviewEnvironment();
        final Interview interview = new Interview();
        final Tab tab = new Tab();

        interviewEnvironment.setInterview(interview);
        interview.setLink("123");
        interview.setStatus(InterviewStatus.IN_PROGRESS);
        programmingLanguage.setName(ProgrammingLanguageEnum.JAVA);
        tab.setEnvironment(interviewEnvironment);
        tab.setProgrammingLanguage(programmingLanguage);

        when(tabRepository.findById("tab-1")).thenReturn(Optional.of(tab));
        when(etherpadLitePadAdapter.getText("tab-1")).thenReturn("test");
        when(executionProcessor.execute(any(), any())).thenReturn(new ExecutionResults());

        executionService.executeTab("tab-1");
    }
}