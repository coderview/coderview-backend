package com.coderview.service.auth;

import com.coderview.domain.user.User;
import com.coderview.error.EmailNotFoundException;
import com.coderview.persistence.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CoderviewUserDetailsServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private CoderviewUserDetailsService coderviewUserDetailsService;

    @Test(expected = EmailNotFoundException.class)
    public void userNotFound() {
        coderviewUserDetailsService.loadUserByUsername("test");
    }

    @Test
    public void loadUserByUsername() {
        when(userRepository.findByEmail("test")).thenReturn(new User());

        coderviewUserDetailsService.loadUserByUsername("test");
    }
}