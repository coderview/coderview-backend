package com.coderview.service.auth;

import com.coderview.persistence.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AuthService authService;

    @Test(expected = NullPointerException.class)
    public void getCurrentlyAuthenticatedUser() {
        authService.getCurrentlyAuthenticatedUser();
    }
}