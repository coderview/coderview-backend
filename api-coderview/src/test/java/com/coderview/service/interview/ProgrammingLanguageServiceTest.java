package com.coderview.service.interview;

import com.coderview.domain.interview.ProgrammingLanguage;
import com.coderview.persistence.repository.ProgrammingLanguageRepository;
import com.coderview.transform.impl.ProgrammingLanguageTransformer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProgrammingLanguageServiceTest {

    @Mock
    private ProgrammingLanguageRepository programmingLanguageRepository;

    @Mock
    private ProgrammingLanguageTransformer programmingLanguageTransformer;

    @InjectMocks
    private ProgrammingLanguageService programmingLanguageService;

    @Test
    public void findAll() {
        when(programmingLanguageRepository.findAll()).thenReturn(Collections.singletonList(new ProgrammingLanguage()));

        programmingLanguageService.findAll();
    }

    @Test
    public void findAllEmpty() {
        when(programmingLanguageRepository.findAll()).thenReturn(Collections.emptyList());

        programmingLanguageService.findAll();
    }
}