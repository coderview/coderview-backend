package com.coderview.service.interview;

import com.coderview.constant.InterviewStatus;
import com.coderview.domain.interview.Candidate;
import com.coderview.domain.interview.Interview;
import com.coderview.domain.user.User;
import com.coderview.dto.interview.CandidateDTO;
import com.coderview.dto.interview.InterviewDTO;
import com.coderview.error.EntityNotFoundException;
import com.coderview.error.InvalidInterviewStatusException;
import com.coderview.persistence.repository.InterviewRepository;
import com.coderview.service.auth.AuthService;
import com.coderview.service.indexing.InterviewDocumentIndexingService;
import com.coderview.service.websocket.TopicBroadcastService;
import com.coderview.transform.impl.InterviewTransformer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InterviewServiceTest {

    @Mock
    private InterviewTransformer interviewTransformer;

    @Mock
    private AuthService authService;

    @Mock
    private InterviewRepository interviewRepository;

    @Mock
    private InterviewDocumentIndexingService interviewDocumentIndexingService;

    @Mock
    private TopicBroadcastService topicBroadcastService;

    @InjectMocks
    private InterviewService interviewService;

    @Test
    public void scheduleNew() {
        when(interviewRepository.saveAndFlush(any())).thenReturn(new Interview());
        when(interviewTransformer.toEntity(any())).thenReturn(new Interview());

        interviewService.scheduleNew(new InterviewDTO());
    }

    @Test
    public void getInterview() {
        when(interviewRepository.findById(1L)).thenReturn(Optional.of(new Interview()));

        interviewService.getInterview(1L);
    }

    @Test
    public void updateInterview() {
        final Interview interview = new Interview();
        final InterviewDTO interviewDTO = new InterviewDTO();

        interview.setCandidate(new Candidate());
        interviewDTO.setCandidate(new CandidateDTO());

        when(interviewRepository.findById(1L)).thenReturn(Optional.of(interview));

        interviewService.updateInterview(1L, interviewDTO);
    }

    @Test
    public void deleteInterview() {
        interviewService.deleteInterview(1L);
    }

    @Test
    public void startInterview() {
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.SCHEDULED);

        when(interviewRepository.findById(1L)).thenReturn(Optional.of(interview));

        interviewService.startInterview(1L);
    }

    @Test
    public void endInterview() {
        final User interviewer = new User();
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.IN_PROGRESS);
        interview.setInterviewer(interviewer);

        when(interviewRepository.findById(1L)).thenReturn(Optional.of(interview));
        when(authService.getCurrentlyAuthenticatedUser()).thenReturn(interviewer);

        interviewService.endInterview(1L);
    }

    @Test
    public void joinInterviewIN_PROGRESS() {
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.IN_PROGRESS);

        when(interviewRepository.findByLink("link")).thenReturn(interview);

        interviewService.joinInterview("link");
    }

    @Test(expected = InvalidInterviewStatusException.class)
    public void joinInterviewSCHEDULED() {
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.SCHEDULED);

        when(interviewRepository.findByLink("link")).thenReturn(interview);

        interviewService.joinInterview("link");
    }

    @Test(expected = InvalidInterviewStatusException.class)
    public void joinInterviewCOMPLETED() {
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.COMPLETED);

        when(interviewRepository.findByLink("link")).thenReturn(interview);

        interviewService.joinInterview("link");
    }

    @Test(expected = InvalidInterviewStatusException.class)
    public void joinInterviewUnknownStatus() {
        final Interview interview = new Interview();

        when(interviewRepository.findByLink("link")).thenReturn(interview);

        interviewService.joinInterview("link");
    }

    @Test
    public void getParticipants() {
        when(interviewRepository.findById(1L)).thenReturn(Optional.of(new Interview()));

        interviewService.getParticipants(1L);
    }

    // ---------------------------------------------------------------------------------------------------------
    @Test(expected = NullPointerException.class)
    public void scheduleNewNotFound() {
        interviewService.scheduleNew(new InterviewDTO());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getInterviewNotFound() {
        interviewService.getInterview(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateInterviewNotFound() {
        interviewService.updateInterview(1L, new InterviewDTO());
    }

    @Test
    public void deleteInterview0() {
        interviewService.deleteInterview(0L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void startInterviewNotFound() {
        interviewService.startInterview(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void endInterviewNotFound() {
        final User interviewer = new User();
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.IN_PROGRESS);
        interview.setInterviewer(interviewer);

        interviewService.endInterview(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void joinInterviewIN_PROGRESSNotFound() {
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.IN_PROGRESS);

        interviewService.joinInterview("link");
    }

    @Test(expected = EntityNotFoundException.class)
    public void joinInterviewSCHEDULEDNotFound() {
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.SCHEDULED);

        interviewService.joinInterview("link");
    }

    @Test(expected = EntityNotFoundException.class)
    public void joinInterviewCOMPLETEDNotFound() {
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.COMPLETED);

        interviewService.joinInterview("link");
    }

    @Test(expected = EntityNotFoundException.class)
    public void joinInterviewUnknownStatusNotFound() {
        final Interview interview = new Interview();

        interviewService.joinInterview("link");
    }

    @Test(expected = EntityNotFoundException.class)
    public void getParticipantsNotFound() {
        interviewService.getParticipants(1L);
    }
}