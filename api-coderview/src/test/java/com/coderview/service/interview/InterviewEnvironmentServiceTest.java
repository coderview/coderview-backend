package com.coderview.service.interview;

import com.coderview.common.constant.ProgrammingLanguageEnum;
import com.coderview.common.integration.etherpadlite.adapter.EtherpadLitePadAdapter;
import com.coderview.constant.InterviewStatus;
import com.coderview.domain.challenge.Challenge;
import com.coderview.domain.interview.Interview;
import com.coderview.domain.interview.InterviewEnvironment;
import com.coderview.domain.interview.ProgrammingLanguage;
import com.coderview.domain.interview.Tab;
import com.coderview.dto.challenge.ChallengeDTO;
import com.coderview.dto.interview.NotesDTO;
import com.coderview.dto.interview.ProgrammingLanguageDTO;
import com.coderview.dto.interview.TabDTO;
import com.coderview.persistence.repository.ChallengeRepository;
import com.coderview.persistence.repository.InterviewEnvironmentRepository;
import com.coderview.persistence.repository.ProgrammingLanguageRepository;
import com.coderview.persistence.repository.TabRepository;
import com.coderview.service.websocket.TopicBroadcastService;
import com.coderview.transform.impl.TabTransformer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InterviewEnvironmentServiceTest {

    @Mock
    private TopicBroadcastService topicBroadcastService;

    @Mock
    private TabTransformer tabTransformer;

    @Mock
    private TabRepository tabRepository;

    @Mock
    private ProgrammingLanguageRepository programmingLanguageRepository;

    @Mock
    private InterviewEnvironmentRepository interviewEnvironmentRepository;

    @Mock
    private ChallengeRepository challengeRepository;

    @Mock
    private EtherpadLitePadAdapter etherpadLitePadAdapter;

    @InjectMocks
    private InterviewEnvironmentService interviewEnvironmentService;

    @Test
    public void createTab() {
        final InterviewEnvironment interviewEnvironment = new InterviewEnvironment();
        final Interview interview = new Interview();
        final TabDTO tabDTO = new TabDTO();
        final Tab tab = new Tab();
        final ProgrammingLanguage programmingLanguage = new ProgrammingLanguage();
        final ProgrammingLanguageDTO programmingLanguageDTO = new ProgrammingLanguageDTO();

        interview.setStatus(InterviewStatus.IN_PROGRESS);
        interviewEnvironment.setInterview(interview);
        programmingLanguageDTO.setName(ProgrammingLanguageEnum.JAVA);
        tabDTO.setProgrammingLanguage(programmingLanguageDTO);
        tab.setId("tab-1");

        when(interviewEnvironmentRepository.findById(1L)).thenReturn(Optional.of(interviewEnvironment));
        when(programmingLanguageRepository.findByName(any())).thenReturn(programmingLanguage);
        when(tabRepository.saveAndFlush(any())).thenReturn(tab);

        interviewEnvironmentService.createTab(1L, tabDTO);
    }

    @Test
    public void deleteTab() {
        final InterviewEnvironment interviewEnvironment = new InterviewEnvironment();
        final Interview interview = new Interview();

        interviewEnvironment.setTabs(new ArrayList<>());
        interview.setStatus(InterviewStatus.IN_PROGRESS);
        interviewEnvironment.setInterview(interview);

        when(interviewEnvironmentRepository.findById(1L)).thenReturn(Optional.of(interviewEnvironment));

        interviewEnvironmentService.deleteTab(1L, "tab-1");
    }

    @Test
    public void saveNotes() {
        final InterviewEnvironment interviewEnvironment = new InterviewEnvironment();

        when(interviewEnvironmentRepository.findById(1L)).thenReturn(Optional.of(interviewEnvironment));

        interviewEnvironmentService.saveNotes(1, new NotesDTO());
    }

    @Test
    public void getNotes() {
        final InterviewEnvironment interviewEnvironment = new InterviewEnvironment();

        when(interviewEnvironmentRepository.findById(1L)).thenReturn(Optional.of(interviewEnvironment));

        interviewEnvironmentService.getNotes(1L);
    }

    @Test
    public void includeChallenge() {
        final InterviewEnvironment interviewEnvironment = new InterviewEnvironment();
        final ChallengeDTO challengeDTO = new ChallengeDTO();
        final Tab tab = new Tab();
        final Interview interview = new Interview();

        interviewEnvironment.setTabs(new ArrayList<>());
        interview.setStatus(InterviewStatus.IN_PROGRESS);
        interviewEnvironment.setInterview(interview);
        challengeDTO.setId(2L);
        tab.setId("tab-1");

        when(interviewEnvironmentRepository.findById(1L)).thenReturn(Optional.of(interviewEnvironment));
        when(challengeRepository.findById(2L)).thenReturn(Optional.of(new Challenge()));
        when(tabRepository.saveAndFlush(any())).thenReturn(tab);

        interviewEnvironmentService.includeChallenge(1L, challengeDTO);
    }
}