package com.coderview.service.indexing;

import com.coderview.common.error.CoreCoderviewException;
import com.coderview.constant.InterviewStatus;
import com.coderview.domain.interview.Candidate;
import com.coderview.domain.interview.Interview;
import com.coderview.domain.user.User;
import com.coderview.persistence.repository.InterviewRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InterviewDocumentIndexingServiceTest {

    @Mock
    private InterviewRepository interviewRepository;

    private InterviewDocumentIndexingService interviewDocumentIndexingService;

    @Before
    public void setUp() throws Exception {
        interviewDocumentIndexingService = new InterviewDocumentIndexingService(interviewRepository, "url");
    }

    @Test(expected = CoreCoderviewException.class)
    public void index() {
        final Interview interview = new Interview();

        interview.setStatus(InterviewStatus.IN_PROGRESS);
        interview.setCandidate(new Candidate());
        interview.setInterviewer(new User());

        when(interviewRepository.findById(1L)).thenReturn(Optional.of(interview));

        interviewDocumentIndexingService.index(1L);
    }

    @Test(expected = CoreCoderviewException.class)
    public void delete() {
        interviewDocumentIndexingService.delete(1L);
    }
}