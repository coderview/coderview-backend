package com.coderview.service.indexing;

import com.coderview.common.constant.ChallengeDifficulty;
import com.coderview.common.constant.ChallengeScope;
import com.coderview.common.constant.ProgrammingLanguageEnum;
import com.coderview.common.error.CoreCoderviewException;
import com.coderview.domain.challenge.Challenge;
import com.coderview.domain.challenge.ChallengeType;
import com.coderview.domain.interview.ProgrammingLanguage;
import com.coderview.domain.user.User;
import com.coderview.persistence.repository.ChallengeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChallengeDocumentIndexingServiceTest {

    @Mock
    private ChallengeRepository challengeRepository;

    private ChallengeDocumentIndexingService challengeDocumentIndexingService;

    @Before
    public void setUp() throws Exception {
        challengeDocumentIndexingService = new ChallengeDocumentIndexingService(challengeRepository, "url");
    }

    @Test(expected = CoreCoderviewException.class)
    public void index() {
        final ProgrammingLanguage programmingLanguage = new ProgrammingLanguage();
        programmingLanguage.setName(ProgrammingLanguageEnum.JAVA);

        final Challenge challenge = new Challenge();
        challenge.setId(1L);
        challenge.setScope(ChallengeScope.GLOBAL);
        challenge.setDescription("test");
        challenge.setDifficulty(ChallengeDifficulty.EASY);
        challenge.setType(new ChallengeType());
        challenge.setOwner(new User());
        challenge.setProgrammingLanguage(programmingLanguage);

        when(challengeRepository.findById(1L)).thenReturn(Optional.of(challenge));

        challengeDocumentIndexingService.index(1L);
    }

    @Test(expected = CoreCoderviewException.class)
    public void delete() {
        challengeDocumentIndexingService.delete(1L);
    }
}