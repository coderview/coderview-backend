package com.coderview.service.user;

import com.coderview.domain.user.User;
import com.coderview.dto.user.UserDTO;
import com.coderview.persistence.repository.UserRepository;
import com.coderview.transform.impl.UserTransformer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserTransformer userTransformer;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userService;

    @Test
    public void register() {
        final UserDTO userDTO = new UserDTO();
        userDTO.setPassword("password");

        when(userTransformer.toEntity(userDTO)).thenReturn(new User());

        userService.register(userDTO);
    }

    @Test(expected = NullPointerException.class)
    public void registerEmpty() {
        userService.register(new UserDTO());
    }
}