package com.coderview.service.websocket;

import com.coderview.dto.interview.TabDTO;
import com.coderview.model.execution.ExecutionResults;
import com.coderview.model.session.InterviewSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@RunWith(MockitoJUnitRunner.class)
public class TopicBroadcastServiceTest {

    @Mock
    private SimpMessagingTemplate messagingTemplate;

    @InjectMocks
    private TopicBroadcastService topicBroadcastService;

    @Test
    public void newTab() {
        topicBroadcastService.newTab("link", new TabDTO());
    }

    @Test
    public void deleteTab() {
        topicBroadcastService.deleteTab("link", "tab-1");
    }

    @Test
    public void executionOutput() {
        topicBroadcastService.executionOutput("link", new ExecutionResults());
    }

    @Test
    public void participantJoined() {
        topicBroadcastService.participantJoined("link", new InterviewSession.User(""));
    }

    @Test
    public void participantLeft() {
        topicBroadcastService.participantLeft("link", new InterviewSession.User(""));
    }

    @Test
    public void endInterview() {
        topicBroadcastService.endInterview("link");
    }

    @Test
    public void newTabEmpty() {
        topicBroadcastService.newTab("", new TabDTO());
    }

    @Test
    public void deleteTabEmpty() {
        topicBroadcastService.deleteTab("", "");
    }

    @Test
    public void executionOutputEmpty() {
        topicBroadcastService.executionOutput("", new ExecutionResults());
    }

    @Test
    public void participantJoinedEmpty() {
        topicBroadcastService.participantJoined("", new InterviewSession.User(""));
    }

    @Test
    public void participantLeftEmpty() {
        topicBroadcastService.participantLeft("", new InterviewSession.User(""));
    }

    @Test
    public void endInterviewEmpty() {
        topicBroadcastService.endInterview("");
    }
}