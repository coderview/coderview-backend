package com.coderview.service.challenge;

import com.coderview.common.constant.ChallengeScope;
import com.coderview.common.constant.ProgrammingLanguageEnum;
import com.coderview.domain.challenge.Challenge;
import com.coderview.domain.challenge.ChallengeType;
import com.coderview.domain.interview.ProgrammingLanguage;
import com.coderview.domain.user.User;
import com.coderview.dto.challenge.ChallengeDTO;
import com.coderview.dto.challenge.ChallengeTypeDTO;
import com.coderview.dto.interview.ProgrammingLanguageDTO;
import com.coderview.persistence.repository.ChallengeRepository;
import com.coderview.persistence.repository.ChallengeTypeRepository;
import com.coderview.persistence.repository.ProgrammingLanguageRepository;
import com.coderview.service.auth.AuthService;
import com.coderview.service.indexing.ChallengeDocumentIndexingService;
import com.coderview.transform.impl.ChallengeTransformer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChallengeServiceTest {

    @Mock
    private ChallengeRepository challengeRepository;

    @Mock
    private ChallengeTypeRepository challengeTypeRepository;

    @Mock
    private ProgrammingLanguageRepository programmingLanguageRepository;

    @Mock
    private ChallengeTransformer challengeTransformer;

    @Mock
    private AuthService authService;

    @Mock
    private ChallengeDocumentIndexingService challengeDocumentIndexingService;

    @InjectMocks
    private ChallengeService challengeService;

    @Test
    public void getChallenge() {
        when(challengeRepository.findById(1L)).thenReturn(Optional.of(new Challenge()));

        challengeService.getChallenge(1);
    }

    @Test
    public void createChallenge() {
        final Challenge saved = new Challenge();
        final ChallengeDTO challengeDTO = new ChallengeDTO();
        final ChallengeTypeDTO challengeTypeDTO = new ChallengeTypeDTO();
        final ProgrammingLanguageDTO programmingLanguageDTO = new ProgrammingLanguageDTO();

        saved.setId(2L);
        challengeTypeDTO.setId(1L);
        programmingLanguageDTO.setName(ProgrammingLanguageEnum.JAVA);
        challengeDTO.setType(challengeTypeDTO);
        challengeDTO.setProgrammingLanguage(programmingLanguageDTO);
        challengeDTO.setScope(ChallengeScope.PRIVATE);

        when(challengeTypeRepository.findById(1L)).thenReturn(Optional.of(new ChallengeType()));
        when(challengeTypeRepository.findById(1L)).thenReturn(Optional.of(new ChallengeType()));
        when(challengeTransformer.toEntity(challengeDTO)).thenReturn(new Challenge());
        when(challengeRepository.saveAndFlush(any())).thenReturn(saved);
        when(programmingLanguageRepository.findByName(ProgrammingLanguageEnum.JAVA)).thenReturn(new ProgrammingLanguage());

        challengeService.createChallenge(challengeDTO);
    }

    @Test
    public void updateChallenge() {
        final Challenge saved = new Challenge();
        final Challenge challenge = new Challenge();
        final User owner = new User();
        final ChallengeDTO challengeDTO = new ChallengeDTO();
        final ChallengeTypeDTO challengeTypeDTO = new ChallengeTypeDTO();
        final ProgrammingLanguageDTO programmingLanguageDTO = new ProgrammingLanguageDTO();

        saved.setId(3L);
        challenge.setOwner(owner);
        challengeTypeDTO.setId(1L);
        programmingLanguageDTO.setName(ProgrammingLanguageEnum.JAVA);
        challengeDTO.setType(challengeTypeDTO);
        challengeDTO.setProgrammingLanguage(programmingLanguageDTO);
        challengeDTO.setScope(ChallengeScope.PRIVATE);
        challengeDTO.setDifficulty("Easy");

        when(challengeTypeRepository.findById(1L)).thenReturn(Optional.of(new ChallengeType()));
        when(challengeRepository.findById(2L)).thenReturn(Optional.of(challenge));
        when(authService.getCurrentlyAuthenticatedUser()).thenReturn(owner);
        when(challengeRepository.saveAndFlush(any())).thenReturn(saved);
        when(programmingLanguageRepository.findByName(ProgrammingLanguageEnum.JAVA)).thenReturn(new ProgrammingLanguage());

        challengeService.updateChallenge(2, challengeDTO);
    }

    @Test
    public void deleteChallenge() {
        challengeService.deleteChallenge(1);
    }
}