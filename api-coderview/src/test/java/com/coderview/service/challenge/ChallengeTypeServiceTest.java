package com.coderview.service.challenge;

import com.coderview.domain.challenge.ChallengeType;
import com.coderview.dto.challenge.ChallengeTypeDTO;
import com.coderview.persistence.repository.ChallengeTypeRepository;
import com.coderview.transform.impl.ChallengeTypeTransformer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChallengeTypeServiceTest {

    @Mock
    private ChallengeTypeRepository challengeTypeRepository;

    @Mock
    private ChallengeTypeTransformer challengeTypeTransformer;

    @InjectMocks
    private ChallengeTypeService challengeTypeService;

    @Test
    public void createChallengeType() {
        challengeTypeService.createChallengeType(new ChallengeTypeDTO());
    }

    @Test
    public void findAll() {
        when(challengeTypeRepository.findAll()).thenReturn(Collections.singletonList(new ChallengeType()));
        challengeTypeService.findAll();
    }
}