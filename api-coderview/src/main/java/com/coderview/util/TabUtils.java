package com.coderview.util;

import com.coderview.domain.interview.Tab;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class TabUtils {

    private static final String DEFAULT_TITLE = "Unnamed";

    public static String getDefaultTitle() {
        return DEFAULT_TITLE;
    }

    public static int resolveOrderNumber(final List<Tab> tabs) {
        return Optional.ofNullable(tabs)
                .map(TabUtils::findMaxOrderNumber)
                .map(orderNumber -> ++orderNumber)
                .orElse(1);
    }

    private static int findMaxOrderNumber(final List<Tab> tabs) {
        return tabs.stream()
                .mapToInt(Tab::getOrderNumber)
                .max().orElse(0);
    }
}
