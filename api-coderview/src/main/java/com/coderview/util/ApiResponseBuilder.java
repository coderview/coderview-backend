package com.coderview.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ApiResponseBuilder<T> {

    private HttpStatus status;

    private T body;

    private ApiResponseBuilder() {
    }

    public static <T> ApiResponseBuilder<T> newBuilder() {
        return new ApiResponseBuilder();
    }

    public ApiResponseBuilder<T> withStatus(final HttpStatus status) {
        this.status = status;
        return this;
    }

    public ApiResponseBuilder<T> withBody(final T body) {
        this.body = body;
        return this;
    }

    public ResponseEntity<T> build() {
        return ResponseEntity.<T>status(status).body(body);
    }
}
