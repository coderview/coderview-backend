package com.coderview.util;

import java.util.UUID;

public class ExecutionUtils {

    public static String getExecutionId() {
        return UUID.randomUUID().toString();
    }
}
