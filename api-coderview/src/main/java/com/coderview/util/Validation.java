package com.coderview.util;

import org.apache.commons.lang3.StringUtils;

import java.util.function.Supplier;

public class Validation {

    public static <T extends Throwable> void requireNonNull(final Object obj, final Supplier<? extends T> exceptionSupplier) throws T {
        if (obj == null) {
            throw exceptionSupplier.get();
        }
    }

    public static <T extends Throwable> void requireNotBlank(final String str, final Supplier<? extends T> exceptionSupplier) throws T {
        if (StringUtils.isBlank(str)) {
            throw exceptionSupplier.get();
        }
    }
}
