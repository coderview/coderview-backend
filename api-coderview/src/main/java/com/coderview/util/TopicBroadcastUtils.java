package com.coderview.util;

import com.coderview.constant.InterviewStatus;
import com.coderview.domain.interview.Interview;

public class TopicBroadcastUtils {

    public static boolean isBroadcastRequired(final Interview interview) {
        return interview.getStatus() == InterviewStatus.IN_PROGRESS;
    }
}
