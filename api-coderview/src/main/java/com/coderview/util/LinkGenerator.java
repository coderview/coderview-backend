package com.coderview.util;

import org.apache.commons.text.RandomStringGenerator;

import java.util.Random;
import java.util.stream.Collectors;

public class LinkGenerator {

    private static int LINK_LENGTH = 45;

    private static Random random = new Random();

    public static String generate() {
        final String link = new RandomStringGenerator.Builder()
                .withinRange('a', 'z')
                .build().generate(LINK_LENGTH);

        return randomizeCase(link);
    }

    private static String randomizeCase(final String link) {
        return link.chars()
                .mapToObj(c -> (char) c)
                .map(c -> random.nextInt(10) >= 5 ? Character.toUpperCase(c) : c)
                .map(String::valueOf)
                .collect(Collectors.joining());
    }
}
