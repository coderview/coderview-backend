package com.coderview.model.auth;

public class UserCredentials {

    private String email;

    private String password;

    public UserCredentials() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
