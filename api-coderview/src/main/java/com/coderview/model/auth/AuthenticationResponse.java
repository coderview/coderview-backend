package com.coderview.model.auth;

public class AuthenticationResponse {

    private String idToken;

    private long expiresAt;

    public AuthenticationResponse(final String idToken, final long expiresAt) {
        this.idToken = idToken;
        this.expiresAt = expiresAt;
    }

    public AuthenticationResponse() {
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(final String idToken) {
        this.idToken = idToken;
    }

    public long getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(final long expiresAt) {
        this.expiresAt = expiresAt;
    }
}
