package com.coderview.model.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
@ConfigurationProperties("docker")
public class DockerConfig {

    private String host;
    private String certPath;
    private Execution execution;

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        this.host = host;
    }

    public String getCertPath() {
        return certPath;
    }

    public void setCertPath(final String certPath) {
        this.certPath = certPath;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(final Execution execution) {
        this.execution = execution;
    }

    public static class Execution {

        private String directory;
        private String bind;
        private String volume;
        private Map<String, String> images;

        public String getDirectory() {
            return directory;
        }

        public void setDirectory(final String directory) {
            this.directory = directory;
        }

        public String getBind() {
            return bind;
        }

        public void setBind(final String bind) {
            this.bind = bind;
        }

        public String getVolume() {
            return volume;
        }

        public void setVolume(final String volume) {
            this.volume = volume;
        }

        public Map<String, String> getImages() {
            return images;
        }

        public void setImages(final Map<String, String> images) {
            this.images = images;
        }
    }
}
