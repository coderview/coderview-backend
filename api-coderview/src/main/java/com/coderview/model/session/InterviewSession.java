package com.coderview.model.session;

import com.coderview.constant.Color;
import com.coderview.domain.interview.Interview;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

public class InterviewSession {

    private final Interview interview;

    private List<User> participants = new ArrayList<>();

    public InterviewSession(final Interview interview) {
        this.interview = interview;
    }

    public Interview getInterview() {
        return interview;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public InterviewSession addParticipant(final User participant) {
        if (isDuplicate(participant)) {
            removeParticipant(participant);
        }

        participants.add(participant.setCircleColor(getRandomColor()));

        return this;
    }

    public User getParticipantBySessionId(final String sessionId) {
        return participants.stream()
                .filter(participant -> participant.getSessionId().equals(sessionId))
                .findFirst().orElse(null);
    }

    public InterviewSession removeParticipant(final User participant) {
        participants = participants.stream()
                .filter(p -> !p.equals(participant))
                .collect(Collectors.toList());

        return this;
    }

    private boolean isDuplicate(final User participant) {
        return participants.stream().anyMatch(participant::equals);
    }

    private Color getRandomColor() {
        final int index = new Random().nextInt(Color.values().length);
        final Color color = Color.values()[index];

        final boolean isDuplicateColor = participants.stream()
                .map(User::getCircleColor)
                .anyMatch(color::equals);

        if (isDuplicateColor) {
            if (index == Color.values().length - 1) {
                return Color.values()[0];
            } else {
                return Color.values()[index + 1];
            }
        } else {
            return color;
        }
    }

    public static class User {

        private final String sessionId;

        private String name;

        private String email;

        private Color circleColor;

        public User(final String sessionId) {
            this.sessionId = sessionId;
        }

        public String getSessionId() {
            return sessionId;
        }

        public String getName() {
            return name;
        }

        public User setName(final String name) {
            this.name = name;
            return this;
        }

        public String getEmail() {
            return email;
        }

        public User setEmail(final String email) {
            this.email = email;
            return this;
        }

        public Color getCircleColor() {
            return circleColor;
        }

        public User setCircleColor(final Color circleColor) {
            this.circleColor = circleColor;
            return this;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof User) {
                final User otherUser = (User) obj;
                return Objects.equals(this.name, otherUser.getName())
                        && Objects.equals(this.email, otherUser.getEmail());
            }

            return false;
        }
    }
}
