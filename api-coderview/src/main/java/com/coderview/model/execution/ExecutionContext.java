package com.coderview.model.execution;

import com.coderview.common.constant.ProgrammingLanguageEnum;

import java.io.File;

import static com.coderview.constant.SandboxConstants.KEY_EXECUTION_ID;

public class ExecutionContext {

    private ProgrammingLanguageEnum programmingLanguage;

    private String sourceCode;

    private String executionId;

    private String directory;

    private String dockerImage;

    private String dockerBind;

    private String dockerVolume;

    public ProgrammingLanguageEnum getProgrammingLanguage() {
        return programmingLanguage;
    }

    public ExecutionContext setProgrammingLanguage(final ProgrammingLanguageEnum programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
        return this;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public ExecutionContext setSourceCode(final String sourceCode) {
        this.sourceCode = sourceCode;
        return this;
    }

    public String getExecutionId() {
        return executionId;
    }

    public ExecutionContext setExecutionId(final String executionId) {
        this.executionId = executionId;
        return this;
    }

    public String getDockerImage() {
        return dockerImage;
    }

    public ExecutionContext setDockerImage(final String dockerImage) {
        this.dockerImage = dockerImage;
        return this;
    }

    public String getDockerBind() {
        return dockerBind.replace(KEY_EXECUTION_ID, executionId);
    }

    public ExecutionContext setDockerBind(final String dockerBind) {
        this.dockerBind = dockerBind;
        return this;
    }

    public String getDockerVolume() {
        return dockerVolume;
    }

    public ExecutionContext setDockerVolume(final String dockerVolume) {
        this.dockerVolume = dockerVolume;
        return this;
    }

    public String getDirectory() {
        return directory.replace(KEY_EXECUTION_ID, executionId);
    }

    public ExecutionContext setDirectory(final String directory) {
        this.directory = directory;
        return this;
    }

    public String getInputFile() {
        return appendSeparatorIfAbsent(getDirectory()) + "input.txt";
    }

    public String getExecutionOutputDirectory() {
        return appendSeparatorIfAbsent(getDirectory()) + "output";
    }

    public String getExecutionOutputFile() {
        return appendSeparatorIfAbsent(getExecutionOutputDirectory()) + "output.txt";
    }

    private String appendSeparatorIfAbsent(final String path) {
        return path.endsWith("\\") ? path : path + File.separator;
    }
}
