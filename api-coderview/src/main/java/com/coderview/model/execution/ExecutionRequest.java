package com.coderview.model.execution;

import com.coderview.common.constant.ProgrammingLanguageEnum;

public class ExecutionRequest {

    private ProgrammingLanguageEnum programmingLanguage;
    private String sourceCode;

    public ProgrammingLanguageEnum getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(final ProgrammingLanguageEnum programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(final String sourceCode) {
        this.sourceCode = sourceCode;
    }
}
