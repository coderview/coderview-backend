package com.coderview.model.execution;

public class ExecutionResults {

    private String executionId;

    private String output;

    private double runtime;

    private long executedLinesCount;

    private String programmingLanguage;

    public String getExecutionId() {
        return executionId;
    }

    public ExecutionResults setExecutionId(final String executionId) {
        this.executionId = executionId;
        return this;
    }

    public String getOutput() {
        return output;
    }

    public ExecutionResults setOutput(final String output) {
        this.output = output;
        return this;
    }

    public double getRuntime() {
        return runtime;
    }

    public ExecutionResults setRuntime(final double runtime) {
        this.runtime = runtime;
        return this;
    }

    public long getExecutedLinesCount() {
        return executedLinesCount;
    }

    public ExecutionResults setExecutedLinesCount(final long executedLinesCount) {
        this.executedLinesCount = executedLinesCount;
        return this;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public ExecutionResults setProgrammingLanguage(final String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
        return this;
    }
}
