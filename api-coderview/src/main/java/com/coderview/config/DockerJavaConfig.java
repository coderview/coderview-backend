package com.coderview.config;

import com.coderview.model.config.DockerConfig;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DockerJavaConfig {

    @Autowired
    private DockerConfig dockerConfig;

    @Bean
    public DockerClientConfig dockerClientConfig() {
        return DefaultDockerClientConfig.createDefaultConfigBuilder()
                .withDockerHost(dockerConfig.getHost())
                .withDockerTlsVerify(true)
                .withDockerCertPath(dockerConfig.getCertPath())
                .build();
    }

    @Bean
    public DockerClient dockerClient(final DockerClientConfig dockerClientConfig) {
        return DockerClientBuilder.getInstance(dockerClientConfig).build();
    }
}
