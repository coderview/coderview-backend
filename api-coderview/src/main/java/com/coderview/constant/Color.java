package com.coderview.constant;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Color {

    BLUE("blue"),
    GREEN("green"),
    RED("red"),
    MAROON("maroon"),
    ORANGE("orange");

    @JsonValue
    private final String value;

    Color(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
