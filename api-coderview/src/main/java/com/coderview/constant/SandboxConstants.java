package com.coderview.constant;

public class SandboxConstants {

    public static final String KEY_EXECUTION_ID = "{{executionId}}";

    private SandboxConstants() {}
}
