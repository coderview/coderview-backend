package com.coderview.constant;

public class SessionConstants {

    public static final String LINK = "link";

    public static final String USER = "user";

    public static final String SIMP_CONNECT_MESSAGE = "simpConnectMessage";

    public static final String NATIVE_HEADERS = "nativeHeaders";

    public static final String SIMP_SESSION_ID = "simpSessionId";

    private SessionConstants() {
    }
}
