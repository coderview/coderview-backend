package com.coderview.constant;

public enum InterviewStatus {
    SCHEDULED,
    IN_PROGRESS,
    COMPLETED
}
