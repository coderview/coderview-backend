package com.coderview.constant;

public enum AccountStatus {
    ACTIVE,
    BLOCKED
}
