package com.coderview.constant;

public class STOMPConstants {

    public static final String LINK = "{{link}}";

    public static final String TOPIC_NEW_TAB = "/topic/{{link}}/tabs/new";

    public static final String TOPIC_DELETE_TAB = "/topic/{{link}}/tabs/delete";

    public static final String TOPIC_EXECUTION_OUTPUT = "/topic/{{link}}/execution/output";

    public static final String TOPIC_PARTICIPANT_JOINED = "/topic/{{link}}/participants/joined";

    public static final String TOPIC_PARTICIPANT_LEFT = "/topic/{{link}}/participants/left";

    public static final String TOPIC_END_INTERVIEW = "/topic/{{link}}/interview/end";

    private STOMPConstants() {
    }
}
