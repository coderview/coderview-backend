package com.coderview.constant;

public enum AccountRole {
    REGULAR,
    ADMIN
}
