package com.coderview.controller;

import com.coderview.dto.interview.ProgrammingLanguageDTO;
import com.coderview.service.interview.ProgrammingLanguageService;
import com.coderview.util.ApiResponseBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProgrammingLanguagesController {

    private final ProgrammingLanguageService programmingLanguageService;

    public ProgrammingLanguagesController(final ProgrammingLanguageService programmingLanguageService) {
        this.programmingLanguageService = programmingLanguageService;
    }

    @GetMapping(path = "/programminglanguages", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProgrammingLanguageDTO>> getAllProgrammingLanguages() {
        final List<ProgrammingLanguageDTO> programmingLanguages = programmingLanguageService.findAll();
        return ApiResponseBuilder.<List<ProgrammingLanguageDTO>> newBuilder()
                .withBody(programmingLanguages)
                .withStatus(HttpStatus.OK).build();
    }
}
