package com.coderview.controller;

import com.coderview.dto.challenge.ChallengeDTO;
import com.coderview.dto.challenge.ChallengeTypeDTO;
import com.coderview.service.challenge.ChallengeService;
import com.coderview.service.challenge.ChallengeTypeService;
import com.coderview.util.ApiResponseBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ChallengesController {

    private final ChallengeService challengeService;

    private final ChallengeTypeService challengeTypeService;

    public ChallengesController(final ChallengeService challengeService,
                                final ChallengeTypeService challengeTypeService) {
        this.challengeService = challengeService;
        this.challengeTypeService = challengeTypeService;
    }

    @GetMapping(path = "/challenges/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChallengeDTO> getChallenge(@PathVariable final long id) {
        final ChallengeDTO challengeDTO = challengeService.getChallenge(id);
        return ApiResponseBuilder.<ChallengeDTO> newBuilder()
                .withStatus(HttpStatus.OK)
                .withBody(challengeDTO).build();
    }

    @PostMapping(path = "/challenges", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChallengeDTO> createChallenge(@RequestBody final ChallengeDTO challengeDTO) {
        final ChallengeDTO created = challengeService.createChallenge(challengeDTO);
        return ApiResponseBuilder.<ChallengeDTO> newBuilder()
                .withStatus(HttpStatus.CREATED)
                .withBody(created).build();
    }

    @PutMapping(path = "/challenges/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChallengeDTO> updateChallenge(@PathVariable final long id,
                                                        @RequestBody final ChallengeDTO challengeDTO) {
        final ChallengeDTO updated = challengeService.updateChallenge(id, challengeDTO);
        return ApiResponseBuilder.<ChallengeDTO> newBuilder()
                .withStatus(HttpStatus.OK)
                .withBody(updated).build();
    }

    @DeleteMapping(path = "/challenges/{id}")
    public ResponseEntity<Void> deleteChallenge(@PathVariable final long id) {
        challengeService.deleteChallenge(id);
        return ApiResponseBuilder.<Void> newBuilder()
                .withStatus(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(path = "/challenges/types", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ChallengeTypeDTO>> getAllChallengeTypes() {
        final List<ChallengeTypeDTO> challengeTypes = challengeTypeService.findAll();
        return ApiResponseBuilder.<List<ChallengeTypeDTO>> newBuilder()
                .withStatus(HttpStatus.OK)
                .withBody(challengeTypes).build();
    }

    @PostMapping(path = "/challenges/types", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createChallengeType(@RequestBody final ChallengeTypeDTO challengeTypeDTO) {
        challengeTypeService.createChallengeType(challengeTypeDTO);
        return ApiResponseBuilder.<Void> newBuilder()
                .withStatus(HttpStatus.CREATED).build();
    }
}
