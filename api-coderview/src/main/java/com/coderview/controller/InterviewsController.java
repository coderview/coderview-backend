package com.coderview.controller;

import com.coderview.dto.interview.InterviewDTO;
import com.coderview.model.session.InterviewSession;
import com.coderview.service.interview.InterviewService;
import com.coderview.util.ApiResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class InterviewsController {

    private final InterviewService interviewService;

    @Autowired
    public InterviewsController(final InterviewService interviewService) {
        this.interviewService = interviewService;
    }

    @PostMapping(path = "/interviews", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> scheduleNew(final @RequestBody InterviewDTO interviewDTO) {
        interviewService.scheduleNew(interviewDTO);
        return ApiResponseBuilder.<Void> newBuilder()
                .withStatus(HttpStatus.CREATED).build();
    }

    @GetMapping(path = "/interviews/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InterviewDTO> getInterview(final @PathVariable long id) {
        final InterviewDTO interviewDTO = interviewService.getInterview(id);
        return ApiResponseBuilder.<InterviewDTO> newBuilder()
                .withStatus(HttpStatus.OK)
                .withBody(interviewDTO).build();
    }

    @PutMapping(path = "/interviews/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateInterview(final @PathVariable long id,
                                                final @RequestBody InterviewDTO interviewDTO) {
        interviewService.updateInterview(id, interviewDTO);
        return ApiResponseBuilder.<Void> newBuilder()
                .withStatus(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping(path = "/interviews/{id}")
    public ResponseEntity<Void> deleteInterview(final @PathVariable long id) {
        interviewService.deleteInterview(id);
        return ApiResponseBuilder.<Void> newBuilder()
                .withStatus(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(path = "/interviews/{id}/start")
    public ResponseEntity<Void> startInterview(final @PathVariable long id) {
        interviewService.startInterview(id);
        return ApiResponseBuilder.<Void> newBuilder()
                .withStatus(HttpStatus.OK).build();
    }

    @GetMapping(path = "/interviews/{link}/join")
    public ResponseEntity<InterviewDTO> joinInterview(final @PathVariable String link) {
        final InterviewDTO interviewDTO = interviewService.joinInterview(link);
        return ApiResponseBuilder.<InterviewDTO> newBuilder()
                .withStatus(HttpStatus.OK)
                .withBody(interviewDTO).build();
    }

    @GetMapping(path = "/interviews/{id}/participants")
    public ResponseEntity<List<InterviewSession.User>> getInterviewParticipants(final @PathVariable long id) {
        final List<InterviewSession.User> participants = interviewService.getParticipants(id);
        return ApiResponseBuilder.<List<InterviewSession.User>> newBuilder()
                .withStatus(HttpStatus.OK)
                .withBody(participants).build();
    }

    @PostMapping(path = "/interviews/{id}/end")
    public ResponseEntity<Void> endInterview(final @PathVariable long id) {
        interviewService.endInterview(id);
        return ApiResponseBuilder.<Void> newBuilder()
                .withStatus(HttpStatus.NO_CONTENT).build();
    }
}
