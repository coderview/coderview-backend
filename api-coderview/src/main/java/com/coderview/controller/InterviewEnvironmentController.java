package com.coderview.controller;

import com.coderview.dto.challenge.ChallengeDTO;
import com.coderview.dto.interview.NotesDTO;
import com.coderview.dto.interview.TabDTO;
import com.coderview.service.interview.InterviewEnvironmentService;
import com.coderview.util.ApiResponseBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InterviewEnvironmentController {

    private final InterviewEnvironmentService interviewEnvironmentService;

    public InterviewEnvironmentController(final InterviewEnvironmentService interviewEnvironmentService) {
        this.interviewEnvironmentService = interviewEnvironmentService;
    }

    @PostMapping(path = "/interview/environment/{environmentId}/tabs", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TabDTO> createTab(@PathVariable final long environmentId, @RequestBody final TabDTO tabDTO) {
        final TabDTO saved = interviewEnvironmentService.createTab(environmentId, tabDTO);
        return ApiResponseBuilder.<TabDTO> newBuilder()
                .withBody(saved)
                .withStatus(HttpStatus.CREATED).build();
    }

    @DeleteMapping(path = "/interview/environment/{environmentId}/tabs/{tabId}")
    public ResponseEntity<Void> deleteTab(@PathVariable final long environmentId, @PathVariable final String tabId) {
        interviewEnvironmentService.deleteTab(environmentId, tabId);
        return ApiResponseBuilder.<Void> newBuilder()
                .withStatus(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping(path = "/interview/environment/{environmentId}/notes", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> saveNotes(@PathVariable final long environmentId, @RequestBody final NotesDTO notesDTO) {
        interviewEnvironmentService.saveNotes(environmentId, notesDTO);
        return ApiResponseBuilder.<Void> newBuilder()
                .withStatus(HttpStatus.OK).build();
    }

    @GetMapping(path = "/interview/environment/{environmentId}/notes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NotesDTO> getNotes(@PathVariable final long environmentId) {
        final NotesDTO notesDTO = interviewEnvironmentService.getNotes(environmentId);
        return ApiResponseBuilder.<NotesDTO> newBuilder()
                .withBody(notesDTO)
                .withStatus(HttpStatus.OK).build();
    }

    @PostMapping(path = "/interview/environment/{environmentId}/challenges", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TabDTO> includeChallenge(@PathVariable final long environmentId, @RequestBody final ChallengeDTO challengeDTO) {
        final TabDTO created = interviewEnvironmentService.includeChallenge(environmentId, challengeDTO);
        return ApiResponseBuilder.<TabDTO> newBuilder()
                .withBody(created)
                .withStatus(HttpStatus.CREATED).build();
    }
}
