package com.coderview.controller;

import com.coderview.dto.user.UserDTO;
import com.coderview.service.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {

    private final UserService userService;

    public UsersController(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/users/register")
    public ResponseEntity<Object> registerNewUser(final @RequestBody UserDTO userDTO) {
        userService.register(userDTO);
        return buildStatusResponse(HttpStatus.CREATED);
    }

    private ResponseEntity<Object> buildStatusResponse(final HttpStatus status) {
        return ResponseEntity.status(status).build();
    }
}
