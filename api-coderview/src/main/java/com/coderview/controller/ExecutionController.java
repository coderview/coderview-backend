package com.coderview.controller;

import com.coderview.model.execution.ExecutionRequest;
import com.coderview.model.execution.ExecutionResults;
import com.coderview.service.execution.ExecutionService;
import com.coderview.util.ApiResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExecutionController {

    private final ExecutionService executionService;

    @Autowired
    public ExecutionController(final ExecutionService executionService) {
        this.executionService = executionService;
    }

    @GetMapping(path = "/execute/tab/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ExecutionResults> executeTab(@PathVariable final String id) {
        final ExecutionResults executionResults = executionService.executeTab(id);
        return ApiResponseBuilder.<ExecutionResults> newBuilder()
                .withBody(executionResults)
                .withStatus(HttpStatus.OK).build();
    }

    @PostMapping(path = "/execute", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ExecutionResults> execute(@RequestBody final ExecutionRequest request) {
        final ExecutionResults executionResults = executionService.execute(request);
        return ApiResponseBuilder.<ExecutionResults> newBuilder()
                .withBody(executionResults)
                .withStatus(HttpStatus.OK).build();
    }
}
