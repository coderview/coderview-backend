package com.coderview.transform.impl;

import com.coderview.domain.user.User;
import com.coderview.dto.user.UserDTO;
import com.coderview.transform.Transformer;
import org.springframework.stereotype.Component;

@Component
public class UserTransformer implements Transformer<UserDTO, User> {

    @Override
    public UserDTO toDto(final User user) {
        final UserDTO userDTO = new UserDTO();
        userDTO.setEmail(user.getEmail());
        userDTO.setName(user.getName());
        return userDTO;
    }

    @Override
    public User toEntity(final UserDTO userDTO) {
        final User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setName(userDTO.getName());
        return user;
    }
}
