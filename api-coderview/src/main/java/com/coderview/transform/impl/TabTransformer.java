package com.coderview.transform.impl;

import com.coderview.domain.interview.Tab;
import com.coderview.dto.interview.TabDTO;
import com.coderview.transform.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TabTransformer implements Transformer<TabDTO, Tab> {

    private final ProgrammingLanguageTransformer programmingLanguageTransformer;

    @Autowired
    public TabTransformer(ProgrammingLanguageTransformer programmingLanguageTransformer) {
        this.programmingLanguageTransformer = programmingLanguageTransformer;
    }

    @Override
    public TabDTO toDto(final Tab tab) {
        final TabDTO tabDTO = new TabDTO();
        tabDTO.setId(tab.getId());
        tabDTO.setReadOnlyId(tab.getReadOnlyId());
        tabDTO.setOrderNumber(tab.getOrderNumber());
        tabDTO.setTitle(tab.getTitle());
        tabDTO.setProgrammingLanguage(programmingLanguageTransformer.toDto(tab.getProgrammingLanguage()));
        tabDTO.setDescription(tab.getDescription());
        return tabDTO;
    }

    @Override
    public Tab toEntity(final TabDTO tabDTO) {
        throw new UnsupportedOperationException("TabDTO transformation to tab entity is not supported.");
    }
}
