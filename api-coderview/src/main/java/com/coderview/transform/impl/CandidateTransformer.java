package com.coderview.transform.impl;

import com.coderview.domain.interview.Candidate;
import com.coderview.dto.interview.CandidateDTO;
import com.coderview.transform.Transformer;
import org.springframework.stereotype.Component;

@Component
public class CandidateTransformer implements Transformer<CandidateDTO, Candidate> {

    @Override
    public CandidateDTO toDto(final Candidate candidate) {
        final CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setFirstName(candidate.getFirstName());
        candidateDTO.setLastName(candidate.getLastName());
        candidateDTO.setEmail(candidate.getEmail());
        return candidateDTO;
    }

    @Override
    public Candidate toEntity(final CandidateDTO candidateDTO) {
        final Candidate candidate = new Candidate();
        candidate.setFirstName(candidateDTO.getFirstName());
        candidate.setLastName(candidateDTO.getLastName());
        candidate.setEmail(candidateDTO.getEmail());
        return candidate;
    }
}
