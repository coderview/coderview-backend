package com.coderview.transform.impl;

import com.coderview.domain.interview.InterviewEnvironment;
import com.coderview.domain.interview.Tab;
import com.coderview.dto.interview.InterviewEnvironmentDTO;
import com.coderview.dto.interview.TabDTO;
import com.coderview.transform.Transformer;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class InterviewEnvironmentTransformer implements Transformer<InterviewEnvironmentDTO, InterviewEnvironment> {

    private final TabTransformer tabTransformer;

    public InterviewEnvironmentTransformer(final TabTransformer tabTransformer) {
        this.tabTransformer = tabTransformer;
    }

    @Override
    public InterviewEnvironmentDTO toDto(final InterviewEnvironment interviewEnvironment) {
        final InterviewEnvironmentDTO interviewEnvironmentDTO = new InterviewEnvironmentDTO();
        interviewEnvironmentDTO.setId(interviewEnvironment.getId());
        interviewEnvironmentDTO.setNotes(interviewEnvironment.getNotes());
        interviewEnvironmentDTO.setTabs(transformTabs(interviewEnvironment.getTabs()));
        return interviewEnvironmentDTO;
    }

    @Override
    public InterviewEnvironment toEntity(final InterviewEnvironmentDTO interviewEnvironmentDTO) {
        throw new UnsupportedOperationException("InterviewEnvironmentDTO transformation to InterviewEnvironment entity is not supported.");
    }

    private List<TabDTO> transformTabs(final List<Tab> tabs) {
        return Optional.ofNullable(tabs).stream()
                .flatMap(Collection::stream)
                .map(tabTransformer::toDto)
                .sorted(Comparator.comparingInt(TabDTO::getOrderNumber))
                .collect(Collectors.toList());
    }
}
