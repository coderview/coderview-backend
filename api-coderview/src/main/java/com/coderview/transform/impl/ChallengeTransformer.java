package com.coderview.transform.impl;

import com.coderview.common.constant.ChallengeDifficulty;
import com.coderview.domain.challenge.Challenge;
import com.coderview.dto.challenge.ChallengeDTO;
import com.coderview.transform.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChallengeTransformer implements Transformer<ChallengeDTO, Challenge> {

    private final ChallengeTypeTransformer challengeTypeTransformer;

    private final ProgrammingLanguageTransformer programmingLanguageTransformer;

    @Autowired
    public ChallengeTransformer(final ChallengeTypeTransformer challengeTypeTransformer,
                                final ProgrammingLanguageTransformer programmingLanguageTransformer) {
        this.challengeTypeTransformer = challengeTypeTransformer;
        this.programmingLanguageTransformer = programmingLanguageTransformer;
    }

    @Override
    public ChallengeDTO toDto(final Challenge challenge) {
        final ChallengeDTO challengeDTO = new ChallengeDTO();
        challengeDTO.setId(challenge.getId());
        challengeDTO.setTitle(challenge.getTitle());
        challengeDTO.setDescription(challenge.getDescription());
        challengeDTO.setSourceCode(challenge.getSourceCode());
        challengeDTO.setDifficulty(challenge.getDifficulty().getValue());
        challengeDTO.setType(challengeTypeTransformer.toDto(challenge.getType()));
        challengeDTO.setProgrammingLanguage(programmingLanguageTransformer.toDto(challenge.getProgrammingLanguage()));
        challengeDTO.setScope(challenge.getScope());
        return challengeDTO;
    }

    @Override
    public Challenge toEntity(final ChallengeDTO challengeDTO) {
        final Challenge challenge = new Challenge();
        challenge.setTitle(challengeDTO.getTitle());
        challenge.setDifficulty(ChallengeDifficulty.getByValue(challengeDTO.getDifficulty()));
        challenge.setDescription(challengeDTO.getDescription());
        challenge.setSourceCode(challengeDTO.getSourceCode());
        return challenge;
    }
}
