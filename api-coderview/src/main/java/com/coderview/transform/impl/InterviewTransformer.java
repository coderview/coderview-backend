package com.coderview.transform.impl;

import com.coderview.domain.interview.Interview;
import com.coderview.dto.interview.InterviewDTO;
import com.coderview.transform.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InterviewTransformer implements Transformer<InterviewDTO, Interview> {

    private final CandidateTransformer candidateTransformer;

    private final InterviewEnvironmentTransformer interviewEnvironmentTransformer;

    @Autowired
    public InterviewTransformer(final CandidateTransformer candidateTransformer,
                                final InterviewEnvironmentTransformer interviewEnvironmentTransformer) {
        this.candidateTransformer = candidateTransformer;
        this.interviewEnvironmentTransformer = interviewEnvironmentTransformer;
    }

    @Override
    public InterviewDTO toDto(final Interview interview) {
        final InterviewDTO interviewDTO = new InterviewDTO();
        interviewDTO.setId(interview.getId());
        interviewDTO.setTitle(interview.getTitle());
        interviewDTO.setLink(interview.getLink());
        interviewDTO.setInterviewer(interview.getInterviewer().getEmail());
        interviewDTO.setStatus(interview.getStatus());
        interviewDTO.setScheduledFor(interview.getScheduledFor());
        interviewDTO.setStartedAt(interview.getStartedAt());
        interviewDTO.setCompletedAt(interview.getCompletedAt());
        interviewDTO.setCandidate(candidateTransformer.toDto(interview.getCandidate()));
        interviewDTO.setEnvironment(interviewEnvironmentTransformer.toDto(interview.getEnvironment()));
        return interviewDTO;
    }

    @Override
    public Interview toEntity(final InterviewDTO interviewDTO) {
        final Interview interview = new Interview();
        interview.setTitle(interviewDTO.getTitle());
        interview.setScheduledFor(interviewDTO.getScheduledFor());
        interview.setCandidate(candidateTransformer.toEntity(interviewDTO.getCandidate()));
        return interview;
    }
}
