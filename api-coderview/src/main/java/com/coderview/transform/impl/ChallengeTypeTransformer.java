package com.coderview.transform.impl;

import com.coderview.domain.challenge.ChallengeType;
import com.coderview.dto.challenge.ChallengeTypeDTO;
import com.coderview.transform.Transformer;
import org.springframework.stereotype.Component;

@Component
public class ChallengeTypeTransformer implements Transformer<ChallengeTypeDTO, ChallengeType> {

    @Override
    public ChallengeTypeDTO toDto(final ChallengeType challengeType) {
        final ChallengeTypeDTO challengeTypeDTO = new ChallengeTypeDTO();
        challengeTypeDTO.setId(challengeType.getId());
        challengeTypeDTO.setName(challengeType.getName());
        return challengeTypeDTO;
    }

    @Override
    public ChallengeType toEntity(final ChallengeTypeDTO challengeTypeDTO) {
        final ChallengeType challengeType = new ChallengeType();
        challengeType.setName(challengeTypeDTO.getName());
        return challengeType;
    }
}
