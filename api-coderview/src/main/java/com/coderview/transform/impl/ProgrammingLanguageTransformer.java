package com.coderview.transform.impl;

import com.coderview.domain.interview.ProgrammingLanguage;
import com.coderview.dto.interview.ProgrammingLanguageDTO;
import com.coderview.transform.Transformer;
import org.springframework.stereotype.Component;

@Component
public class ProgrammingLanguageTransformer implements Transformer<ProgrammingLanguageDTO, ProgrammingLanguage> {

    @Override
    public ProgrammingLanguageDTO toDto(final ProgrammingLanguage programmingLanguage) {
        final ProgrammingLanguageDTO programmingLanguageDTO = new ProgrammingLanguageDTO();
        programmingLanguageDTO.setName(programmingLanguage.getName());
        programmingLanguageDTO.setVersion(programmingLanguage.getVersion());
        programmingLanguageDTO.setInfo(programmingLanguage.getInfo());
        programmingLanguageDTO.setDefaultTemplate(programmingLanguage.getDefaultTemplate());
        return programmingLanguageDTO;
    }

    @Override
    public ProgrammingLanguage toEntity(final ProgrammingLanguageDTO programmingLanguageDTO) {
        final ProgrammingLanguage programmingLanguage = new ProgrammingLanguage();
        programmingLanguage.setName(programmingLanguageDTO.getName());
        programmingLanguage.setVersion(programmingLanguageDTO.getVersion());
        programmingLanguage.setInfo(programmingLanguageDTO.getInfo());
        programmingLanguage.setDefaultTemplate(programmingLanguage.getDefaultTemplate());
        return programmingLanguage;
    }
}
