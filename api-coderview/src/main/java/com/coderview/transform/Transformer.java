package com.coderview.transform;

import com.coderview.domain.DomainEntity;
import com.coderview.dto.DomainDTO;

public interface Transformer<DTO extends DomainDTO, ENTITY extends DomainEntity> {

    DTO toDto(ENTITY entity);

    ENTITY toEntity(DTO dto);
}
