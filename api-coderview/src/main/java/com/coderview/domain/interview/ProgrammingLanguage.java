package com.coderview.domain.interview;

import com.coderview.common.constant.ProgrammingLanguageEnum;
import com.coderview.domain.DomainEntity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = ProgrammingLanguage.TABLE_NAME)
public class ProgrammingLanguage implements DomainEntity {

    public static final String TABLE_NAME = "programming_languages";

    private Long id;

    private ProgrammingLanguageEnum name;

    private String version;

    private String defaultTemplate;

    private String info;

    public ProgrammingLanguage() {
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", unique = true)
    @Enumerated(EnumType.STRING)
    public ProgrammingLanguageEnum getName() {
        return name;
    }

    public void setName(ProgrammingLanguageEnum name) {
        this.name = name;
    }

    @Basic
    @Column(name = "version")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Basic
    @Column(name = "default_template")
    public String getDefaultTemplate() {
        return defaultTemplate;
    }

    public void setDefaultTemplate(String defaultTemplate) {
        this.defaultTemplate = defaultTemplate;
    }

    @Basic
    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
