package com.coderview.domain.interview;

import com.coderview.domain.DomainEntity;
import com.coderview.persistence.hibernate.StringPrefixedSequenceIdGenerator;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = Tab.TABLE_NAME)
public class Tab implements DomainEntity {

    public static final String TABLE_NAME = "tabs";

    public static final String ID_PREFIX = "tab-";

    private String id;

    private String readOnlyId;

    private String title;

    private Integer orderNumber;

    private ProgrammingLanguage programmingLanguage;

    private InterviewEnvironment environment;

    private String description;

    public Tab() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tab_sequence")
    @GenericGenerator(
            name = "tab_sequence",
            strategy = "com.coderview.persistence.hibernate.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.PREFIX_PARAMETER, value = Tab.ID_PREFIX)
            }
    )
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "read_only_id")
    public String getReadOnlyId() {
        return readOnlyId;
    }

    public void setReadOnlyId(final String readOnlyId) {
        this.readOnlyId = readOnlyId;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "order_number")
    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer number) {
        this.orderNumber = number;
    }

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "fk_programming_language")
    public ProgrammingLanguage getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(ProgrammingLanguage programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "fk_interview_environment")
    public InterviewEnvironment getEnvironment() {
        return environment;
    }

    public void setEnvironment(InterviewEnvironment environment) {
        this.environment = environment;
    }

    @Lob
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
