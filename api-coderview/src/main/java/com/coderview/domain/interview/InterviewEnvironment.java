package com.coderview.domain.interview;

import com.coderview.domain.DomainEntity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = InterviewEnvironment.TABLE_NAME)
public class InterviewEnvironment implements DomainEntity {

    public static final String TABLE_NAME = "interview_environments";

    private Long id;

    private String notes;

    private List<Tab> tabs;

    private Interview interview;

    public InterviewEnvironment() {
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getNotes() {
        return notes;
    }

    @Basic
    @Column(name = "notes")
    public void setNotes(final String notes) {
        this.notes = notes;
    }

    @OneToMany(mappedBy = "environment", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public List<Tab> getTabs() {
        return tabs;
    }

    public void setTabs(final List<Tab> tabs) {
        this.tabs = tabs;
    }

    public void removeTab(final String id) {
        tabs.removeIf(tab -> tab.getId().equals(id));
    }

    @OneToOne(mappedBy = "environment", cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    public Interview getInterview() {
        return interview;
    }

    public void setInterview(final Interview interview) {
        this.interview = interview;
    }
}
