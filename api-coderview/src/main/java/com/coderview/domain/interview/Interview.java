package com.coderview.domain.interview;

import com.coderview.constant.InterviewStatus;
import com.coderview.domain.DomainEntity;
import com.coderview.domain.user.User;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = Interview.TABLE_NAME)
public class Interview implements DomainEntity {

    public static final String TABLE_NAME = "interviews";

    private Long id;

    private String title;

    private Date scheduledFor;

    private Date completedAt;

    private Date startedAt;

    private Date lastEditedAt;

    private InterviewStatus status;

    private String link;

    private InterviewEnvironment environment;

    private Candidate candidate;

    private User interviewer;

    public Interview() {
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "scheduled_for")
    public Date getScheduledFor() {
        return scheduledFor;
    }

    public void setScheduledFor(Date scheduledAt) {
        this.scheduledFor = scheduledAt;
    }

    @Basic
    @Column(name = "completed_at")
    public Date getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(Date completedAt) {
        this.completedAt = completedAt;
    }

    @Basic
    @Column(name = "started_at")
    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(final Date startedAt) {
        this.startedAt = startedAt;
    }

    @Basic
    @Column(name = "last_edited_at")
    public Date getLastEditedAt() {
        return lastEditedAt;
    }

    public void setLastEditedAt(Date lastEditedAt) {
        this.lastEditedAt = lastEditedAt;
    }

    @Basic
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    public InterviewStatus getStatus() {
        return status;
    }

    public void setStatus(InterviewStatus status) {
        this.status = status;
    }

    @Basic
    @Column(name = "link", unique = true, nullable = false)
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "fk_environment")
    public InterviewEnvironment getEnvironment() {
        return environment;
    }

    public void setEnvironment(InterviewEnvironment environment) {
        this.environment = environment;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "fk_candidate")
    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "fk_interviewer")
    public User getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(User interviewer) {
        this.interviewer = interviewer;
    }
}
