package com.coderview.domain.challenge;

import com.coderview.common.constant.ChallengeDifficulty;
import com.coderview.common.constant.ChallengeScope;
import com.coderview.domain.DomainEntity;
import com.coderview.domain.interview.ProgrammingLanguage;
import com.coderview.domain.user.User;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = Challenge.TABLE_NAME)
public class Challenge implements DomainEntity {

    public static final String TABLE_NAME = "challenges";

    private Long id;

    private String title;

    private String description;

    private String sourceCode;

    private ChallengeScope scope;

    private ChallengeDifficulty difficulty;

    private ChallengeType type;

    private User owner;

    private ProgrammingLanguage programmingLanguage;

    private Date lastModifiedDate;

    public Challenge() {
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    @Lob
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Lob
    @Column(name = "source_code")
    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(final String sourceCode) {
        this.sourceCode = sourceCode;
    }

    @Basic
    @Column(name = "scope")
    @Enumerated(EnumType.STRING)
    public ChallengeScope getScope() {
        return scope;
    }

    public void setScope(final ChallengeScope scope) {
        this.scope = scope;
    }

    @Basic
    @Column(name = "difficulty")
    @Enumerated(EnumType.STRING)
    public ChallengeDifficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(final ChallengeDifficulty difficulty) {
        this.difficulty = difficulty;
    }

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "fk_challenge_type")
    public ChallengeType getType() {
        return type;
    }

    public void setType(final ChallengeType type) {
        this.type = type;
    }

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "fk_owner")
    public User getOwner() {
        return owner;
    }

    public void setOwner(final User owner) {
        this.owner = owner;
    }

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "fk_programming_language")
    public ProgrammingLanguage getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(final ProgrammingLanguage programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    @Basic
    @Column(name = "last_modified_date")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(final Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
