package com.coderview.service.challenge;

import com.coderview.common.error.CoreCoderviewException;
import com.coderview.constant.AccountRole;
import com.coderview.common.constant.ChallengeDifficulty;
import com.coderview.common.constant.ChallengeScope;
import com.coderview.domain.challenge.Challenge;
import com.coderview.domain.challenge.ChallengeType;
import com.coderview.domain.interview.ProgrammingLanguage;
import com.coderview.domain.user.User;
import com.coderview.dto.challenge.ChallengeDTO;
import com.coderview.dto.interview.ProgrammingLanguageDTO;
import com.coderview.error.EntityNotFoundException;
import com.coderview.persistence.repository.ChallengeRepository;
import com.coderview.persistence.repository.ChallengeTypeRepository;
import com.coderview.persistence.repository.ProgrammingLanguageRepository;
import com.coderview.service.auth.AuthService;
import com.coderview.service.indexing.ChallengeDocumentIndexingService;
import com.coderview.transform.impl.ChallengeTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class ChallengeService {

    private final ChallengeRepository challengeRepository;

    private final ChallengeTypeRepository challengeTypeRepository;

    private final ProgrammingLanguageRepository programmingLanguageRepository;

    private final ChallengeTransformer challengeTransformer;

    private final AuthService authService;

    private final ChallengeDocumentIndexingService challengeDocumentIndexingService;

    @Autowired
    public ChallengeService(final ChallengeRepository challengeRepository,
                            final ChallengeTypeRepository challengeTypeRepository,
                            final ProgrammingLanguageRepository programmingLanguageRepository,
                            final ChallengeTransformer challengeTransformer,
                            final AuthService authService,
                            final ChallengeDocumentIndexingService challengeDocumentIndexingService) {
        this.challengeRepository = challengeRepository;
        this.challengeTypeRepository = challengeTypeRepository;
        this.programmingLanguageRepository = programmingLanguageRepository;
        this.challengeTransformer = challengeTransformer;
        this.authService = authService;
        this.challengeDocumentIndexingService = challengeDocumentIndexingService;
    }

    public ChallengeDTO getChallenge(final long id) {
        final Challenge challenge = findChallengeById(id);

        return challengeTransformer.toDto(challenge);
    }

    public ChallengeDTO createChallenge(final ChallengeDTO challengeDTO) {
        final Challenge challenge = challengeTransformer.toEntity(challengeDTO);
        final ChallengeType challengeType = findChallengeTypeById(challengeDTO.getType().getId());
        final ProgrammingLanguage programmingLanguage = findProgrammingLanguage(challengeDTO.getProgrammingLanguage());

        if (!isAllowedToCreate(challengeDTO.getScope())) {
            throw new CoreCoderviewException("User does not have permission to create GLOBAL scoped challenge.");
        }

        challenge.setScope(challengeDTO.getScope());
        challenge.setType(challengeType);
        challenge.setProgrammingLanguage(programmingLanguage);
        challenge.setOwner(authService.getCurrentlyAuthenticatedUser());
        challenge.setLastModifiedDate(new Date());

        final Challenge saved = challengeRepository.saveAndFlush(challenge);

        challengeDocumentIndexingService.index(saved.getId());

        return challengeTransformer.toDto(saved);
    }

    public ChallengeDTO updateChallenge(final long id, final ChallengeDTO challengeDTO) {
        final Challenge challenge = findChallengeById(id);
        final ChallengeType challengeType = findChallengeTypeById(challengeDTO.getType().getId());
        final ProgrammingLanguage programmingLanguage = findProgrammingLanguage(challengeDTO.getProgrammingLanguage());

        if (!isAllowedToUpdate(challenge)) {
            throw new CoreCoderviewException("User is not allowed to update challenge.");
        }

        challenge.setTitle(challengeDTO.getTitle());
        challenge.setDescription(challengeDTO.getDescription());
        challenge.setSourceCode(challengeDTO.getSourceCode());
        challenge.setDifficulty(ChallengeDifficulty.getByValue(challengeDTO.getDifficulty()));
        challenge.setType(challengeType);
        challenge.setProgrammingLanguage(programmingLanguage);
        challenge.setLastModifiedDate(new Date());

        final Challenge saved = challengeRepository.saveAndFlush(challenge);

        challengeDocumentIndexingService.index(saved.getId());

        return challengeTransformer.toDto(saved);
    }

    public void deleteChallenge(final long id) {
        challengeDocumentIndexingService.delete(id);
        challengeRepository.deleteById(id);
    }

    private Challenge findChallengeById(final long id) {
        return challengeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Challenge with id %d was not found.", id)));
    }

    private ChallengeType findChallengeTypeById(final long id) {
        return challengeTypeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("ChallengeType with id %d was not found.", id)));
    }

    private ProgrammingLanguage findProgrammingLanguage(final ProgrammingLanguageDTO programmingLanguageDTO) {
        return Optional.ofNullable(programmingLanguageRepository.findByName(programmingLanguageDTO.getName()))
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "%s ProgrammingLanguage was not found.", programmingLanguageDTO.getName().name())));
    }

    private boolean isAllowedToCreate(final ChallengeScope scope) {
        return scope == ChallengeScope.GLOBAL
                ? authService.getCurrentlyAuthenticatedUser().getRole() == AccountRole.ADMIN
                : Boolean.TRUE;
    }

    private boolean isAllowedToUpdate(final Challenge challenge) {
        final User user = authService.getCurrentlyAuthenticatedUser();
        if (challenge.getScope() == ChallengeScope.GLOBAL) {
            return user.getRole() == AccountRole.ADMIN;
        } else {
            return challenge.getOwner().equals(user);
        }
    }
}
