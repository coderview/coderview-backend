package com.coderview.service.challenge;

import com.coderview.domain.challenge.ChallengeType;
import com.coderview.dto.challenge.ChallengeTypeDTO;
import com.coderview.persistence.repository.ChallengeTypeRepository;
import com.coderview.transform.impl.ChallengeTypeTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChallengeTypeService {

    private final ChallengeTypeRepository challengeTypeRepository;

    private final ChallengeTypeTransformer challengeTypeTransformer;

    @Autowired
    public ChallengeTypeService(final ChallengeTypeRepository challengeTypeRepository,
                                final ChallengeTypeTransformer challengeTypeTransformer) {
        this.challengeTypeRepository = challengeTypeRepository;
        this.challengeTypeTransformer = challengeTypeTransformer;
    }

    public void createChallengeType(final ChallengeTypeDTO challengeTypeDTO) {
        final ChallengeType challengeType = challengeTypeTransformer.toEntity(challengeTypeDTO);

        challengeTypeRepository.save(challengeType);
    }

    public List<ChallengeTypeDTO> findAll() {
        return challengeTypeRepository.findAll().stream()
                .map(challengeTypeTransformer::toDto)
                .collect(Collectors.toList());
    }
}
