package com.coderview.service.execution;

import com.coderview.model.execution.ExecutionContext;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.api.model.Volume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DockerService {

    private final DockerClient dockerClient;

    @Autowired
    public DockerService(final DockerClient dockerClient) {
        this.dockerClient = dockerClient;
    }

    public void run(final ExecutionContext context) {
        dockerClient.startContainerCmd(createContainer(context).getId()).exec();
    }

    private CreateContainerResponse createContainer(final ExecutionContext context) {
        return dockerClient.createContainerCmd(context.getDockerImage())
                .withHostConfig(new HostConfig().withAutoRemove(true))
                .withBinds(new Bind(context.getDockerBind(), new Volume(context.getDockerVolume())))
                .exec();
    }
}
