package com.coderview.service.execution;

import com.coderview.common.error.CoreCoderviewException;
import com.coderview.error.DirectoryAlreadyExistsException;
import com.coderview.model.execution.ExecutionContext;
import com.coderview.model.execution.ExecutionResults;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

@Service
public class ExecutionDirectoryService {

    private static final int READ_RETRY_COUNT = 16;

    private static final int SLEEP_FOR = 250; // milliseconds

    public void prepare(final ExecutionContext context) throws IOException {
        createDirectory(context.getDirectory());
        createDirectory(context.getExecutionOutputDirectory());
        createFile(context.getInputFile(), context.getSourceCode());
    }

    public ExecutionResults readExecutionResults(final ExecutionContext context) throws IOException, InterruptedException {
        final String input = readFile(context.getInputFile());
        final String output = readFile(context.getExecutionOutputFile());

        return new ExecutionResults()
                .setExecutionId(context.getExecutionId())
                .setProgrammingLanguage(context.getProgrammingLanguage().getValue())
                .setExecutedLinesCount(input.lines().count())
                .setRuntime(parseRuntime(output))
                .setOutput(parseOutput(output));
    }

    public void remove(final ExecutionContext context) throws IOException {
        deleteDirectory(context.getDirectory());
    }

    private void createDirectory(final String name) throws IOException {
        final File directory = FileUtils.getFile(name);

        if (!directory.exists()) {
            FileUtils.forceMkdir(directory);
        } else {
            throw new DirectoryAlreadyExistsException(name);
        }
    }

    private void deleteDirectory(final String name) throws IOException {
        final File directory = FileUtils.getFile(name);

        if (directory.exists()) {
            FileUtils.deleteDirectory(directory);
        }
    }

    private void createFile(final String name, final String content) throws IOException {
        final File file = new File(name);
        FileUtils.touch(file);
        FileUtils.write(file, content, StandardCharsets.UTF_8.name());
    }

    private String readFile(final String name) throws IOException, InterruptedException {
        final File file = FileUtils.getFile(name);

        for (int count = 0; count < READ_RETRY_COUNT; count++) {
            if (file.exists()) {
                final String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
                if (StringUtils.isNotBlank(content)) {
                    return content;
                }
            }
            TimeUnit.MILLISECONDS.sleep(SLEEP_FOR);
        }

        throw new CoreCoderviewException("Failed to read file: " + name);
    }

    private String parseOutput(final String output) {
        if (output.lines().count() > 1) {
            return output.substring(output.indexOf('\n') + 1); // Skipping first line because it contains runtime information
        } else {
            return StringUtils.EMPTY;
        }
    }

    private double parseRuntime(final String output) {
        return output.lines()
                .findFirst()
                .filter(StringUtils::isNotBlank)
                .map(line -> line.split(":"))
                .filter(parts -> parts.length == 2)
                .map(parts -> Double.parseDouble(parts[1].trim()))
                .orElse(Double.NaN);
    }
}
