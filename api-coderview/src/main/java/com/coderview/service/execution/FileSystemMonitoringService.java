package com.coderview.service.execution;

import com.coderview.common.error.CoreCoderviewException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.TimeUnit;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

@Service
public class FileSystemMonitoringService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemMonitoringService.class);

    private static final int TIMEOUT = 30_000; //milliseconds
    private static final int WAIT_FOR = 250; //milliseconds

    public WatchService register(final String directory) throws IOException {
        final WatchService watchService = FileSystems.getDefault().newWatchService();
        Paths.get(directory).register(watchService, ENTRY_CREATE);
        return watchService;
    }

    public void monitor(final WatchService watchService) {
        for (int time = 0; time < TIMEOUT; time += WAIT_FOR) {
            try {
                LOGGER.info("monitor time: " + time);
                WatchKey key = watchService.poll(WAIT_FOR, TimeUnit.MILLISECONDS);
                if (key != null) {
                    for (WatchEvent<?> event : key.pollEvents()) {
                        if (event.kind() == ENTRY_CREATE) {
                            return;
                        }
                    }
                    if (!key.reset()) {
                        break;
                    }
                }
            } catch (InterruptedException e) {
                return;
            }
        }

        throw new CoreCoderviewException("Execution timeout.");
    }
}
