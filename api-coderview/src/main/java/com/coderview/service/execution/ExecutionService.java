package com.coderview.service.execution;

import com.coderview.common.integration.etherpadlite.adapter.EtherpadLitePadAdapter;
import com.coderview.domain.interview.Tab;
import com.coderview.error.EntityNotFoundException;
import com.coderview.model.execution.ExecutionRequest;
import com.coderview.model.execution.ExecutionResults;
import com.coderview.persistence.repository.TabRepository;
import com.coderview.processor.ExecutionProcessor;
import com.coderview.service.websocket.TopicBroadcastService;
import com.coderview.util.TopicBroadcastUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExecutionService {

    private final ExecutionProcessor executionProcessor;

    private final TabRepository tabRepository;

    private final EtherpadLitePadAdapter etherpadLitePadAdapter;

    private final TopicBroadcastService topicBroadcastService;

    @Autowired
    public ExecutionService(final ExecutionProcessor executionProcessor,
                            final TabRepository tabRepository,
                            final EtherpadLitePadAdapter etherpadLitePadAdapter,
                            final TopicBroadcastService topicBroadcastService) {
        this.executionProcessor = executionProcessor;
        this.tabRepository = tabRepository;
        this.etherpadLitePadAdapter = etherpadLitePadAdapter;
        this.topicBroadcastService = topicBroadcastService;
    }

    public ExecutionResults execute(final ExecutionRequest request) {
        return executionProcessor.execute(request.getProgrammingLanguage(), request.getSourceCode());
    }

    public ExecutionResults executeTab(final String id) {
        final Tab tab = tabRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(id));
        final String sourceCode = etherpadLitePadAdapter.getText(id);

        final ExecutionResults results = executionProcessor.execute(tab.getProgrammingLanguage().getName(), sourceCode);

        if (TopicBroadcastUtils.isBroadcastRequired(tab.getEnvironment().getInterview())) {
            topicBroadcastService.executionOutput(tab.getEnvironment().getInterview().getLink(), results);
        }

        return results;
    }
}
