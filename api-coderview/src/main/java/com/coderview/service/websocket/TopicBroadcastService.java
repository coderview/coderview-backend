package com.coderview.service.websocket;

import com.coderview.dto.interview.TabDTO;
import com.coderview.model.execution.ExecutionResults;
import com.coderview.model.session.InterviewSession;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import static com.coderview.constant.STOMPConstants.LINK;
import static com.coderview.constant.STOMPConstants.TOPIC_DELETE_TAB;
import static com.coderview.constant.STOMPConstants.TOPIC_END_INTERVIEW;
import static com.coderview.constant.STOMPConstants.TOPIC_EXECUTION_OUTPUT;
import static com.coderview.constant.STOMPConstants.TOPIC_NEW_TAB;
import static com.coderview.constant.STOMPConstants.TOPIC_PARTICIPANT_JOINED;
import static com.coderview.constant.STOMPConstants.TOPIC_PARTICIPANT_LEFT;

@Service
public class TopicBroadcastService {

    private final SimpMessagingTemplate messagingTemplate;

    public TopicBroadcastService(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    public void newTab(final String interviewLink, final TabDTO tabDTO) {
        messagingTemplate.convertAndSend(TOPIC_NEW_TAB.replace(LINK, interviewLink), tabDTO);
    }

    public void deleteTab(final String interviewLink, final String id) {
        messagingTemplate.convertAndSend(TOPIC_DELETE_TAB.replace(LINK, interviewLink), id);
    }

    public void executionOutput(final String interviewLink, final ExecutionResults results) {
        messagingTemplate.convertAndSend(TOPIC_EXECUTION_OUTPUT.replace(LINK, interviewLink), results);
    }

    public void participantJoined(final String interviewLink, final InterviewSession.User participant) {
        messagingTemplate.convertAndSend(TOPIC_PARTICIPANT_JOINED.replace(LINK, interviewLink), participant);
    }

    public void participantLeft(final String interviewLink, final InterviewSession.User participant) {
        messagingTemplate.convertAndSend(TOPIC_PARTICIPANT_LEFT.replace(LINK, interviewLink), participant);
    }

    public void endInterview(final String interviewLink) {
        messagingTemplate.convertAndSend(TOPIC_END_INTERVIEW.replace(LINK, interviewLink), Boolean.TRUE);
    }
}
