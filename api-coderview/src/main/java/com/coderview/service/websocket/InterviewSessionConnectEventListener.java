package com.coderview.service.websocket;

import com.coderview.domain.interview.Interview;
import com.coderview.model.session.InterviewSession;
import com.coderview.persistence.repository.InterviewRepository;
import com.coderview.service.interview.InterviewSessionsHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.coderview.constant.SessionConstants.LINK;
import static com.coderview.constant.SessionConstants.NATIVE_HEADERS;
import static com.coderview.constant.SessionConstants.SIMP_CONNECT_MESSAGE;
import static com.coderview.constant.SessionConstants.SIMP_SESSION_ID;
import static com.coderview.constant.SessionConstants.USER;

@Component
public class InterviewSessionConnectEventListener implements ApplicationListener<SessionConnectedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InterviewSessionConnectEventListener.class);

    private final InterviewRepository interviewRepository;

    private final TopicBroadcastService topicBroadcastService;

    @Autowired
    public InterviewSessionConnectEventListener(final InterviewRepository interviewRepository,
                                                final TopicBroadcastService topicBroadcastService) {
        this.interviewRepository = interviewRepository;
        this.topicBroadcastService = topicBroadcastService;
    }

    @Override
    public void onApplicationEvent(SessionConnectedEvent sessionConnectedEvent) {
        LOGGER.info("New connection {}", sessionConnectedEvent.toString());

        Optional.ofNullable(interviewRepository.findByLink(getLink(sessionConnectedEvent)))
                .map(InterviewSessionsHolder::getOrStartSession)
                .map(session -> {
                    final InterviewSession.User participant = getParticipant(session.getInterview(),
                            getUserInfo(sessionConnectedEvent),
                            getSessionId(sessionConnectedEvent));

                    session.addParticipant(participant);

                    topicBroadcastService.participantJoined(session.getInterview().getLink(), participant);

                    return session;
                });
    }

    private String getSessionId(final SessionConnectedEvent sessionConnectedEvent) {
        return sessionConnectedEvent.getMessage().getHeaders().get(SIMP_SESSION_ID, String.class);
    }

    private String getLink(final SessionConnectedEvent sessionConnectedEvent) {
        return Optional.ofNullable((List) getNativeHeaders(sessionConnectedEvent).get(LINK))
                .filter(userInfo -> !userInfo.isEmpty())
                .map(userInfo -> (String) userInfo.get(0))
                .orElse(null);
    }

    private String getUserInfo(final SessionConnectedEvent sessionConnectedEvent) {
        return Optional.ofNullable((List) getNativeHeaders(sessionConnectedEvent).get(USER))
                .filter(userInfo -> !userInfo.isEmpty())
                .map(userInfo -> (String) userInfo.get(0))
                .orElse(null);
    }

    private Map getNativeHeaders(final SessionConnectedEvent sessionConnectedEvent) {
        return Optional.ofNullable(sessionConnectedEvent)
                .map(event -> event.getMessage().getHeaders())
                .map(headers -> headers.get(SIMP_CONNECT_MESSAGE, GenericMessage.class))
                .map(GenericMessage::getHeaders)
                .map(headers -> headers.get(NATIVE_HEADERS, Map.class))
                .orElse(Collections.emptyMap());
    }

    private InterviewSession.User getParticipant(final Interview interview, final String userInfo, final String sessionId) {
        if (isInterviewer(interview, userInfo)) {
            return new InterviewSession.User(sessionId)
                    .setEmail(interview.getInterviewer().getEmail())
                    .setName(interview.getInterviewer().getName());
        } else {
            return new InterviewSession.User(sessionId)
                    .setEmail(interview.getCandidate().getEmail())
                    .setName(interview.getCandidate().getFirstName());
        }
    }

    private boolean isInterviewer(final Interview interview, final String userInfo) {
        return Optional.ofNullable(interview.getInterviewer())
                .map(interviewer -> interviewer.getEmail().equals(userInfo))
                .orElse(Boolean.FALSE);
    }
}
