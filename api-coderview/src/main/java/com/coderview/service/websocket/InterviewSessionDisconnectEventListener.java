package com.coderview.service.websocket;

import com.coderview.model.session.InterviewSession;
import com.coderview.service.interview.InterviewSessionsHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.coderview.constant.SessionConstants.LINK;
import static com.coderview.constant.SessionConstants.NATIVE_HEADERS;
import static com.coderview.constant.SessionConstants.SIMP_CONNECT_MESSAGE;
import static com.coderview.constant.SessionConstants.SIMP_SESSION_ID;

@Component
public class InterviewSessionDisconnectEventListener implements ApplicationListener<SessionDisconnectEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InterviewSessionDisconnectEventListener.class);

    private final TopicBroadcastService topicBroadcastService;

    @Autowired
    public InterviewSessionDisconnectEventListener(final TopicBroadcastService topicBroadcastService) {
        this.topicBroadcastService = topicBroadcastService;
    }

    @Override
    public void onApplicationEvent(final SessionDisconnectEvent sessionDisconnectEvent) {
        LOGGER.info("User disconnected {}", sessionDisconnectEvent);

        final String sessionId = getSessionId(sessionDisconnectEvent);
        final InterviewSession interviewSession = InterviewSessionsHolder.getSessionByUserSessionId(sessionId);

        Optional.ofNullable(interviewSession)
                .map(session -> session.getParticipantBySessionId(sessionId))
                .ifPresent(participant -> {
                    interviewSession.removeParticipant(participant);
                    topicBroadcastService.participantLeft(interviewSession.getInterview().getLink(), participant);
                });
    }

    private String getSessionId(final SessionDisconnectEvent sessionDisconnectEvent) {
        return sessionDisconnectEvent.getMessage().getHeaders().get(SIMP_SESSION_ID, String.class);
    }

    private String getLink(final SessionConnectedEvent sessionConnectedEvent) {
        return Optional.ofNullable((List) getNativeHeaders(sessionConnectedEvent).get(LINK))
                .filter(userInfo -> !userInfo.isEmpty())
                .map(userInfo -> (String) userInfo.get(0))
                .orElse(null);
    }

    private Map getNativeHeaders(final SessionConnectedEvent sessionConnectedEvent) {
        return Optional.ofNullable(sessionConnectedEvent)
                .map(event -> event.getMessage().getHeaders())
                .map(headers -> headers.get(SIMP_CONNECT_MESSAGE, GenericMessage.class))
                .map(GenericMessage::getHeaders)
                .map(headers -> headers.get(NATIVE_HEADERS, Map.class))
                .orElse(Collections.emptyMap());
    }
}
