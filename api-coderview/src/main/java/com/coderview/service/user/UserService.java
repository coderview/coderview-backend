package com.coderview.service.user;

import com.coderview.constant.AccountRole;
import com.coderview.constant.AccountStatus;
import com.coderview.domain.user.User;
import com.coderview.dto.user.UserDTO;
import com.coderview.persistence.repository.UserRepository;
import com.coderview.transform.impl.UserTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserService {

    private final UserTransformer userTransformer;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(final UserTransformer userTransformer,
                       final UserRepository userRepository,
                       final PasswordEncoder passwordEncoder) {
        this.userTransformer = userTransformer;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void register(final UserDTO userDTO) {
        final User user = userTransformer.toEntity(userDTO);

        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setRole(AccountRole.REGULAR);
        user.setStatus(AccountStatus.ACTIVE);
        user.setCreatedAt(new Date());

        userRepository.save(user);
    }
}
