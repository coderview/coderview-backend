package com.coderview.service.indexing;

import com.coderview.common.constant.SolrConstants;
import com.coderview.common.error.CoreCoderviewException;
import com.coderview.common.solr.document.InterviewDocument;
import com.coderview.domain.interview.Interview;
import com.coderview.persistence.repository.InterviewRepository;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class InterviewDocumentIndexingService {

    private static final String ERROR_MESSAGE_TEMPLATE = "Interview with id: %d was not found, unable to create interview document.";

    private final HttpSolrClient solrClient;

    private final InterviewRepository interviewRepository;

    @Autowired
    public InterviewDocumentIndexingService(final InterviewRepository interviewRepository,
                                            final @Value("${solr.baseUrl}") String solrBaseUrl) {
        this.interviewRepository = interviewRepository;
        this.solrClient = new HttpSolrClient
                .Builder(solrBaseUrl.replace(SolrConstants.INDEX_KEY, SolrConstants.INTERVIEW_INDEX))
                .build();
    }

    public void index(final Long id) {
        final Interview interview = getInterview(id);
        final InterviewDocument interviewDocument = createDocument(interview);

        try {
            solrClient.addBean(interviewDocument);
            solrClient.commit();
        } catch (IOException | SolrServerException e) {
            throw new CoreCoderviewException(e);
        }
    }

    public void delete(final Long id) {
        try {
            solrClient.deleteById(String.valueOf(id));
            solrClient.commit();
        } catch (IOException | SolrServerException e) {
            throw new CoreCoderviewException(e);
        }
    }

    private Interview getInterview(final Long id) {
        return interviewRepository.findById(id)
                .orElseThrow(() -> new CoreCoderviewException(String.format(ERROR_MESSAGE_TEMPLATE, id)));
    }

    private InterviewDocument createDocument(final Interview interview) {
        final InterviewDocument interviewDocument = new InterviewDocument();

        interviewDocument.setId(String.valueOf(interview.getId()));
        interviewDocument.setTitle(interview.getTitle());
        interviewDocument.setStatus(interview.getStatus().name());
        interviewDocument.setLastEditedAt(interview.getLastEditedAt());
        interviewDocument.setScheduledFor(interview.getScheduledFor());
        interviewDocument.setCandidateFirstName(interview.getCandidate().getFirstName());
        interviewDocument.setCandidateLastName(interview.getCandidate().getLastName());
        interviewDocument.setCandidateEmail(interview.getCandidate().getEmail());
        interviewDocument.setInterviewer(interview.getInterviewer().getEmail());
        interviewDocument.setLink(interview.getLink());

        return interviewDocument;
    }
}
