package com.coderview.service.indexing;

import com.coderview.common.constant.SolrConstants;
import com.coderview.common.error.CoreCoderviewException;
import com.coderview.common.solr.document.ChallengeDocument;
import com.coderview.domain.challenge.Challenge;
import com.coderview.persistence.repository.ChallengeRepository;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ChallengeDocumentIndexingService {

    private static final String ERROR_MESSAGE_TEMPLATE = "Challenge with id: %d was not found, unable to create challenge document.";

    private final HttpSolrClient solrClient;

    private final ChallengeRepository challengeRepository;

    public ChallengeDocumentIndexingService(final ChallengeRepository challengeRepository,
                                            final @Value("${solr.baseUrl}") String solrBaseUrl) {
        this.challengeRepository = challengeRepository;
        this.solrClient = new HttpSolrClient
                .Builder(solrBaseUrl.replace(SolrConstants.INDEX_KEY, SolrConstants.CHALLENGE_INDEX))
                .build();
    }

    public void index(final long id) {
        final Challenge challenge = getChallenge(id);
        final ChallengeDocument challengeDocument = createDocument(challenge);

        try {
            solrClient.addBean(challengeDocument);
            solrClient.commit();
        } catch (IOException | SolrServerException e) {
            throw new CoreCoderviewException(e);
        }
    }

    public void delete(final Long id) {
        try {
            solrClient.deleteById(String.valueOf(id));
            solrClient.commit();
        } catch (IOException | SolrServerException e) {
            throw new CoreCoderviewException(e);
        }
    }

    private Challenge getChallenge(final long id) {
        return challengeRepository.findById(id)
                .orElseThrow(() -> new CoreCoderviewException(String.format(ERROR_MESSAGE_TEMPLATE, id)));
    }

    private ChallengeDocument createDocument(final Challenge challenge) {
        final ChallengeDocument challengeDocument = new ChallengeDocument();
        challengeDocument.setId(String.valueOf(challenge.getId()));
        challengeDocument.setTitle(challenge.getTitle());
        challengeDocument.setDescription(Jsoup.parse(challenge.getDescription()).text());
        challengeDocument.setScope(challenge.getScope().name());
        challengeDocument.setDifficulty(challenge.getDifficulty().getValue());
        challengeDocument.setType(challenge.getType().getName());
        challengeDocument.setOwner(challenge.getOwner().getEmail());
        challengeDocument.setProgrammingLanguage(challenge.getProgrammingLanguage().getName().getValue());
        challengeDocument.setLastModifiedDate(challenge.getLastModifiedDate());
        return challengeDocument;
    }
}
