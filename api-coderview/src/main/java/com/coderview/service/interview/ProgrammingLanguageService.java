package com.coderview.service.interview;

import com.coderview.dto.interview.ProgrammingLanguageDTO;
import com.coderview.persistence.repository.ProgrammingLanguageRepository;
import com.coderview.transform.impl.ProgrammingLanguageTransformer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProgrammingLanguageService {

    private final ProgrammingLanguageRepository programmingLanguageRepository;

    private final ProgrammingLanguageTransformer programmingLanguageTransformer;

    public ProgrammingLanguageService(final ProgrammingLanguageRepository programmingLanguageRepository,
                                      final ProgrammingLanguageTransformer programmingLanguageTransformer) {
        this.programmingLanguageRepository = programmingLanguageRepository;
        this.programmingLanguageTransformer = programmingLanguageTransformer;
    }

    public List<ProgrammingLanguageDTO> findAll() {
        return programmingLanguageRepository.findAll().stream()
                .map(programmingLanguageTransformer::toDto)
                .collect(Collectors.toList());
    }
}
