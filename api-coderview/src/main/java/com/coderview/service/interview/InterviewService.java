package com.coderview.service.interview;

import com.coderview.common.error.CoreCoderviewException;
import com.coderview.constant.InterviewStatus;
import com.coderview.domain.interview.Interview;
import com.coderview.domain.interview.InterviewEnvironment;
import com.coderview.domain.user.User;
import com.coderview.dto.interview.InterviewDTO;
import com.coderview.error.EntityNotFoundException;
import com.coderview.error.InvalidInterviewStatusException;
import com.coderview.model.session.InterviewSession;
import com.coderview.persistence.repository.InterviewRepository;
import com.coderview.service.auth.AuthService;
import com.coderview.service.indexing.InterviewDocumentIndexingService;
import com.coderview.service.websocket.TopicBroadcastService;
import com.coderview.transform.impl.InterviewTransformer;
import com.coderview.util.LinkGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class InterviewService {

    private final InterviewTransformer interviewTransformer;

    private final AuthService authService;

    private final InterviewRepository interviewRepository;

    private final InterviewDocumentIndexingService interviewDocumentIndexingService;

    private final TopicBroadcastService topicBroadcastService;

    @Autowired
    public InterviewService(final InterviewTransformer interviewTransformer,
                            final AuthService authService,
                            final InterviewRepository interviewRepository,
                            final InterviewDocumentIndexingService interviewDocumentIndexingService,
                            final TopicBroadcastService topicBroadcastService) {
        this.interviewTransformer = interviewTransformer;
        this.authService = authService;
        this.interviewRepository = interviewRepository;
        this.interviewDocumentIndexingService = interviewDocumentIndexingService;
        this.topicBroadcastService = topicBroadcastService;
    }

    public void scheduleNew(final InterviewDTO interviewDTO) {
        final User user = authService.getCurrentlyAuthenticatedUser();
        final Interview interview = interviewTransformer.toEntity(interviewDTO);

        interview.setInterviewer(user);
        interview.setStatus(InterviewStatus.SCHEDULED);
        interview.setLink(LinkGenerator.generate());
        interview.setEnvironment(new InterviewEnvironment());
        interview.setLastEditedAt(new Date());

        final Long id = interviewRepository.saveAndFlush(interview).getId();
        interviewDocumentIndexingService.index(id);
    }

    public InterviewDTO getInterview(final long id) {
        final Interview interview = findInterview(id);
        return interviewTransformer.toDto(interview);
    }

    public void updateInterview(final long id, final InterviewDTO interviewDTO) {
        final Interview interview = findInterview(id);
        patch(interview, interviewDTO);
        interviewRepository.saveAndFlush(interview);
        interviewDocumentIndexingService.index(id);
    }

    public void deleteInterview(final long id) {
        interviewDocumentIndexingService.delete(id);
        interviewRepository.deleteById(id);
    }

    public void startInterview(final long id) {
        final Interview interview = findInterview(id);

        if (interview.getStatus() == InterviewStatus.SCHEDULED) {
            interview.setStatus(InterviewStatus.IN_PROGRESS);
            interview.setLastEditedAt(new Date());
            interview.setStartedAt(new Date());
        } else {
            throw new InvalidInterviewStatusException("Cannot start interview due to invalid status, status should be SCHEDULED");
        }

        interviewRepository.saveAndFlush(interview);
        interviewDocumentIndexingService.index(id);
    }

    public void endInterview(final long id) {
        final Interview interview = findInterview(id);

        if (interview.getStatus() != InterviewStatus.IN_PROGRESS) {
            throw new InvalidInterviewStatusException("Cannot end interview due to invalid status, status should be IN_PROGRESS.");
        }

        if (!interview.getInterviewer().equals(authService.getCurrentlyAuthenticatedUser())) {
            throw new CoreCoderviewException("Unauthorized request.");
        }

        interview.setStatus(InterviewStatus.COMPLETED);
        interview.setLastEditedAt(new Date());
        interview.setCompletedAt(new Date());

        interviewRepository.saveAndFlush(interview);
        interviewDocumentIndexingService.index(id);

        InterviewSessionsHolder.stopSession(interview);

        topicBroadcastService.endInterview(interview.getLink());
    }

    public InterviewDTO joinInterview(final String link) {
        final Interview interview = Optional.ofNullable(interviewRepository.findByLink(link))
                .orElseThrow(() -> new EntityNotFoundException(String.format("No interview associated with %s link exists.", link)));

        if (interview.getStatus() == InterviewStatus.IN_PROGRESS) {
            return interviewTransformer.toDto(interview);
        } else if (interview.getStatus() == InterviewStatus.SCHEDULED) {
            throw new InvalidInterviewStatusException("Interview not started.");
        } else if (interview.getStatus() == InterviewStatus.COMPLETED) {
            throw new InvalidInterviewStatusException("Interview completed.");
        }

        throw new InvalidInterviewStatusException("Unknown interview status.");
    }

    public List<InterviewSession.User> getParticipants(final long id) {
        return Optional.ofNullable(InterviewSessionsHolder.getSession(findInterview(id)))
                .map(InterviewSession::getParticipants)
                .orElse(Collections.emptyList());
    }

    private Interview patch(final Interview interview, final InterviewDTO interviewDTO) {
        interview.setTitle(interviewDTO.getTitle());
        interview.setScheduledFor(interviewDTO.getScheduledFor());
        interview.getCandidate().setFirstName(interviewDTO.getCandidate().getFirstName());
        interview.getCandidate().setLastName(interviewDTO.getCandidate().getLastName());
        interview.getCandidate().setEmail(interviewDTO.getCandidate().getEmail());
        interview.setLastEditedAt(new Date());

        return interview;
    }

    private Interview findInterview(final long id) {
        return interviewRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(id));
    }
}
