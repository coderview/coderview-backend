package com.coderview.service.interview;

import com.coderview.common.integration.etherpadlite.adapter.EtherpadLitePadAdapter;
import com.coderview.domain.challenge.Challenge;
import com.coderview.domain.interview.InterviewEnvironment;
import com.coderview.domain.interview.ProgrammingLanguage;
import com.coderview.domain.interview.Tab;
import com.coderview.dto.challenge.ChallengeDTO;
import com.coderview.dto.interview.NotesDTO;
import com.coderview.dto.interview.TabDTO;
import com.coderview.error.EntityNotFoundException;
import com.coderview.persistence.repository.ChallengeRepository;
import com.coderview.persistence.repository.InterviewEnvironmentRepository;
import com.coderview.persistence.repository.ProgrammingLanguageRepository;
import com.coderview.persistence.repository.TabRepository;
import com.coderview.service.websocket.TopicBroadcastService;
import com.coderview.transform.impl.TabTransformer;
import com.coderview.util.TabUtils;
import com.coderview.util.TopicBroadcastUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InterviewEnvironmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InterviewEnvironmentService.class);

    private final TopicBroadcastService topicBroadcastService;

    private final TabTransformer tabTransformer;

    private final TabRepository tabRepository;

    private final ProgrammingLanguageRepository programmingLanguageRepository;

    private final InterviewEnvironmentRepository interviewEnvironmentRepository;

    private final ChallengeRepository challengeRepository;

    private final EtherpadLitePadAdapter etherpadLitePadAdapter;

    @Autowired
    public InterviewEnvironmentService(final TopicBroadcastService topicBroadcastService,
                                       final TabTransformer tabTransformer,
                                       final TabRepository tabRepository,
                                       final ProgrammingLanguageRepository programmingLanguageRepository,
                                       final InterviewEnvironmentRepository interviewEnvironmentRepository,
                                       final ChallengeRepository challengeRepository,
                                       final EtherpadLitePadAdapter etherpadLitePadAdapter) {
        this.topicBroadcastService = topicBroadcastService;
        this.tabTransformer = tabTransformer;
        this.tabRepository = tabRepository;
        this.programmingLanguageRepository = programmingLanguageRepository;
        this.interviewEnvironmentRepository = interviewEnvironmentRepository;
        this.challengeRepository = challengeRepository;
        this.etherpadLitePadAdapter = etherpadLitePadAdapter;
    }

    public TabDTO createTab(final long environmentId, final TabDTO tabDTO) {
        final InterviewEnvironment interviewEnvironment = findInterviewEnvironment(environmentId);
        final ProgrammingLanguage programmingLanguage = programmingLanguageRepository.findByName(tabDTO.getProgrammingLanguage().getName());

        final Tab tab = new Tab();
        tab.setTitle(StringUtils.isNotBlank(tabDTO.getTitle()) ? tabDTO.getTitle() : TabUtils.getDefaultTitle());
        tab.setOrderNumber(TabUtils.resolveOrderNumber(interviewEnvironment.getTabs()));
        tab.setProgrammingLanguage(programmingLanguage);
        tab.setEnvironment(interviewEnvironment);

        Tab saved = tabRepository.saveAndFlush(tab);

        etherpadLitePadAdapter.createPad(tab.getId(), programmingLanguage.getDefaultTemplate());

        final String readOnlyId = etherpadLitePadAdapter.getReadOnlyID(saved.getId());

        saved.setReadOnlyId(readOnlyId);

        saved = tabRepository.saveAndFlush(tab);

        if (TopicBroadcastUtils.isBroadcastRequired(interviewEnvironment.getInterview())) {
            topicBroadcastService.newTab(interviewEnvironment.getInterview().getLink(), tabTransformer.toDto(saved));
        }

        return tabTransformer.toDto(saved);
    }

    public void deleteTab(final long environmentId, final String tabId) {
        LOGGER.info("Deleting tab: " + tabId + ", from environment: " + environmentId);

        final InterviewEnvironment interviewEnvironment = findInterviewEnvironment(environmentId);

        interviewEnvironment.removeTab(tabId);

        interviewEnvironmentRepository.save(interviewEnvironment);

        try {
            etherpadLitePadAdapter.deletePad(tabId);
        } catch (Exception e) {
            // TODO: investigate duplicate request issue when better times come
            LOGGER.error("Unable to delete Pad with id: " + tabId, e);
        }

        if (TopicBroadcastUtils.isBroadcastRequired(interviewEnvironment.getInterview())) {
            topicBroadcastService.deleteTab(interviewEnvironment.getInterview().getLink(), tabId);
        }
    }

    public void saveNotes(final long environmentId, final NotesDTO notesDTO) {
        final InterviewEnvironment interviewEnvironment = findInterviewEnvironment(environmentId);

        interviewEnvironment.setNotes(notesDTO.getNotes());

        interviewEnvironmentRepository.save(interviewEnvironment);
    }

    public NotesDTO getNotes(final long environmentId) {
        final InterviewEnvironment interviewEnvironment = findInterviewEnvironment(environmentId);

        return new NotesDTO(interviewEnvironment.getNotes());
    }

    public TabDTO includeChallenge(final long environmentId, final ChallengeDTO challengeDTO) {
        final InterviewEnvironment interviewEnvironment = findInterviewEnvironment(environmentId);
        final Challenge challenge = findChallenge(challengeDTO.getId());

        final Tab tab = new Tab();
        tab.setTitle(challenge.getTitle());
        tab.setOrderNumber(TabUtils.resolveOrderNumber(interviewEnvironment.getTabs()));
        tab.setProgrammingLanguage(challenge.getProgrammingLanguage());
        tab.setEnvironment(interviewEnvironment);
        tab.setDescription(challenge.getDescription());

        Tab saved = tabRepository.saveAndFlush(tab);

        etherpadLitePadAdapter.createPad(tab.getId(), challenge.getSourceCode());

        final String readOnlyId = etherpadLitePadAdapter.getReadOnlyID(saved.getId());

        saved.setReadOnlyId(readOnlyId);

        saved = tabRepository.saveAndFlush(tab);

        if (TopicBroadcastUtils.isBroadcastRequired(interviewEnvironment.getInterview())) {
            topicBroadcastService.newTab(interviewEnvironment.getInterview().getLink(), tabTransformer.toDto(saved));
        }

        return tabTransformer.toDto(saved);
    }

    private InterviewEnvironment findInterviewEnvironment(final long environmentId) {
        return interviewEnvironmentRepository.findById(environmentId)
                .orElseThrow(() -> new EntityNotFoundException(environmentId));
    }

    private Challenge findChallenge(final long id) {
        return challengeRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(id));
    }
}
