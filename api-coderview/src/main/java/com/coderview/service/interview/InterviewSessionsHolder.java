package com.coderview.service.interview;

import com.coderview.domain.interview.Interview;
import com.coderview.model.session.InterviewSession;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class InterviewSessionsHolder {

    public static final List<InterviewSession> SESSIONS = new CopyOnWriteArrayList<>();

    public static InterviewSession getOrStartSession(final Interview interview) {
        if (isSessionStarted(interview)) {
            return getSession(interview);
        } else {
            final InterviewSession interviewSession = new InterviewSession(interview);
            SESSIONS.add(interviewSession);
            return interviewSession;
        }
    }

    public static void stopSession(final Interview interview) {
        Optional.ofNullable(getSession(interview)).ifPresent(SESSIONS::remove);
    }

    public static InterviewSession getSessionByUserSessionId(final String sessionId) {
        return SESSIONS.stream()
                .filter(session -> session.getParticipants().stream()
                        .anyMatch(participant -> participant.getSessionId().equals(sessionId)))
                .findFirst().orElse(null);
    }

    public static InterviewSession getSession(final Interview interview) {
        return SESSIONS.stream()
                .filter(session -> session.getInterview().getId().equals(interview.getId()))
                .findFirst().orElse(null);
    }

    private static boolean isSessionStarted(final Interview interview) {
        return SESSIONS.stream()
                .anyMatch(session -> session.getInterview().getId().equals(interview.getId()));
    }
}
