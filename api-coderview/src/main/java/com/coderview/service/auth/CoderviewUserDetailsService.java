package com.coderview.service.auth;

import com.coderview.domain.user.User;
import com.coderview.error.EmailNotFoundException;
import com.coderview.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class CoderviewUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public CoderviewUserDetailsService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(final String email) throws EmailNotFoundException {
        final User user = userRepository.findByEmail(email);

        if (user == null) {
            throw new EmailNotFoundException(email);
        }

        return new CoderviewUserPrincipal(user);
    }
}
