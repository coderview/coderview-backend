package com.coderview.service.auth;

import com.coderview.common.error.CoreCoderviewException;
import com.coderview.domain.user.User;
import com.coderview.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService {

    private final UserRepository userRepository;

    @Autowired
    public AuthService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getCurrentlyAuthenticatedUser() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return Optional.ofNullable(userRepository.findByEmail(authentication.getName()))
                .orElseThrow(() -> new CoreCoderviewException("User is not authenticated."));
    }
}
