package com.coderview.error;

public class EntityNotFoundException extends RuntimeException {

    private static final String ID_KEY = "{{id}}";

    private static final String ERROR_MESSAGE = "Entity with id {{id}} was not found.";

    public EntityNotFoundException() {
    }

    public EntityNotFoundException(final Object id) {
        super(ERROR_MESSAGE.replace(ID_KEY, String.valueOf(id)));
    }

    public EntityNotFoundException(final String message) {
        super(message);
    }
}
