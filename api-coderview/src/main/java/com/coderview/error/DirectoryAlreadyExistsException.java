package com.coderview.error;

public class DirectoryAlreadyExistsException extends RuntimeException {

    private static final String KEY_NAME = "{{name}}";

    private static final String ERROR_MESSAGE_TEMPLATE = "Directory: {{name}} already exists.";

    public DirectoryAlreadyExistsException(final String name) {
        super(ERROR_MESSAGE_TEMPLATE.replace(KEY_NAME, name));
    }
}
