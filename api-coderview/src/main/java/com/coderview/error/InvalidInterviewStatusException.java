package com.coderview.error;

public class InvalidInterviewStatusException extends RuntimeException {

    public InvalidInterviewStatusException() {
    }

    public InvalidInterviewStatusException(final String message) {
        super(message);
    }
}
