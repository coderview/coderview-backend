package com.coderview.error;

public class ExecutionException extends RuntimeException {

    public ExecutionException(final String message) {
        super(message);
    }
}
