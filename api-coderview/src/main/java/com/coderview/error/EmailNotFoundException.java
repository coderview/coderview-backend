package com.coderview.error;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class EmailNotFoundException extends UsernameNotFoundException {

    private static final String ERROR_MESSAGE = "User with email %s was not found.";

    public EmailNotFoundException(final String email) {
        super(String.format(ERROR_MESSAGE, email));
    }

    public EmailNotFoundException(final String email, final Throwable t) {
        super(String.format(ERROR_MESSAGE, email), t);
    }
}
