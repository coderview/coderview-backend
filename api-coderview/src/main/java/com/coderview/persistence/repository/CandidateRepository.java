package com.coderview.persistence.repository;

import com.coderview.domain.interview.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {
}
