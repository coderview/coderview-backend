package com.coderview.persistence.repository;

import com.coderview.domain.interview.Tab;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TabRepository extends JpaRepository<Tab, String> {
}
