package com.coderview.persistence.repository;

import com.coderview.domain.challenge.ChallengeType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChallengeTypeRepository extends JpaRepository<ChallengeType, Long> {
}
