package com.coderview.persistence.repository;

import com.coderview.common.constant.ProgrammingLanguageEnum;
import com.coderview.domain.interview.ProgrammingLanguage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProgrammingLanguageRepository extends JpaRepository <ProgrammingLanguage, Long> {

    ProgrammingLanguage findByName(ProgrammingLanguageEnum name);
}
