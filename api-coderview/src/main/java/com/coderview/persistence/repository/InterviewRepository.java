package com.coderview.persistence.repository;

import com.coderview.domain.interview.Interview;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InterviewRepository extends JpaRepository<Interview, Long> {

    Interview findByLink(String link);
}
