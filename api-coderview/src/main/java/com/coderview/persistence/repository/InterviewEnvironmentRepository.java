package com.coderview.persistence.repository;

import com.coderview.domain.interview.InterviewEnvironment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InterviewEnvironmentRepository extends JpaRepository<InterviewEnvironment, Long> {
}
