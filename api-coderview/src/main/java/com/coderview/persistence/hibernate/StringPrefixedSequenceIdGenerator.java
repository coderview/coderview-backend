package com.coderview.persistence.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Properties;

public class StringPrefixedSequenceIdGenerator extends SequenceStyleGenerator {

    public static final String PREFIX_PARAMETER = "prefix";
    public static final String PREFIX_DEFAULT = "";

    private String prefix = "";

    @Override
    public Serializable generate(final SharedSessionContractImplementor session, final Object object) throws HibernateException {
        return prefix + super.generate(session, object);
    }

    @Override
    public void configure(final Type type, final Properties params, final ServiceRegistry serviceRegistry) throws MappingException {
        super.configure(LongType.INSTANCE, params, serviceRegistry);
        this.prefix = ConfigurationHelper.getString(PREFIX_PARAMETER, params, PREFIX_DEFAULT);
    }
}
