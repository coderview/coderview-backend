package com.coderview.processor;

import com.coderview.common.constant.ProgrammingLanguageEnum;
import com.coderview.common.error.CoreCoderviewException;
import com.coderview.error.ExecutionException;
import com.coderview.model.execution.ExecutionContext;
import com.coderview.model.execution.ExecutionResults;
import com.coderview.model.config.DockerConfig;
import com.coderview.service.execution.DockerService;
import com.coderview.service.execution.ExecutionDirectoryService;
import com.coderview.service.execution.FileSystemMonitoringService;
import com.coderview.util.ExecutionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.WatchService;

import static com.coderview.util.Validation.requireNonNull;
import static com.coderview.util.Validation.requireNotBlank;

@Component
public class ExecutionProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionProcessor.class);

    private final FileSystemMonitoringService fileSystemMonitoringService;

    private final DockerService dockerService;

    private final ExecutionDirectoryService executionDirectoryService;

    private final DockerConfig dockerConfig;

    @Autowired
    public ExecutionProcessor(final FileSystemMonitoringService fileSystemMonitoringService,
                              final DockerService dockerService,
                              final ExecutionDirectoryService executionDirectoryService,
                              final DockerConfig dockerConfig) {
        this.fileSystemMonitoringService = fileSystemMonitoringService;
        this.dockerService = dockerService;
        this.executionDirectoryService = executionDirectoryService;
        this.dockerConfig = dockerConfig;
    }

    public ExecutionResults execute(final ProgrammingLanguageEnum programmingLanguage, final String sourceCode) {
        return this.execute(prepareExecutionContext(programmingLanguage, sourceCode));
    }

    private ExecutionContext prepareExecutionContext(final ProgrammingLanguageEnum programmingLanguage, final String sourceCode) {
        requireNonNull(programmingLanguage, () -> new CoreCoderviewException("Programming language not specified."));
        requireNotBlank(sourceCode, () -> new CoreCoderviewException("Source code not specified."));

        return new ExecutionContext()
                .setProgrammingLanguage(programmingLanguage)
                .setSourceCode(sourceCode)
                .setExecutionId(ExecutionUtils.getExecutionId())
                .setDirectory(dockerConfig.getExecution().getDirectory())
                .setDockerImage(dockerConfig.getExecution().getImages().get(programmingLanguage.getValue()))
                .setDockerBind(dockerConfig.getExecution().getBind())
                .setDockerVolume(dockerConfig.getExecution().getVolume());
    }

    private ExecutionResults execute(final ExecutionContext context) {
        try {
            executionDirectoryService.prepare(context);

            final WatchService watchService = fileSystemMonitoringService.register(context.getExecutionOutputDirectory());

            dockerService.run(context);

            fileSystemMonitoringService.monitor(watchService);

            return executionDirectoryService.readExecutionResults(context);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            throw new ExecutionException(e.getMessage());
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            throw new ExecutionException(e.getMessage());
        } finally {
            try {
                executionDirectoryService.remove(context);
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }
}
