package com.coderview.dto.challenge;

import com.coderview.common.constant.ChallengeScope;
import com.coderview.dto.DomainDTO;
import com.coderview.dto.interview.ProgrammingLanguageDTO;

public class ChallengeDTO implements DomainDTO {

    private Long id;

    private String title;

    private String description;

    private String sourceCode;

    private String difficulty;

    private ChallengeTypeDTO type;

    private ProgrammingLanguageDTO programmingLanguage;

    private ChallengeScope scope;

    public ChallengeDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(final String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(final String difficulty) {
        this.difficulty = difficulty;
    }

    public ChallengeTypeDTO getType() {
        return type;
    }

    public void setType(final ChallengeTypeDTO type) {
        this.type = type;
    }

    public ProgrammingLanguageDTO getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(final ProgrammingLanguageDTO programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    public ChallengeScope getScope() {
        return scope;
    }

    public void setScope(final ChallengeScope scope) {
        this.scope = scope;
    }
}
