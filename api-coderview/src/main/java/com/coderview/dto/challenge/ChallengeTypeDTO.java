package com.coderview.dto.challenge;

import com.coderview.dto.DomainDTO;

public class ChallengeTypeDTO implements DomainDTO {

    private Long id;

    private String name;

    public ChallengeTypeDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
