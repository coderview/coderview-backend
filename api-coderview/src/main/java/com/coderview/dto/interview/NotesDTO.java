package com.coderview.dto.interview;

public class NotesDTO {

    private String notes;

    public NotesDTO() {
    }

    public NotesDTO(final String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(final String notes) {
        this.notes = notes;
    }
}
