package com.coderview.dto.interview;

import com.coderview.common.constant.ProgrammingLanguageEnum;
import com.coderview.dto.DomainDTO;

public class ProgrammingLanguageDTO implements DomainDTO {

    private ProgrammingLanguageEnum name;

    private String version;

    private String info;

    private String defaultTemplate;

    public ProgrammingLanguageDTO() {
    }

    public ProgrammingLanguageEnum getName() {
        return name;
    }

    public void setName(final ProgrammingLanguageEnum name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(final String info) {
        this.info = info;
    }

    public String getDefaultTemplate() {
        return defaultTemplate;
    }

    public void setDefaultTemplate(final String defaultTemplate) {
        this.defaultTemplate = defaultTemplate;
    }
}
