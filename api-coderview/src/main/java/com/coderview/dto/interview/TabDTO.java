package com.coderview.dto.interview;

import com.coderview.dto.DomainDTO;

public class TabDTO implements DomainDTO {

    private String id;

    private String readOnlyId;

    private String title;

    private ProgrammingLanguageDTO programmingLanguage;

    private Integer orderNumber;

    private String description;

    public TabDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getReadOnlyId() {
        return readOnlyId;
    }

    public void setReadOnlyId(final String readOnlyId) {
        this.readOnlyId = readOnlyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public ProgrammingLanguageDTO getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(final ProgrammingLanguageDTO programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(final Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
