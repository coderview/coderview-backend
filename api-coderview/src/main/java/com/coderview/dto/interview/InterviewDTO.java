package com.coderview.dto.interview;


import com.coderview.constant.InterviewStatus;
import com.coderview.dto.DomainDTO;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InterviewDTO implements DomainDTO {

    private Long id;

    private String title;

    private String link;

    private String interviewer;

    private InterviewStatus status;

    private Date scheduledFor;

    private CandidateDTO candidate;

    private InterviewEnvironmentDTO environment;

    private Date startedAt;

    private Date completedAt;

    public InterviewDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public String getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(final String interviewer) {
        this.interviewer = interviewer;
    }

    public InterviewStatus getStatus() {
        return status;
    }

    public void setStatus(final InterviewStatus status) {
        this.status = status;
    }

    public Date getScheduledFor() {
        return scheduledFor;
    }

    public void setScheduledFor(final Date scheduledFor) {
        this.scheduledFor = scheduledFor;
    }

    public CandidateDTO getCandidate() {
        return candidate;
    }

    public void setCandidate(final CandidateDTO candidate) {
        this.candidate = candidate;
    }

    public InterviewEnvironmentDTO getEnvironment() {
        return environment;
    }

    public void setEnvironment(final InterviewEnvironmentDTO environment) {
        this.environment = environment;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(final Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(final Date completedAt) {
        this.completedAt = completedAt;
    }
}
