package com.coderview.dto.interview;

import com.coderview.dto.DomainDTO;

import java.util.List;

public class InterviewEnvironmentDTO implements DomainDTO {

    private Long id;

    private String notes;

    private List<TabDTO> tabs;

    public InterviewEnvironmentDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(final String notes) {
        this.notes = notes;
    }

    public List<TabDTO> getTabs() {
        return tabs;
    }

    public void setTabs(final List<TabDTO> tabs) {
        this.tabs = tabs;
    }
}
